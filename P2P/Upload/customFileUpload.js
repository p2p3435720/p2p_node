import multer from "multer";
import path from "path";
import reply from "../../Common/reply.js";
import _ from "lodash";

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log(req.headers.mypath,"req.headers.mypathreq.headers.mypath");
        cb(null, `./P2P/Images/${req.headers.mypath}`);
    },

    filename: function (req, file, cb) {
        let extensionFile = path.extname(file.originalname);
        let saveFileName = `${Date.now()}${extensionFile}`;
        // console.log({extensionFile,saveFileName})
        cb(null, saveFileName);
    }
});

const checkFileType = (file, cb) => {
    var ext = path.extname(file?.originalname);

    if (ext !== '.png' && ext !== '.jpg' && ext !== '.jpeg' && ext !== '.pdf') {
        return cb(new Error('Only png, jpg , jpeg and pdf are allowed'));
    }

    return cb(null, true);
}

// done
const upload = multer({
    storage: storage, fileFilter: (req, file, cb) => { checkFileType(file, cb) }
}).single("file");

var uploadFiles = multer({ storage: storage,  fileFilter: (req, file, cb) => { checkFileType(file, cb) } }).array("images", 3);

const customFileUpload = (req, res, next) => {
    const Image_not_required_array = ["/comment_create"];

    const is_multiple = req.headers?.is_multiple || 'single'; 
   
    if(is_multiple == "multiple"){
        uploadFiles(req, res, (err) => {

            let filelength = req.files.length || 0;
           
            if(err){
                let result = reply.failed(err.message);
                req.filedata = result;
                return res.send(reply.failed("Getting some problem!!"));
            }

            if(filelength == 0 && Image_not_required_array.includes(req.path) ){
                let result = reply.failed("");
                req.filedata = result;
                return  next();
            }

            if (filelength == 0 && !Image_not_required_array.includes(req.path)) {
                return res.send(reply.failed("Please Upload Image or File"));
            }

            let filenames = [];

            _.map(req.files, (v) => {
                filenames.push(v.filename)
            })

            req.filedata = reply.success("Images Added Successfully!!", filenames);
            return next();
        });

    }else{
        upload(req, res, (err) => {
            console.log(err,"-0-0-0-0-0-0");
            if(err){
                let result = reply.failed(err.message);
                req.filedata = result;
                return  next();
            }

            if(req.file == undefined && Image_not_required_array.includes(req.path)){
                let result = reply.failed("");
                req.filedata = result;
                return  next();
            }

            if (req.file == undefined && !Image_not_required_array.includes(req.path)) {
                req.filedata = reply.failed("Please Upload Image or File");
                return next();
            }

            req.filedata = reply.success(req?.file?.filename);
            
            return next();
        });
    }
}

export default customFileUpload;

