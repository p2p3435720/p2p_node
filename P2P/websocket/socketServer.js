import { WebSocketServer } from "ws";
import P2PSocketAuthenticate from "../../Middleware/P2PSocketAuthenticate.js";
import _ from "lodash";
import { P2P_Trade } from "../Models/P2P_Trade.js";
import { P2P_Order } from "../Models/P2P_Order.js";
import { P2P_Msg } from "../Models/P2P_Msg.js";
import { CL } from "cal-time-stamper";
import p2pevent from "../Events/myp2pevent.js";
import { APPEAL_STATUS, ORDER_STATUS, P2P_WALLET_EVENT } from "../Common/GVariables.js";
import { Op } from "sequelize";
import sequelize from 'sequelize';

import { P2p_Appeal } from "../Models/P2p_Appeal.js";
import { P2P_Feedback } from "../Models/P2P_Feedback.js";
import CValidator from "../../Validator/CustomValidation.js";
import variables from "../../Config/variables.js";

export function socketServer(server) {


    /////////////////////////////////*******************************************///////////////////////////////////
    ////////////////////////////Cancel Orders If minimum Qty greater than pending qty//////////////////////////////////
    /////////////////////////////////*******************************************///////////////////////////////////
    const Expire_Order = async () => {

        let Orders = await P2P_Order.findAll({
            where: {
                [Op.and]: {
                    pending_qty: {
                        [Op.lt]: sequelize.col('min_qty'),
                    },
                    status: {
                        [Op.in]: [ORDER_STATUS.partially_completed]
                    }
                }
            }, attributes: ['id', 'status', 'reason', 'cancelled_at', 'order_type', 'pending_qty', 'user_id', 'currency']
        });


        if(Orders.length  == 0){
            return "NO ORDER FOUND FOR EXPIRED";
        }


        Orders.map(async (v) => {

            if (v.order_type == 'SELL') {

                p2pevent.emit("P2PWalletEvent",{
                    data:{
                        user_id:v.user_id,
                        currency:v.currency,
                        amount:v.pending_qty,
                        comment:`Refund ${v.pending_qty} ${v.currency} by p2p sell order Expired!`
                    },
                    type:P2P_WALLET_EVENT.cancel
                });
            } 
            v.status = 'expired'
            v.reason = 'Due to less Pending Quantity than Minimum Quantity'
            v.cancelled_at = new Date(Date.now())
            await v.save();
        })
    }

    setInterval(() => {
        Expire_Order()
    }, 120000); // after two mintues.


    const wss = new WebSocketServer({ server });

    const ALL_CLIENTS = {};
    // Example Array
    // {
    // "1677234547071" : ws ,
    // "1677234547071" : ws 
    // }

    let ALL_SOCKETS = {};
    // Example Array 
    // {
    //     "2":"1677234547071"
    // }

    wss.getUniqueID = function () {

        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000000).toString(16).substring(2);
        }
        return Date.now() + '-' + s4();
    }

    const wsMessage = (ws, status, msg, data = null) => {
        console.log(msg,"msgmsg");
        let result = {
            status_code: status,
            msg: msg
        };
        if (data != null) {
            Object.assign(result, data);
        }
        result = JSON.stringify(result);
        return ws.send(result);
    }

    const authorize = async (req) => {
        return await P2PSocketAuthenticate(req);
    }


    // Common Database Functions
    const TableTrade = async (update_columns = {}, whereCondition = {}) => {
console.log(whereCondition,"whereConditionwhereCondition");
       let r = await P2P_Trade.findOne({ where: whereCondition }); 
       if(r){
            if(Object.keys(update_columns).length > 0){
                for (const key in update_columns) {
                    r[key] = update_columns[key];
                }
                await r.save();
            }
            return r;
       }else{
            return TYPES_SOCKET.INVALID;
       }

    }


    // If you want to add new event just add it in this object.
    var HitEvents = {
        'MESSAGE': (param) => MESSAGE(param),
        'IMAGE': (param) => IMAGE(param),
        'NOTIFY_SELLER': (param) => NOTIFY_SELLER(param),
        'PAYMENT_RECIEVE': (param) => PAYMENT_RECIEVE(param),
        'ORDER_CANCEL': (param) => ORDER_CANCEL(param),
        'TIME_OUT': (param) => TIME_OUT(param),
        'APPEAL': (param) => APPEAL(param),
        'CANCEL_APPEAL': (param) => CANCEL_APPEAL(param),
        'SUPPORT': (param) => SUPPORT(param),
        'FEEDBACK':(param)=>FEEDBACK(param),
        'FEEDBACK_DELETE':(param)=>FEEDBACK_DELETE(param)
    }

    const TYPES_SOCKET = {
        TRANSFERRED:"TRANSFERRED",
        PAYMENT_RECIEVE:"PAYMENT_RECIEVE",
        ORDER_CANCEL:"ORDER_CANCEL",
        TIME_OUT:"TIME_OUT",
        MESSAGE:"MESSAGE",
        IMAGE:"IMAGE",
        INVALID:"INVALID",
        VALIDATED:"VALIDATED",
        APPEAL: "APPEAL",
        CANCEL_APPEAL: "CANCEL_APPEAL",
        SUPPORT: "SUPPORT",
        FEEDBACK:"FEEDBACK",
        FEEDBACK_DELETE:"FEEDBACK_DELETE"
    }

    const IsValidParam = (ObjectKeys, ValidateKeys) => {
        
        let status = TYPES_SOCKET.VALIDATED;

        for (const key of ValidateKeys) {

            let o = ObjectKeys[key];
            console.log(o);
            if (!o) {
                status = TYPES_SOCKET.INVALID;
                break;
            }
        }

        return status;
    }


    const addMintues =  (m) => {
       let mm = new Date().setMinutes(new Date().getMinutes() + m);
       return mm;
    }


    const MESSAGE = async (param) => {
        let validate = IsValidParam(param,["user_id","receiver_id","trade_id"]);

        if(validate == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        let trade_id = param?.trade_id || "";
        let user_id = param?.user_id || "";
        let sender_id = param?.user_id || "";
        let receiver_id = param?.receiver_id || "";
        let msg = param?.msg || "";
        let file = param?.file || "";


        let result = await TableTrade({}, {
            id:trade_id,
            status: ORDER_STATUS.processing,
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ]
        });

        if(result == TYPES_SOCKET.INVALID){
          return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

       let res =  await P2P_Msg.create({ sender_id , msg, receiver_id, trade_id,file });

       const client = ALL_CLIENTS[ALL_SOCKETS[param.receiver_id]];

        if (client && client.readyState === param.ws.OPEN) {

            let sendResponse = JSON.stringify({ type: TYPES_SOCKET.MESSAGE, msg, file:res.file,receiver_id,sender_id });
            client.send(sendResponse);
        }
    }

    const NOTIFY_SELLER = async (param) => { // It will complete by buyer

        let validate = IsValidParam(param,["user_id","receiver_id","trade_id"]);

        if(validate == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        let trade_id = param?.trade_id || "";
        let receiver_id = param?.receiver_id || "";
        let user_id = param?.user_id || "";


        let appeal_at = addMintues(1); // it will replace with 10 mintues

        let result = await TableTrade({
            buyer_confirmation:1,
            expired_at: null,
            appeal_at
        }, {
            id:trade_id,
            status: ORDER_STATUS.processing,
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ],
            buyer_user_id: user_id
        });

        if(result == TYPES_SOCKET.INVALID){
          return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }
        
        const client = ALL_CLIENTS[ALL_SOCKETS[receiver_id]];

        wsMessage(param.ws, 1, "Mail sent Sucessfully", { appeal_at , type: TYPES_SOCKET.TRANSFERRED });

        if (client && client.readyState === param.ws.OPEN) {
            let sendResponse = JSON.stringify({ type: TYPES_SOCKET.TRANSFERRED, appeal_at });
            client.send(sendResponse);
        }
    }

    const PAYMENT_RECIEVE = async (param) => {   // It will complete by seller

        // Validation Required Keys
        let validate = IsValidParam(param,["user_id","receiver_id","trade_id"]);

        if(validate == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        let trade_id = param?.trade_id || "";
        let receiver_id = param?.receiver_id || "";
        let user_id = param?.user_id || "";


        // Trade Update
        let trade = await TableTrade({
            seller_confirmation:1,
            status: ORDER_STATUS.completed
        }, {
            id:trade_id,
            status: ORDER_STATUS.processing,
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ],
            seller_user_id: user_id
        });

        if(trade == TYPES_SOCKET.INVALID){
          return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }


        // Buyer Order Update
        let Buyer_Order = await P2P_Order.findOne({ where: { id: trade.buyer_order_id } });

        Buyer_Order.pending_qty = CL.sub(Buyer_Order.pending_qty, trade.quantity);

        Buyer_Order.search_max_qty = CL.mul(trade.at_price, Buyer_Order.pending_qty);

        if (parseFloat(Buyer_Order.pending_qty) > 0) {
            Buyer_Order.status = ORDER_STATUS.partially_completed;
        }

        if (parseFloat(Buyer_Order.pending_qty) == 0) {
            Buyer_Order.status = ORDER_STATUS.completed;
        }

        await Buyer_Order.save();


        p2pevent.emit("P2PWalletEvent", {
            data: {
                user_id: Buyer_Order.user_id,
                currency: Buyer_Order.currency,
                amount: trade.quantity,
                comment: `Credit ${trade.quantity} ${Buyer_Order.currency} to your p2p wallet`
            },
            type: P2P_WALLET_EVENT.credit
        });

        //////Seller Order Update///////////////////////

        let Seller_Order = await P2P_Order.findOne({ where: { id: trade.seller_order_id } });

        Seller_Order.pending_qty = CL.sub(Seller_Order.pending_qty, trade.quantity);

        Seller_Order.search_max_qty = CL.mul(trade.at_price, Seller_Order.pending_qty);

        if (parseFloat(Seller_Order.pending_qty) > 0) {
            Seller_Order.status = ORDER_STATUS.partially_completed
        }
        if (parseFloat(Seller_Order.pending_qty) == 0) {
            Seller_Order.status = ORDER_STATUS.completed
        }

        await Seller_Order.save();

        p2pevent.emit("P2PWalletEvent", {
            data: {
                user_id: Seller_Order.user_id,
                currency: Seller_Order.currency,
                amount: trade.quantity,
                comment: `UnFreeze ${trade.quantity} ${Seller_Order.currency} from your p2p wallet`
            },
            type: P2P_WALLET_EVENT.unfreeze
        });

        wsMessage(param.ws, 1, "Payment Recieved Sucessfully", { type: TYPES_SOCKET.PAYMENT_RECIEVE })

        const client = ALL_CLIENTS[ALL_SOCKETS[receiver_id]];

        if (client && client.readyState === param.ws.OPEN) {
            let sendResponse = JSON.stringify({ type: TYPES_SOCKET.PAYMENT_RECIEVE, msg: 'Order Completed Sucessfully' });
            client.send(sendResponse);
        }


    }

    const ORDER_CANCEL = async (param) => {

        // Validation Required Keys
        let validate = IsValidParam(param,["user_id","receiver_id","trade_id", "reason"]);

        if(validate == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        let trade_id = param?.trade_id || "";
        let receiver_id = param?.receiver_id || "";
        let reason = param?.reason || "";
        let user_id = param?.user_id || "";


        // Trade Update
        let trade = await TableTrade({
            buyer_confirmation:2,
            status: ORDER_STATUS.canceled,
            reason:reason,
            cancelled_at:new Date(Date.now())
        }, {
            id:trade_id,
            status: ORDER_STATUS.processing,
            buyer_user_id: user_id
        });

        if(trade == TYPES_SOCKET.INVALID){
          return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        //////Buyer Order Update///////////////////////

        let BuyerOrder = await P2P_Order.findOne({ where: { id: trade.buyer_order_id } });
        BuyerOrder.status = ORDER_STATUS.canceled;
        BuyerOrder.reason = reason
        BuyerOrder.cancelled_at = trade.cancelled_at


        await BuyerOrder.save()

        //////Seller Order Update///////////////////////

        let Seller_Order = await P2P_Order.findOne({ where: { id: trade.seller_order_id } });
        let res = CL.sub(Seller_Order.max_qty, Seller_Order.pending_qty)
        if (res > 0) {
            Seller_Order.status = ORDER_STATUS.partially_completed;
        }
        if (res == 0) {
            Seller_Order.status = ORDER_STATUS.placed;
        }

        await Seller_Order.save()

        
        const client = ALL_CLIENTS[ALL_SOCKETS[receiver_id]];

        wsMessage(param.ws, 0, "Order Cancelled", { type: TYPES_SOCKET.ORDER_CANCEL });

        if (client && client.readyState === param.ws.OPEN) {
            let sendResponse = JSON.stringify({ type: TYPES_SOCKET.ORDER_CANCEL, msg: 'Order Cancelled' });
            client.send(sendResponse);
        }
    }

    const TIME_OUT = async (param) => {
        console.log({param});
        // Validation Required Keys
        let validate = IsValidParam(param,["user_id","receiver_id","trade_id"]);
        console.log({validate});

        if(validate == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }


        let trade_id = param?.trade_id || "";
        let receiver_id = param?.receiver_id || "";
        let user_id = param?.user_id || "";


        // Trade Update
        let trade = await TableTrade({
            status: ORDER_STATUS.expired,
            reason:"Times Out",
            cancelled_at:new Date(Date.now())
        }, {
            id:trade_id,
            status: {
                [Op.in]:[ORDER_STATUS.processing, ORDER_STATUS.expired]  
            },
            buyer_confirmation:0,
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ]
        });

        console.log({trade});

        if(trade == TYPES_SOCKET.INVALID){
          return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        // Buyer Order Update

        let BuyerOrder = await P2P_Order.findOne({ where: { id: trade.buyer_order_id } });

        let Buy = CL.sub(BuyerOrder.max_qty, BuyerOrder.pending_qty);

        if (Buy > 0) {
            BuyerOrder.status = ORDER_STATUS.partially_completed;
        }
        if (Buy == 0) {
            BuyerOrder.status = ORDER_STATUS.placed;
        }


        await BuyerOrder.save();

        // Seller Order Update

        let Seller_Order = await P2P_Order.findOne({ where: { id: trade.seller_order_id } });

        let res = CL.sub(Seller_Order.max_qty, Seller_Order.pending_qty)

        if (res > 0) {
            Seller_Order.status = ORDER_STATUS.partially_completed;
        }
        if (res == 0) {
            Seller_Order.status = ORDER_STATUS.placed;
        }

        await Seller_Order.save()

        // let obj = { type: 'cancelled', msg: "Order Cancelled due to Time Out" }
        // obj = JSON.stringify(obj)

        wsMessage(param.ws, 0, "Order Cancelled due to Time Out", { type: TYPES_SOCKET.TIME_OUT });

        const client = ALL_CLIENTS[ALL_SOCKETS[param.receiver_id]];

        if (client && client.readyState === param.ws.OPEN) {
            let sendResponse = JSON.stringify({ type: TYPES_SOCKET.TIME_OUT, msg: 'Order Expired!' });
            client.send(sendResponse);
        }
    }


    // new events 

    //FEEDBACK (9/3/2023)
    const FEEDBACK = async (param) => {
        let validate = IsValidParam(param,["user_id","receiver_id","trade_id","feedback_type","feedback_comment"]);

        if(validate == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        const {trade_id, user_id, receiver_id, feedback_type, feedback_comment} = param;

        // Trade Update
         let trade = await TableTrade({}, {
            id:trade_id,
            status: {
                [Op.not]:[ORDER_STATUS.processing]  
            },
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ]
        });

        if(trade == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        const feedback_data = {
            trade_id,
            user_id,
            feedback_to:receiver_id,
            feedback_type,
            feedback_comment
        }

        let exist_feed = await P2P_Feedback.findOne({
            where:{
                trade_id: feedback_data.trade_id,
                user_id: feedback_data.user_id,
                feedback_to: feedback_data.feedback_to
            }
        });
    
        try{
            if(exist_feed){
                exist_feed.feedback_type = feedback_data.feedback_type;
                exist_feed.feedback_comment = feedback_data.feedback_comment;
                await exist_feed.save();
                return wsMessage(param.ws, 1, "Feedback updated successfully", { type: TYPES_SOCKET.FEEDBACK, data: exist_feed })
            }else{
                let create = await P2P_Feedback.create(feedback_data);
                return wsMessage(param.ws, 1, "Feedback added successfully", { type: TYPES_SOCKET.FEEDBACK, data: create })
            }
        }catch(err){
            console.log(err);
            return wsMessage(param.ws, 0, "Unable to add feedback", { type: TYPES_SOCKET.INVALID })
        }
    }

    //FEEDBACK_DELETE
    const FEEDBACK_DELETE = async (param) => {
        let validate = IsValidParam(param,["user_id","receiver_id","trade_id"]);

        if(validate == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        const {trade_id, user_id, receiver_id} = param;

        // Trade Update
        let trade = await TableTrade({}, {
            id:trade_id,
            status: {
                [Op.not]:[ORDER_STATUS.processing]  
            },
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ]
        });

        if(trade == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        //find feedback
        try{
            let destroy = await P2P_Feedback.destroy({
                where: {
                    trade_id: trade_id,
                    user_id: user_id,
                    feedback_to: receiver_id
                }
            })
            return (destroy == 1) ? wsMessage(param.ws, 1, "Feedback Deleted Successfully.", { type: TYPES_SOCKET.FEEDBACK_DELETE }) : wsMessage(param.ws, 0, "Nothing to delete.", { type: TYPES_SOCKET.FEEDBACK_DELETE })
           
        }catch(err){
            console.log(err)
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID })
        }
    }

    const APPEAL = async (param) => { // It will complete by buyer

        let validate = IsValidParam(param, ["user_id", "receiver_id", "trade_id", "appeal_raisedBy", "reason"]);

        if (validate == TYPES_SOCKET.INVALID) {
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        let trade_id = param?.trade_id || "";
        let user_id = param?.user_id || "";
        let receiver_id = param?.receiver_id || "";
        let appeal_by = param?.appeal_raisedBy || "";
        let reason = param?.reason || "";

        // Trade Update
        let trade = await TableTrade({}, {
            id:trade_id,
            status: {
                [Op.in]:[ORDER_STATUS.processing]  
            },
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ]
        });

        if(trade == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }


        let expired_at = addMintues(1); // we will change it with 10 mintues

        let data = {
            trade_id,
            appeal_by,
            appeal_user_id: param.user_id,
            reason,
            expired_at
        }

        let appeal_found = await P2p_Appeal.findOne({
            where: {
                trade_id,
                [Op.or]: [
                    {
                        status: {
                            [Op.or]: [APPEAL_STATUS.closed, APPEAL_STATUS.opened, APPEAL_STATUS.processing]
                        }
                    },
                    { expired_at: { [Op.gt]: addMintues(0) } } // greater than current time
                ],
            }, 
            raw: true
        })

        if (appeal_found) {
            return wsMessage(param.ws, 0, "APPEAL after some time ", { type: TYPES_SOCKET.APPEAL });
        }

        let result = await P2p_Appeal.create(data)

        let appeal_request = {
            id: result.id,
            status: result.dataValues.status,
            appeal_by: result.dataValues.appeal_by,
            appeal_user_id: result.dataValues.appeal_user_id,
            reason: result.dataValues.reason,
            expired_at: result.dataValues.expired_at
        }
        
        wsMessage(param.ws, 1, "APPEALED Sucessfully", { type: TYPES_SOCKET.APPEAL, id: result.id,
            status: result.dataValues.status,
            appeal_by: result.dataValues.appeal_by,
            appeal_user_id: result.dataValues.appeal_user_id,
            reason: result.dataValues.reason,
            expired_at: result.dataValues.expired_at });
        
        const client = ALL_CLIENTS[ALL_SOCKETS[receiver_id]];
        
        if (client && client.readyState === param.ws.OPEN) {
            let sendResponse = JSON.stringify({ type: TYPES_SOCKET.APPEAL, id: result.id,
                status: result.dataValues.status,
                appeal_by: result.dataValues.appeal_by,
                appeal_user_id: result.dataValues.appeal_user_id,
                reason: result.dataValues.reason,
                expired_at: result.dataValues.expired_at });
            client.send(sendResponse);
        }
    }

    const CANCEL_APPEAL = async (param) => {

        let validate = IsValidParam(param, ["user_id", "receiver_id", "trade_id", "appeal_id"]);

       

        if (validate == TYPES_SOCKET.INVALID) {
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

     

        let trade_id = param?.trade_id || "";
        let user_id = param?.user_id || "";
        let receiver_id = param?.receiver_id || "";

        // Trade Update
        let trade = await TableTrade({}, {
            id:trade_id,
            status: {
                [Op.in]:[ORDER_STATUS.processing]  
            },
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ]
        });


      
 


        if(trade == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }


        let Appeal_data = await P2p_Appeal.findOne({ 
            where: { 
                [Op.and]:[
                    { id: param.appeal_id },
                    { trade_id },
                    { appeal_user_id: param.user_id }
                ],
                status: {[Op.in]: [APPEAL_STATUS.opened]} 
            } 
        });

        // console.log({Appeal_data})

      
        
        if (!Appeal_data) {
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.CANCEL_APPEAL });
        }

        let appeal_at = addMintues(1);

        await P2P_Trade.update({ appeal_at }, { where: { id: trade_id } })

        
        Appeal_data.status = APPEAL_STATUS.canceled;
        Appeal_data.cancelled_at = addMintues(0);
        await Appeal_data.save();

        let send = {
            "appeal_by": Appeal_data.appeal_by,
            "appeal_user_id": Appeal_data.appeal_user_id,
            "cancelled_at": Appeal_data.cancelled_at,
            "expired_at": Appeal_data.expired_at,
            "reason": Appeal_data.reason,
            "status": Appeal_data.status,
            "appeal_at": appeal_at

        }

        wsMessage(param.ws, 1, "APPEAL CANCELLED !!", { send, type: TYPES_SOCKET.CANCEL_APPEAL });
        
        const client = ALL_CLIENTS[ALL_SOCKETS[receiver_id]];

        if (client && client.readyState === param.ws.OPEN) {
            let sendResponse = JSON.stringify({ type: TYPES_SOCKET.CANCEL_APPEAL, send });
            client.send(sendResponse);
        }
    }

    const SUPPORT = async (param) => {

        let validate = IsValidParam(param, ["user_id", "receiver_id", "trade_id", "appeal_id"]);

        if (validate == TYPES_SOCKET.INVALID) {
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        let trade_id = param?.trade_id || "";
        let user_id = param?.user_id || "";
        let receiver_id = param?.receiver_id || "";

        // Trade Update
        let tt = await TableTrade({}, {
            id:trade_id,
            status: {
                [Op.in]:[ORDER_STATUS.processing]  
            },
            [Op.or]: [
                {
                    buyer_user_id: user_id
                },
                {
                    seller_user_id: user_id
                }
            ]
        });

        if(tt == TYPES_SOCKET.INVALID){
            return wsMessage(param.ws, 0, "Invalid Data!", { type: TYPES_SOCKET.INVALID });
        }

        
        let Appeal_data = await P2p_Appeal.findOne({ where: { id: param.appeal_id, status: {[Op.in]: [APPEAL_STATUS.opened]}  } })
        
        if (!Appeal_data) {
            wsMessage(param.ws, 0, "Invalid Data!!", { type: TYPES_SOCKET.SUPPORT });
        }
        
        Appeal_data.status = APPEAL_STATUS.processing
        await Appeal_data.save()

        let trade = await P2P_Trade.findOne({ where: { id: trade_id } })
        trade.status = ORDER_STATUS.disputed
        await trade.save();

       
        await P2P_Order.update({status : ORDER_STATUS.disputed},{
             where: { 
                id: {
                    [Op.in]: [trade.buyer_order_id, trade.seller_order_id]
                } 
            } 
        });

        wsMessage(param.ws, 1, "Contacted Successfully !!", { type: TYPES_SOCKET.SUPPORT, trade_status:trade.status });

        const client = ALL_CLIENTS[ALL_SOCKETS[receiver_id]];

        if (client && client.readyState === param.ws.OPEN) {
            let sendResponse = JSON.stringify({ type: TYPES_SOCKET.SUPPORT, trade_status:trade.status });
            client.send(sendResponse);
        }
        
    }


    // Websocket Connection
    wss.on('connection', async function connection(ws, req) {

        let { data, status_code } = await authorize(req);
        // Verify the token.
        if (status_code == '0') { return; }

        let user_id = data.id;

        const clientId = wss.getUniqueID();

        ALL_SOCKETS[user_id] = clientId;

        ALL_CLIENTS[clientId] = ws;

        ws.on('message', function incoming(input) {
            input = input.toString();  // {}

            try {
                input = JSON.parse(input);
            } catch (error) {
                return wsMessage(ws, 0, 'INVALID FORMAT');
            }

            const EVENT = input.type || " ";
            input.ws = ws
            input.user_id = user_id
            let validEvent = _.keys(HitEvents);

            if (!validEvent.includes(EVENT)) {
                return wsMessage(ws, 0, 'INVALID DATA!!');
            }

            HitEvents[EVENT](input);
        });

        ws.on('close', () => {
            let sid = ALL_SOCKETS[user_id];
            delete ALL_SOCKETS[user_id];
            delete ALL_CLIENTS[sid];
            console.log(`WebSocket connection ${clientId} closed`);
        });

        // send immediatly a feedback to the incoming connection    
        return wsMessage(ws, 1, 'Connected Successfully', { c_id: clientId });
    });

}