
import P2PAuthenticate from "../../Middleware/P2PAuthenticate.js";
import Authenticate from "../../Middleware/Authenticate.js";
import express from "express";
import FeedbackController from "../Controllers/FeedbackController.js"

var router = express.Router();

router.get('/get', P2PAuthenticate, FeedbackController.get);
router.delete('/delete', Authenticate, P2PAuthenticate, FeedbackController.Fdelete);
router.post('/create', Authenticate,P2PAuthenticate, FeedbackController.create);

export const FeedbackRoutes = router;