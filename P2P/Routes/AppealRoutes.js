
import AuthenticateAdmin from "../../Middleware/AuthenticateAdmin.js";
import express from "express";
import AppealController from "../Controllers/AppealController.js";

var router = express.Router();

router.get('/get', AuthenticateAdmin, AppealController.get_appeal);
export const AppealRoutes = router;