import Authenticate from "../../Middleware/Authenticate.js";
import P2PAuthenticate from "../../Middleware/P2PAuthenticate.js";
import AuthenticateAdmin from "../../Middleware/AuthenticateAdmin.js";
import OrderController from "../Controllers/OrderController.js";
import express from "express";
import customFileUpload from "../Upload/customFileUpload.js"

var router = express.Router();
const UploadImageWithPath = (req, res, next) => {
    req.headers['mypath'] = "chatImages";
    return customFileUpload(req, res, next);
  }
router.post('/place',Authenticate,P2PAuthenticate,OrderController.place_order);
router.get('/country',P2PAuthenticate,OrderController.getCountries);
router.get('/get',P2PAuthenticate,OrderController.get_order);
router.get('/order_crypto',P2PAuthenticate,OrderController.order_crypto);
router.post('/cancel',Authenticate,P2PAuthenticate,OrderController.cancel_order);
router.get('/history',Authenticate,P2PAuthenticate,OrderController.order_history);
router.get('/get_crypto',Authenticate,P2PAuthenticate,OrderController.get_crypto);
router.get('/P2p_Profile',P2PAuthenticate,OrderController.User_P2p_Profile);
router.get('/trade-history',Authenticate,P2PAuthenticate,OrderController.trade_history);
router.get('/my_get_order',Authenticate,P2PAuthenticate,OrderController.my_get_order);
router.post('/upload_image',UploadImageWithPath,P2PAuthenticate,OrderController.upload_image);





// Admin Routes
router.get('/get_order_boss',AuthenticateAdmin,P2PAuthenticate,OrderController.get_order_boss);


export const OrderRoutes = router;