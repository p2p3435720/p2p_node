import express from "express";
const app = express();

// P2P Wallet
import { WalletRoutes } from "./WalletRoutes.js";
app.use('/wallet', WalletRoutes);

//P2P Order
import { OrderRoutes } from "./OrderRoutes.js";
app.use('/order', OrderRoutes);

//P2P Chat
import { MsgRoutes } from "./MsgRoutes.js";
app.use('/msg', MsgRoutes);


//P2P Order
import { PaymentRoutes } from "./PaymentRoutes.js";
app.use('/paymentType', PaymentRoutes);

//P2P Trade
import { TradeRoutes } from "./TradeRoutes.js";
app.use('/trade', TradeRoutes);

//P2P Feedback
import { FeedbackRoutes } from "./FeedbackRoutes.js";
app.use('/feedback', FeedbackRoutes);

//P2P Appeal
import { AppealRoutes } from "./AppealRoutes.js";
app.use('/appeal', AppealRoutes);

//P2P Support
import { SupportRoutes } from "./P2pSupportRoutes.js";
app.use('/support', SupportRoutes);


export const P2PBaseRoutes = app;