import Authenticate from "../../Middleware/Authenticate.js";
import AuthenticateAdmin from "../../Middleware/AuthenticateAdmin.js";
import P2PAuthenticate from "../../Middleware/P2PAuthenticate.js";
import TradeController from "../Controllers/TradeController.js";
import express from "express";

var router = express.Router();

router.post('/place',Authenticate,P2PAuthenticate,TradeController.place_trade);
router.get('/get',Authenticate,P2PAuthenticate,TradeController.get_trade);
router.get('/get_trade_boss',Authenticate,AuthenticateAdmin,TradeController.get_trade_boss);



export const TradeRoutes = router;