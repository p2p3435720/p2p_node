import Authenticate from "../../Middleware/Authenticate.js";
import P2PAuthenticate from "../../Middleware/P2PAuthenticate.js";
import AuthenticateAdmin from "../../Middleware/AuthenticateAdmin.js";
import express from "express";
import customFileUpload from "../../Upload/customFileUpload.js";
import SupportController from "../Controllers/SupportController.js";

var router = express.Router();

const UploadImageWithPath = (req, res, next) => {
    req.headers['mypath'] = "tickets";
    return customFileUpload(req, res, next);
}


router.post('/comment_create',Authenticate,P2PAuthenticate,UploadImageWithPath,SupportController.comment_create);
router.get("/comment_get", P2PAuthenticate, SupportController.comment_get)
router.post('/appeal_win', AuthenticateAdmin, SupportController.appeal_win);

export const SupportRoutes = router;