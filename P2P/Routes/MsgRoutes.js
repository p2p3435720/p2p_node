
import P2PAuthenticate from "../../Middleware/P2PAuthenticate.js";
import Authenticate from "../../Middleware/Authenticate.js";
import express from "express";
import MessageController from "../Controllers/MessageController.js";

var router = express.Router();

router.get('/get', Authenticate, P2PAuthenticate, MessageController.get_msg);

export const MsgRoutes = router;