import express from "express";
import WalletController from "../Controllers/WalletController.js";
import Authenticate from "../../Middleware/Authenticate.js";
import P2PAuthenticate from "../../Middleware/P2PAuthenticate.js";
import AuthenticateAdmin from "../../Middleware/AuthenticateAdmin.js";

var router = express.Router();

// Transfer fund spot to p2p wallet
router.post('/transfer_fund', Authenticate, P2PAuthenticate, WalletController.spot_to_p2p); 
router.get('/getp2pCurrency', Authenticate, P2PAuthenticate, WalletController.getp2pCurrency); 
router.get('/getSpotCurrency', Authenticate, P2PAuthenticate, WalletController.getSpotCurrency); 

export const WalletRoutes = router;