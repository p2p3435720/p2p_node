import Authenticate from "../../Middleware/Authenticate.js";
import P2PAuthenticate from "../../Middleware/P2PAuthenticate.js";
import PaymentController from "../Controllers/PaymentController.js";
import AuthenticateAdmin from "../../Middleware/AuthenticateAdmin.js";


import express from "express";

var router = express.Router();

// User Frontend
router.get('/get',  P2PAuthenticate, PaymentController.getType); 
router.post('/create-payment', Authenticate, P2PAuthenticate, PaymentController.createPaymentMethod);
router.post('/payment-status', Authenticate, P2PAuthenticate, PaymentController.updatePaymentStatus);
router.get('/all-payment', Authenticate, P2PAuthenticate, PaymentController.getPaymentMethod);
router.get('/getById', Authenticate, P2PAuthenticate, PaymentController.getbyId); 

// Admin Panel
router.post('/create', AuthenticateAdmin, P2PAuthenticate, PaymentController.createType);
router.post('/verify', AuthenticateAdmin, P2PAuthenticate, PaymentController.verifyPaymentMethod);
router.get('/all', AuthenticateAdmin, P2PAuthenticate, PaymentController.getAllPaymentMethods);


export const PaymentRoutes = router;