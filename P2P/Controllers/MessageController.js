import reply from "../../Common/reply.js";
import _ from "lodash";
import { P2P_Msg } from "../Models/P2P_Msg.js";
import CValidator from "../../Validator/CustomValidation.js";
import { P2P_Trade } from "../Models/P2P_Trade.js";
import { P2P_Order } from "../Models/P2P_Order.js";
import { User } from "../../Models/User.js";
import { Op } from "sequelize";


const get_msg = async (req, res) => {

    let request = req.query;

    let { status, message } = await CValidator(request, {
        trade_id: "required|integer|exists:p2p_trades,id"
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let t = await P2P_Trade.findOne({
        where: {
            id: request.trade_id,
            [Op.or]: [
                {
                    buyer_user_id: req.user.id
                },
                {
                    seller_user_id: req.user.id
                }
            ]
        },
        attributes: ["id", "status"],
        include: [
            {
                model: P2P_Order,
                as: 'buyer',
                attributes: ["id", "user_id"],
                include: [
                    { model: User, attributes: ["name", "email"] }
                ]
            },
            {
                model: P2P_Order,
                as: 'seller',
                attributes: ["id", "user_id", "updated_at"],
                include: [
                    { model: User, attributes: ["name", "email"] }
                ]
            },
        ]
    });

    if (!t) {
        return res.send(reply.failed('Invalid Data!'));
    }


    try {
        let m = await P2P_Msg.findAll({ where: { trade_id: request.trade_id } });
        return res.send(reply.success("All Messages fetched successfully.", m, { trade: t }));
    } catch (err) {
        return res.send(reply.failed("Unable to fetch Messages at this time."))
    }

}


export default {
    get_msg,
}