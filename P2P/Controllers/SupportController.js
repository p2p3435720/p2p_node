import reply from "../../Common/reply.js";
import CValidator from "../../Validator/CustomValidation.js";
import { P2P_Wallet } from "../Models/P2P_Wallet.js";
import _ from "lodash";
import { Sequelize } from "sequelize";
const { Op } = Sequelize;
import { P2p_dispute_chat } from "../Models/P2p_dispute_chat.js";
import { P2p_Appeal } from "../Models/P2p_Appeal.js";
import { APPEAL_STATUS, ORDER_STATUS, P2P_WALLET_EVENT } from "../Common/GVariables.js";
import { P2P_Trade } from "../Models/P2P_Trade.js";
import { CL } from "cal-time-stamper";
import { User } from "../../Models/User.js";
import { P2P_Order } from "../Models/P2P_Order.js";
import { Token } from "../../Models/Token.js";

import p2pevent from "../Events/myp2pevent.js";

const comment_create = async (req, res) => {
    let request = req.body;
    //custom validation
    let { status, message } = await CValidator(request, {
        comment: "required|string",
        // trade_id: "required|exists:p2p_appeals,trade_id", 
        appeal_id: "required|exists:p2p_appeals,id",
        receiver_id: "required|exists:users,id"
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let chk_status = await P2p_Appeal.findOne({
        where: {
            id: request.appeal_id,
            status: {
                [Op.or]: [APPEAL_STATUS.processing, APPEAL_STATUS.closed]
            }
        }
    })
    if (!chk_status) {
        return res.send(reply.failed("Either appeal not in processing or not found"))
    }
    request.sender_id = req.user.id;
    request.image = req?.filedata?.message || "";
    request.trade_id = chk_status.trade_id;
    try {
        await P2p_dispute_chat.create(request);
        return res.json(reply.success(" Comment Created Successfully"))
    } catch (err) {
        console.log(err)
        return res.json(reply.failed("Unable to create Ticket comment"))
    }
}

const comment_get = async (req, res) => {
    let request = req.query;

    //custom validation
    let { status, message } = await CValidator(request, {
        sender_id: "required",
        receiver_id: "required",
        appeal_id: "required"
    });

    let { receiver_id, sender_id } = request;

    if (!status) {
        return res.send(reply.failed(message));
    }
    try {
        let result = await P2p_dispute_chat.findAll({
            where: {
                [Op.and]: [
                    { appeal_id: request.appeal_id },
                    {
                        [Op.or]: [
                            { receiver_id: sender_id },
                            { receiver_id }
                        ]
                    },
                    {
                        [Op.or]: [
                            { sender_id: receiver_id },
                            { sender_id }
                        ]
                    }
                ]
            },
        })
        return res.json(reply.success("Comment Fetched Successfully", result));
    } catch (err) {
        console.log(err)
        return res.json(reply.failed("unable to fetch ticket comment data!"));
    }
}

const appeal_win = async (req, res) => {
    let request = req.body;
    let validation = await CValidator(request, {
        "id": "required|exists:p2p_appeals,id",
        "appeal_win": "required|in:buyer,seller",
    });

    if (!validation.status) {
        return res.json(reply.failed(validation.message));
    }
    try {

        let appeal = await P2p_Appeal.findOne({ where: { id: request.id, status: APPEAL_STATUS.processing } });

        if (!appeal) {
            return res.json(reply.failed('Either appeal not found or status not in processing'));
        }

        let { appeal_win } = request;
        let { trade_id } = appeal;
        const trade = await P2P_Trade.findOne({
            where: {
                id: trade_id,
                status: ORDER_STATUS.disputed
            },
            include: [
                {
                    model: P2P_Order,
                    as: 'buyer',
                    attributes: ["id", "user_id", "currency", "with_currency", "pending_qty", "at_price", "created_at"]
                },
                {
                    model: P2P_Order,
                    as: 'seller',
                    attributes: ["id", "user_id", "currency", "with_currency", "pending_qty", "at_price", "created_at"]
                },
            ]
        });
        if (!trade) {
            return res.send(reply.failed("Trade not found."))
        }

        await P2P_fund_Set({ appeal_win, trade });
        appeal.appeal_win = request.appeal_win;
        appeal.status = APPEAL_STATUS.closed;
        await appeal.save();
        return res.send(reply.success("Appeal Submitted Successfully"));
    } catch (error) {
        console.log(error)
        return res.send(reply.failed("Unable to close appeal."))
    }
}

const send_crypto_s_to_b = async ({ deduct_from, credit_to, quantity, currency }) => {
    let seller = await P2P_Wallet.findOne({ where: { user_id: deduct_from, currency } });

    if (!seller) {
        return reply.failed(`seller not found!!`);
    }

    if (parseFloat(seller.freeze_balance) < parseFloat(quantity)) {
        return reply.failed(`seller don't have enough balance!!`);
    }
    // buyer wallet
    let buyer = await P2P_Wallet.findOne({ where: { user_id: credit_to, currency: currency } });


    // update seller locked balance
    let fz = CL.sub(seller.freeze_balance, quantity);
    seller.freeze_balance = fz;
    await seller.save();

    if (buyer) {
        let bal = CL.add(buyer.balance, quantity);
        buyer.balance = bal;
        await buyer.save();
    } else {
        await P2P_Wallet.create({
            'user_id': credit_to,
            'currency': currency,
            'balance': quantity
        });
    }

    await p2pevent.emit("P2PWalletEvent", {
        data: {
            user_id: seller.user_id,
            currency: seller.currency,
            amount: seller.freeze_balance,
            comment: `Unfreeze ${seller.freeze_balance} ${seller.currency} from p2p wallet`
        },
        type: P2P_WALLET_EVENT.unfreeze
    });

    await p2pevent.emit("P2PWalletEvent", {
        data: {
            user_id: seller.user_id,
            currency: seller.currency,
            amount: seller.freeze_balance,
            comment: `Credit ${seller.freeze_balance} ${seller.currency} to p2p wallet`
        },
        type: P2P_WALLET_EVENT.credit
    });
}

const P2P_fund_Set = async ({ appeal_win, trade }) => {
    let t = trade;
    if (appeal_win == "buyer") {
        await send_crypto_s_to_b({ deduct_from: t.seller_user_id, credit_to: t.buyer_user_id, quantity: t.quantity, currency: t.buyer.currency })

        await User.update({ status: 0 }, {
            where: { id: t.seller_user_id }
        })



        await Token.destroy({
            where: { user_id: t.seller_user_id }
        })

        //
        console.log(t.buyer.pending_qty, t.quantity, parseFloat(t.buyer.pending_qty) > parseFloat(t.quantity));
        if (parseFloat(t.buyer.pending_qty) > parseFloat(t.quantity)) {
            let p_qty = CL.sub(t.buyer.pending_qty, t.quantity);
            let up_data = {
                status: ORDER_STATUS.partially_completed,
                pending_qty: p_qty,
                search_max_qty: CL.mul(t.buyer.at_price, p_qty)
            }
            await P2P_Order.update(up_data, {
                where: {
                    id: t.buyer_order_id,

                }
            })
        } else {

            await P2P_Order.update({ status: ORDER_STATUS.completed, pending_qty: 0 }, {
                where: {
                    id: t.buyer_order_id
                }
            })
        }

        await P2P_Order.update({ status: ORDER_STATUS.canceled }, {
            where: {
                id: t.seller_order_id
            }
        })
    }
    if (appeal_win == "seller") {

        await User.update({ status: 0 }, {
            where: { id: t.buyer_user_id }
        })

        //seller wallet update with order cancel
        let seller_wallet = await P2P_Wallet.findOne({
            where: {
                user_id: t.seller_user_id,
                currency: t.seller.currency
            }
        });

        if (seller_wallet) {
            await p2pevent.emit("P2PWalletEvent", {
                data: {
                    user_id: seller_wallet.user_id,
                    currency: seller_wallet.currency,
                    amount: seller_wallet.freeze_balance,
                    comment: `Unfreeze ${seller_wallet.freeze_balance} ${seller_wallet.currency} from p2p wallet`
                },
                type: P2P_WALLET_EVENT.unfreeze
            });

            await p2pevent.emit("P2PWalletEvent", {
                data: {
                    user_id: seller_wallet.user_id,
                    currency: seller_wallet.currency,
                    amount: seller_wallet.freeze_balance,
                    comment: `Credit ${seller_wallet.freeze_balance} ${seller_wallet.currency} to p2p wallet`
                },
                type: P2P_WALLET_EVENT.credit
            });
        }

        //seller wallet update with order cancel

        await P2P_Order.update({ status: ORDER_STATUS.canceled }, {
            where: {
                user_id: t.seller_user_id
            }
        })



        await P2P_Order.update({ status: ORDER_STATUS.canceled }, {
            where: {
                user_id: t.buyer_user_id
            }
        })
    }
}

export default {
    comment_create,
    comment_get,
    appeal_win
}