import reply from "../../Common/reply.js";
import { P2p_Appeal } from "../Models/P2p_Appeal.js";
import { User } from "../../Models/User.js";
import { Op } from "sequelize";
import Helper from "../../Common/Helper.js";
import { P2P_Trade } from "../Models/P2P_Trade.js";
import { P2P_Order } from "../Models/P2P_Order.js";

import CValidator from "../../Validator/CustomValidation.js";

const get_appeal = async (req, res)  => {
    // const { name, email, appeal_by, reason, status, created_at, sortby, sortbyname } = req.query;
    const { appeal_by, reason, status, created_at, sortby, sortbyname } = req.query;

    let validation = await CValidator(req.query, {
        page: "integer|gt:0",
        per_page: `integer|gt:0`,
    });

    if (!validation.status) {
        return res.send(reply.failed(validation.message));
    }

    var condition = {
        where: {},
        include: [
            { model: User, attributes: ["id", "name", "email"] },
            {
                model: P2P_Trade,
                attributes: ["id", "buyer_confirmation", "seller_confirmation", "at_price", "quantity", "status"],
                include: [
                    {
                        model: P2P_Order,
                        as: 'buyer',
                        attributes: ["id", "user_id", "status", "currency", "with_currency"],
                        include: [
                            { model: User, attributes: ["id", "name", "email"] }
                        ]
                    },
                    {
                        model: P2P_Order,
                        as: "seller",
                        attributes: ["id", "user_id", "status", "currency", "with_currency"],
                        include: [
                            { model: User, attributes: ["id", "name", "email"] }
                        ]
                    }
                ]
            }
        ]
    }
    if (appeal_by) {
        condition.where.appeal_by = appeal_by;
    }
    if (reason) {
        condition.where.reason = { [Op.substring]: reason };
    }
    if (status) {
        condition.where.status = status;
    }
    if (created_at) {
        condition.where.created_at = created_at;
    }
    try {
        const paginate = Helper.getPaginate(req, sortbyname, sortby);

        let finalquery = Object.assign(condition, paginate)

        let all_appeal = await P2p_Appeal.findAll(finalquery);

        let total_count = await P2p_Appeal.count(condition);

        all_appeal = reply.paginate(paginate.page, all_appeal, paginate.limit, total_count);

        return res.send(reply.success("All appeals fetched successfully.", all_appeal))
    } catch (error) {
        console.log(error);
        return res.send(reply.failed("Unable to fetch appeals at this time."))
    }
}

export default {
    get_appeal
}