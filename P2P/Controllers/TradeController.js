import reply from "../../Common/reply.js";
import { P2P_Order } from "../Models/P2P_Order.js";
import CValidator from "../../Validator/CustomValidation.js";
import { ORDER_STATUS, P2P_WALLET_EVENT } from "../Common/GVariables.js";
import _ from "lodash";
import { Kyc_status } from "../Common/GFunctions.js";
import { P2P_Wallet } from "../Models/P2P_Wallet.js";
import { Sequelize } from "sequelize";
import p2pevent from "../Events/myp2pevent.js";
import { P2P_PaymentDetail } from "../Models/P2P_PaymentDetail.js";
import { CL } from "cal-time-stamper";
const { Op } = Sequelize;
import { P2P_Trade } from "../Models/P2P_Trade.js";
import { User } from "../../Models/User.js";
import { P2p_Appeal } from "../Models/P2p_Appeal.js";
import { P2P_Feedback } from "../Models/P2P_Feedback.js";
import Helper from "../../Common/Helper.js";


const p2p_trade = async (old_order, new_order, { buyer_user_id, seller_user_id }) => {

    let expired_at = new Date().setMinutes(new Date().getMinutes() + 15);

    let trade_data = {
        buyer_order_id: old_order.order_type == "BUY" ? old_order.id : new_order.id,
        seller_order_id: old_order.order_type == "SELL" ? old_order.id : new_order.id,
        at_price: old_order.at_price,
        quantity: new_order.pending_qty,
        expired_at,
        buyer_user_id,
        seller_user_id,
        currency:old_order.currency,
        with_currency:old_order.with_currency
    }

    return await P2P_Trade.create(trade_data);
}

const place_trade = async (req, res) => {

    try {

        let request = req.body;

        let { status, message } = await CValidator(request, {
            currency: "required|exists:p2p_currencies,currency",
            with_currency: `required|exists:p2p_fiat_currencies,currency`, // fiat curriencies types
            order_id: "required|exists:p2p_orders,id",
            order_type: "required|in:BUY,SELL",
            quantity: "required|numeric|gt:0",
            payment_type: `required_if:order_type,SELL|inFiatArray:${request.with_currency}`, // payment types
        });

        if (!status) {
            return res.send(reply.failed(message));
        }

        request.user_id = req.user.id;

        request.pending_qty = request.quantity;
        request.min_qty = request.quantity;
        request.max_qty = request.quantity;


        // Kyc is Approved or not
        let kyc = await Kyc_status(request.user_id);
        if (kyc.status_code == "0") {
            return res.send(reply.failed(kyc.message));
        }
        request.country = kyc.data;

        // USER CAN PLACE ONE ORDER AT A TIME
        let OrderAlreadyPlaced = await P2P_Order.count({
            where: {
                user_id: request.user_id,
                status: {
                    [Op.notIn]: [ORDER_STATUS.canceled, ORDER_STATUS.completed, ORDER_STATUS.expired, ORDER_STATUS.disputed]
                }
            }
        });

        if (OrderAlreadyPlaced != 0) {
            return res.json(reply.failed('Previous Order still in processing or placed.'));
        }


        // ORDER ACTUALLY MATCHES THE CONDITIONS ? 
        let order_exist = await P2P_Order.findOne({
            where: {
                id: request.order_id,
                currency: request.currency,
                with_currency: request.with_currency,
                order_type: {
                    [Op.not]: request.order_type
                },
                status: {
                    [Op.notIn]: [ORDER_STATUS.canceled, ORDER_STATUS.completed, ORDER_STATUS.processing, ORDER_STATUS.expired, ORDER_STATUS.disputed]
                },
                // 50 30
                min_qty: { [Op.lte]: parseFloat(request.quantity) },
                pending_qty: { [Op.gte]: parseFloat(request.quantity) }
            },
            attributes: { exclude: ["reason"] }
        });

        if (!order_exist) {
            return res.send(reply.failed("Order does not exist."))
        }



        request.at_price = order_exist.at_price;

        request.search_min_qty = CL.mul(request.min_qty, request.at_price); // set search min qty*price
        request.search_max_qty = CL.mul(request.pending_qty, request.at_price); // set search pending qty*price

        let buyer_user_id;
        let seller_user_id;

        if (request.order_type == "SELL") {

            buyer_user_id = order_exist.user_id;
            seller_user_id = request.user_id;

            // verifying multiple payment methods ->
            let payMethodStatus = await P2P_PaymentDetail.count({
                where: {
                    user_id: request.user_id,
                    is_verify: 1, // activated by admin
                    status: 1,
                    payment_slug: {
                        [Op.in]: request.payment_type //GPAY, ICIC BANK
                    },
                    currency:request.with_currency
                },
                attributes: ['id']
            });

            if (parseFloat(payMethodStatus) != parseFloat(request.payment_type.length)) {
                return res.send(reply.failed("Payment method is not active."));
            }

            let total_balance = await P2P_Wallet.count({
                where: {
                    user_id: request.user_id,
                    currency: request.currency,
                    balance: {
                        [Op.gte]: [parseFloat(request.quantity)]
                    },
                }
            });

            //check if user don't have wallet or sufficent money.. 
            if (!total_balance) {
                return res.json(reply.failed("You have no sufficient balance to sell"));
            }

            // freezed currency in p2p wallet
            p2pevent.emit("P2PWalletEvent", {
                data: {
                    user_id: request.user_id,
                    currency: request.currency,
                    amount: request.quantity,
                    comment: `Freeze ${request.quantity} ${request.currency} by placed sell order in p2p`
                },
                type: P2P_WALLET_EVENT.freeze
            });
        }

        if (request.order_type == "BUY") {
            seller_user_id = order_exist.user_id
            buyer_user_id = request.user_id
        }

        //NEW ORDER CREATED NOW 
        let new_order = await P2P_Order.create(request);

        //CREATE AN ENTRY IN P2P TRADE TABLE 
        let trade = await p2p_trade(order_exist, new_order, { seller_user_id, buyer_user_id })

        //UPDATE ORDER STATUS OF BOTH ORDERS
        if (trade) {
            order_exist.status = ORDER_STATUS.processing;
            await order_exist.save();
            new_order.status = ORDER_STATUS.processing;
            await new_order.save()
        }

        let identity = trade?.id || 0;

        // console.log(reply.success("trade matched", identity));

        return res.send(reply.success("trade matched", identity));

    } catch (err) {
        console.log(err)
        return res.send(reply.failed("Unable to trade at this time."))
    }
}
const get_trade = async (req, res) => {

    try {

        let request = req.query;

        let { status, message } = await CValidator(request, {
            trade_id: "required|exists:p2p_trades,id",
        });

        if (!status) {
            return res.send(reply.failed(message));
        }

        request.user_id = req.user.id;



        // ORDER ACTUALLY MATCHES THE CONDITIONS ? 
        let order_exist = await P2P_Trade.findOne({
            where: {
                id: request.trade_id
            },
            include: [
                {
                    model: P2P_Feedback,
                    required: false,
                    where:{user_id:request.user_id},
                    as: 'feedback',
                    attributes: ['id', 'feedback_type', 'feedback_comment', 'created_at', "user_id"],
                    include: [
                        { model: User, attributes: ["name", "email"] }
                    ],
                },
                {
                    model: P2P_Order,
                    as: 'buyer',
                    attributes: ["id", "user_id", "created_at", "updated_at"],
                    include: [
                        { model: User, attributes: ["name", "email"] }
                    ]
                },
                {
                    model: P2P_Order,
                    as: 'seller',
                    attributes: ["id", "user_id", "payment_type","created_at"],
                    include: [{
                        model: User, attributes: ["name", "email"]
                    }]
                },
            ]
        });

        if (!order_exist) {
            return res.send(reply.failed("Order does not exist."))
        }


        let seller_user_id = order_exist.seller.user_id;
        let seller_payment_types = order_exist.seller.payment_type;

        let p2p_payment_details = await P2P_PaymentDetail.findAll({
            where:{
                user_id:order_exist.seller.user_id,
                payment_slug: {
                    [Op.in]:seller_payment_types
                },
                status:1,
                is_verify:1
            },
            attributes: ["id", "payment_slug", "payment_detail", "currency", "payment_type"]
        });

        order_exist.dataValues['seller'].dataValues['seller_payment_detail'] = p2p_payment_details;


        let Appeal_data = await P2p_Appeal.findAll({
            where: {
                trade_id: request.trade_id,
            },
            attributes: ["id", "appeal_by", "appeal_user_id", "cancelled_at", "expired_at", "reason", "status"],
            raw: true,
            order: [['id', 'DESC']]
        })

        if (Appeal_data && order_exist) {
            order_exist.dataValues.Appeal_data = Appeal_data;
        }

       

        return res.send(reply.success("trade fetched successfully", order_exist))


    } catch (err) {
        console.log(err)
        return res.send(reply.failed("Unable to trade at this time."))
    }
}


const get_trade_boss = async (req, res) => {
    let { sell_confirmation, status, page = 1, per_page = 10, buy_confirmation, sortbyname, sortby ,id,date} = req.query;

    
    var condition = { where:{}};
    if (buy_confirmation) {
        condition.where.buyer_confirmation = buy_confirmation;
    }
    if (sell_confirmation) {
        condition.where.seller_confirmation = sell_confirmation;
    }
    if (status) {
        condition.where.status = status;
    }
    if(id){
        condition.where.id = id;

    }
    if (date) {
        condition.where.created_at = date;
    }
    try {
        const paginate = Helper.getPaginate(req, sortbyname, sortby);
        let finalquery = Object.assign(condition, paginate)
        let all_trades = await P2P_Trade.findAll(finalquery);
        let total_count = await P2P_Trade.count(condition);
        let all_order = reply.paginate(paginate.page, all_trades, paginate.limit, total_count);

        return res.send(reply.success("All trades", all_order));
    } catch (err) {
        console.log(err)
        return res.send(reply.failed("Unable to trade at this time."))
    }
}

export default {
    place_trade, 
    get_trade,
    get_trade_boss
}