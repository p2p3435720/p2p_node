import reply from "../../Common/reply.js";
import { UserCrypto } from "../../Models/UserCrypto.js";
import { P2P_Wallet } from "../Models/P2P_Wallet.js";
import CValidator from "../../Validator/CustomValidation.js";
import _ from "lodash";
import { CL } from "cal-time-stamper";
import p2pevent from "../Events/myp2pevent.js";
import { P2P_WALLET_EVENT } from "../Common/GVariables.js";
import { } from "../Common/GFunctions.js";
import { Sequelize } from "sequelize";
const { Op } = Sequelize;


const spot_to_p2p = async (req, res) => {

    let request = req.body;

    let { status, message } = await CValidator(request, {
        amount: "required|numeric|gt:0",
        currency: "required"
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let uCrypto = await UserCrypto.findOne({
        where: {
            currency: request.currency,
            user_id: req.user.id,
            balance: {
                [Op.gte]: [parseFloat(request.amount)]
            },
        }
    });

    if (!uCrypto) {
        return res.send(reply.failed('You have no sufficient balance to transfer!!'));
    }

    // console.log(uCrypto);

    if (!_.gte(parseFloat(uCrypto.balance), parseFloat(request.amount))) {
        return res.send(reply.failed('Insufficent Balance!!'));
    }

    uCrypto.balance = CL.sub(uCrypto.balance, request.amount);
    await uCrypto.save();


    p2pevent.emit("P2PWalletEvent", {
        data: {
            user_id: req.user.id,
            currency: request.currency,
            amount: request.amount,
            comment: `Credit ${request.amount} ${request.currency} from spot wallet to p2p wallet`
        },
        type: P2P_WALLET_EVENT.credit
    });

    return res.send(reply.success('Fund Transferred from spot to funding account!!'));
}

const getp2pCurrency = async (req, res) => {


    if(req.query?.currency){
        let n = await P2P_Wallet.findOne({
            where: {
                user_id: req.user.id,
                currency:req.query?.currency
            },
            attributes: ['currency', 'balance', 'freeze_balance']
        });

        if(!n){
            let d = {currency:req.query?.currency, balance:0, freeze_balance: 0 };
            return res.send(reply.success('currencies fetched!!', d));
        }
    
        return res.send(reply.success('currencies fetched!!', n));
    }


    let n = await P2P_Wallet.findAll({
        where: {
            user_id: req.user.id
        },
        attributes: ['currency', 'balance', 'freeze_balance']
    });

    return res.send(reply.success('currencies fetched!!', n));
}

const getSpotCurrency = async (req, res) => {
    let n = await UserCrypto.findAll({
        where: {
            user_id: req.user.id
        },
        attributes: ['currency', 'balance', 'freezed_balance']
    });

    return res.send(reply.success('currencies fetched!!', n));
}


export default {
    spot_to_p2p,
    getp2pCurrency,
    getSpotCurrency
}