import reply from "../../Common/reply.js";
import { P2P_Order } from "../Models/P2P_Order.js";
import CValidator from "../../Validator/CustomValidation.js";
import { ORDER_STATUS, P2P_WALLET_EVENT, DEFAULT_FIAT_CURRENCIES } from "../Common/GVariables.js";
import _ from "lodash";
import { Kyc_status } from "../Common/GFunctions.js";
import { P2P_Wallet } from "../Models/P2P_Wallet.js";
import p2pevent from "../Events/myp2pevent.js";
import { P2P_PaymentDetail } from "../Models/P2P_PaymentDetail.js";
import { CL } from "cal-time-stamper";
import Helper from "../../Common/Helper.js";
import { Model, Sequelize } from '../../Database/sequelize.js';
const { Op, QueryTypes } = Sequelize;
import { User } from "../../Models/User.js";

import { P2P_Trade } from "../Models/P2P_Trade.js";
import { startOfDay, endOfDay } from "date-fns";

import myCache from "../NodeCache/cache.js";
import { P2p_Currency } from "../Models/P2p_Currency.js";
import variables from "../../Config/variables.js";

const getCountries = (req, res) => {
    let cc = myCache.get('p2p_countries') ?? null;
    return res.send(reply.success('Curriencies Fetched Successfully!!', cc));
}

import { SYMBOL_ISO_CODE, FIAT_DECIMAL } from "../Common/StaticObject.js";

function isEmptyEl(array, i = 0) {
    return !(array[i]);
}

const validateRegion = (validateArray = []) => {
    if (!Array.isArray(validateArray)) {
        return { s: false, m: 'The regions should be array.' };
    }

    if (isEmptyEl(validateArray)) {
        return { s: false, m: 'The regions must not be empty.' };
    }

    // only 10 countries allowed
    if (validateArray.length > 10 || validateArray.length <= 0) {
        return { s: false, m: 'The regions min limit is 1 and max limit is 10.' };
    }

    let attribute = myCache.get('validate_p2p_countries') ?? [];

    let aa = _.find(validateArray, (v) => {
        if (!attribute.includes(v)) {
            return v;
        }
    });

    if (aa) {
        return { s: false, m: 'The regions is not supported.' };
    }

    return { s: true, m: 'The regions is supported.' };
}


// get current datetime and previous datetime. Just need to send total days minus from current date, ex: back to 10 days
// output will be {current date , current date - 10 days}
const getdates = (pdays) => {
    let end = new Date();
    let start = new Date();

    start.setDate(start.getDate() - pdays);  // current day - pdays


    start = start.toISOString().split("T")[0];

    end = end.toISOString().split("T")[0];

    return ({ start, end });
}

const place_order = async (req, res) => {


    try {
        let request = req.body;
        let { status, message } = await CValidator(request, {
            currency: "required|exists:p2p_currencies,currency",
            with_currency: `required|exists:p2p_fiat_currencies,currency`, // fiat curriencies types
            at_price: "required|numeric|gt:0",
            order_type: "required|in:BUY,SELL",
            min_qty: "required|numeric|gt:0",
            max_qty: `required|numeric|gte:${request.min_qty}`,
            payment_type: `required_if:order_type,SELL|inFiatArray:${request.with_currency}`, // payment types
            terms_and_conditions: "string|max:5000",
            // regions: `required`
        });

        if (!status) {
            return res.send(reply.failed(message));
        }

        // let vr = validateRegion(request.regions); // validating region

        // if (!vr.s) {
        //     return res.send(reply.failed(vr.m));
        // }

        request.regions = ["India"]

        request.user_id = req.user.id; // set user id

        // Kyc is Approved or not
        let kyc = await Kyc_status(request.user_id);


        if (kyc.status_code == "0") {
            return res.send(reply.failed(kyc.message));
        }

        request.country = kyc.data; // current user country

        request.pending_qty = request.max_qty; // set pending qty
        request.search_min_qty = CL.mul(request.min_qty, request.at_price); // set search min qty*price
        request.search_max_qty = CL.mul(request.pending_qty, request.at_price); // set search pending qty*price
        

        // USER CAN PLACE ONE ORDER AT A TIME
        let OrderAlreadyPlaced = await P2P_Order.count({
            where: {
                user_id: request.user_id,
                status: {
                    [Op.notIn]: [ORDER_STATUS.canceled, ORDER_STATUS.completed, ORDER_STATUS.expired, ORDER_STATUS.disputed]
                }
            }
        });

        if (OrderAlreadyPlaced != 0) {
            return res.json(reply.failed('Previous Order still in processing or placed.'));
        }

        if (request.order_type == "SELL") {

            // Currently we are using more than 1 fiat currency like => INR,EUR
            // verifying multiple payment methods ->
            let payMethodStatus = await P2P_PaymentDetail.count({
                where: {
                    currency: request.with_currency,
                    user_id: request.user_id,
                    is_verify: 1, // activated by admin
                    status: 1,
                    payment_slug: {
                        [Op.in]: request.payment_type //g_pay, bank_transfer
                    }
                },
                attributes: ['id']
            });

            if (parseFloat(payMethodStatus) != parseFloat(request.payment_type.length)) {
                return res.send(reply.failed("Payment method is not active."));
            }


            let total_balance = await P2P_Wallet.count({
                where: {
                    user_id: request.user_id,
                    currency: request.currency,
                    balance: {
                        [Op.gte]: [parseFloat(request.max_qty)]
                    },
                }
            });

            //check if user don't have wallet or sufficent money.. 
            if (!total_balance) {
                return res.json(reply.failed("You have no sufficient balance to sell"));
            }


        }

        let o = await P2P_Order.create(request);

        // IF sell order placed freze seller crypto in p2p_wallet
        if (o && request.order_type == "SELL") {
            // freezed currency in p2p wallet
            p2pevent.emit("P2PWalletEvent", {
                data: {
                    user_id: request.user_id,
                    currency: request.currency,
                    amount: request.max_qty,
                    comment: `Freeze ${request.max_qty} ${request.currency} by placed sell order in p2p`
                },
                type: P2P_WALLET_EVENT.freeze
            });
        }


        return res.send(reply.success('Order Placed Successfully.'));
    } catch (error) {
        console.log(error);
        return res.send(reply.success('Unable to create Order at this time.'));
    }
}

// const cancel_order = async (req, res) => {
//     try {
//         let request = req.body;

//         let { status, message } = await CValidator(request, {
//             order_id: "required|integer|exists:p2p_orders,id",
//             reason: `required|max:255`, // fiat curriencies types
//         });

//         if (!status) {
//             return res.send(reply.failed(message));
//         }

//         let o = await P2P_Order.findOne({
//             where: {
//                 id: request.order_id,
//                 user_id: req.user.id,
//                 status: {
//                     [Op.in]: [ORDER_STATUS.partially_completed, ORDER_STATUS.placed]
//                 }
//             }
//         });


//         if (!o) {
//             return res.send(reply.failed('Invalid Data!!'));
//         }

//         o.status = ORDER_STATUS.canceled; // order cancelled
//         o.reason = request.reason; // cancelled Order Reason
//         o.cancelled_at = new Date(Date.now()); // cancelled Order Date Time


//         if (o.order_type == "SELL") {
//             // return pending qty to user p2p wallet
//             p2pevent.emit("P2PWalletEvent", {
//                 data: {
//                     user_id: o.user_id,
//                     currency: o.currency,
//                     amount: o.pending_qty,
//                     comment: `Credit ${o.pending_qty} ${o.currency} by p2p sell order cancelled!`
//                 },
//                 type: P2P_WALLET_EVENT.cancel
//             });
//         }


//         await o.save();

//         return res.send(reply.success("Order cancelled!!"));
//     } catch (error) {
//         console.log(error);
//         return res.send(reply.success("Unable to cancel order at this time!!"));
//     }
// }

const cancel_order = async (req, res) => {
    try {
        let { order_id } = req.query
        let request = req.query;

        let { status, message } = await CValidator(request, {
            order_id: "required|integer|exists:p2p_orders,id",
            // reason: `required|max:255`, 
            // fiat curriencies types
        });

        if (!status) {
            return res.send(reply.failed(message));
        }

        let o = await P2P_Order.findOne({
            where: {
                id: order_id,
                user_id:req.user.id,
                status: {
                    [Op.in]: [ORDER_STATUS.partially_completed, ORDER_STATUS.placed]
                }
            }
        });


        if (!o) {
            return res.send(reply.failed('Invalid Data!!'));
        }

        o.status = ORDER_STATUS.canceled; // order cancelled
        o.reason = request.reason; // cancelled Order Reason
        o.cancelled_at = new Date(Date.now()); // cancelled Order Date Time


        if (o.order_type == "SELL") {
            // return pending qty to user p2p wallet
            p2pevent.emit("P2PWalletEvent", {
                data: {
                    user_id: o.user_id,
                    currency: o.currency,
                    amount: o.pending_qty,
                    comment: `Credit ${o.pending_qty} ${o.currency} by p2p sell order cancelled!`
                },
                type: P2P_WALLET_EVENT.cancel
            });
        }


        await o.save();

        return res.send(reply.success("Order cancelled!!"));
    } catch (error) {
        console.log(error);
        return res.send(reply.success("Unable to cancel order at this time!!"));
    }
}
const get_order = async (req, res) => {
    console.log("hello");
    let { order_type, country, page = 1, per_page = 10, currency, with_currency, payment_type, region, amount, sortbyname, sortby } = req.query;

    let { status, message } = await CValidator(req.query, {
        page: "integer|gt:0",
        per_page: `integer|gt:0`,
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    // Order status filter
    var condition = {
        where: {
            status: { [Op.notIn]: [ORDER_STATUS.canceled, ORDER_STATUS.completed, ORDER_STATUS.processing, ORDER_STATUS.expired, ORDER_STATUS.disputed] }
        },
        attributes: ['id', 'user_id', 'country', 'at_price', 'currency', 'with_currency', 'order_type', 'min_qty', 'max_qty','search_min_qty','search_max_qty', ['pending_qty', 'available_qty'], 'terms_and_conditions', 'created_at', 'payment_type', 'regions'],
        include: [{ model: User, attributes: ["id", "name", "lname", "email"] }]
    };

    order_type = (order_type == "BUY") ? "SELL" : "BUY";

    if (order_type) {
        condition.where.order_type = order_type;
    }

    if (country) {
        condition.where.country = country;
    }

    if (currency) {
        condition.where.currency = currency;
    }

    with_currency = (with_currency == null) ? DEFAULT_FIAT_CURRENCIES : with_currency; // INR

    if (with_currency) {
        condition.where.with_currency = with_currency;
    }

    if (payment_type) {
        condition.where.payment_type = { [Op.like]: `%${payment_type}%` }
    }

    if (region) {
        condition.where.regions = { [Op.like]: `%${region}%` }
    }

    if (amount) {
        condition.where.search_max_qty = { [Op.gte]: parseFloat(amount) }
        condition.where.search_min_qty = { [Op.lte]: parseFloat(amount) }
    }

    try {
        const paginate = Helper.getPaginate(req, sortbyname, sortby);

        let finalquery = Object.assign(condition, paginate)

        let all_order = await P2P_Order.findAll(finalquery);

        let total_count = await P2P_Order.count(condition);

        all_order = reply.paginate(paginate.page, all_order, paginate.limit, total_count);

        return res.send(reply.success("All p2p orders fetched successfully.", all_order))
    } catch (err) {
        console.log(err);
        return res.send(reply.failed("Unable to fetch orders at this time."))
    }

}

const User_P2p_Profile = async (req, res) => {
    let { status, message } = await CValidator(req.query, {
        user_id: "required|integer|exists:users,id"
    });

    let { user_id } = req.query

    if (!status) {
        return res.send(reply.failed(message));
    }

    try {

        let { start, end } = getdates(30);

        let user = await User.findOne({
            where: {
                id: user_id
            },
            attributes: ['name', 'email_verified_at', "mobile_verified_at", "user_verify"],
        });


        // This query getting all trades , buy_trades, sell_trades, 30daysTrade for a specific user!!

        let query = `SELECT COUNT(id) as totalOrder,
        (SELECT Count(id) FROM p2p_trades WHERE buyer_user_id = ${user_id}  AND status != '${ORDER_STATUS.processing}')  as otype_buy,
        (SELECT Count(id) FROM p2p_trades WHERE seller_user_id = ${user_id} AND status != '${ORDER_STATUS.processing}') as otype_sell,
        (SELECT Count(id) FROM p2p_trades WHERE DATE(created_at) >= '${start}' AND DATE(created_at) <= '${end}' AND status='${ORDER_STATUS.completed}' AND (buyer_user_id = ${user_id} OR seller_user_id=${user_id})) as thirtyDaysOrder
        FROM p2p_trades
        WHERE buyer_user_id = ${user_id} OR seller_user_id=${user_id} AND status != '${ORDER_STATUS.processing}'`;

        const [rr] = await Model.query(query, { type: QueryTypes.SELECT });

        let { totalOrder = 0, otype_buy = 0, otype_sell = 0, thirtyDaysOrder = 0 } = rr;


        let percent = thirtyDaysOrder == 0 ? 0 : CL.mul(100, (CL.div(thirtyDaysOrder, totalOrder)));

        let box = [
            {
                label: "All Trades",
                value: totalOrder,
                value_in: "Time(s)",
                total_buy: otype_buy,
                total_sell: otype_sell,
                text: 'Toal Number of P2P completed orders(buy and sell included)'
            },
            {
                label: "30d Trades",
                value: thirtyDaysOrder,
                value_in: "Time(s)",
                text: 'Toal Number of P2P completed orders(buy and sell included) in past 30 days'
            },
            {
                label: "30d Completion Rate",
                value: percent,
                value_in: "%",
                text: 'Completion Rate = Trades/Orders(30d)*100%'
            },
        ]
        return res.send(reply.success("All p2p orders fetched successfully.", { user, box }))



    } catch (err) {
        console.log(err);
        return res.send(reply.failed("Unable to fetch orders at this time."))
    }
}

const order_crypto = async (req, res) => {
    try {
        let request = req.query;

        let { status, message } = await CValidator(request, {
            with_currency: `required|exists:p2p_fiat_currencies,currency`, // fiat curriencies types
        });

        if (!status) {
            return res.send(reply.failed(message));
        }

        let result = await P2P_Order.findAll({
            where: {
                with_currency: request.with_currency,
                status: { [Op.notIn]: [ORDER_STATUS.canceled, ORDER_STATUS.completed, ORDER_STATUS.processing, ORDER_STATUS.expired, ORDER_STATUS.disputed] }
            },
            attributes: ['currency'],
            group: ['currency'],
            raw: true
        }).then(v => v.map(v => v.currency));


        let dl = {};

        await P2p_Currency.findAll({attributes: ['currency', 'decimal_length'], raw: true}).then(v => v.map(v => {
           Object.assign(dl, {[v.currency] : v.decimal_length});
        }));

        let rr = [];

        _.map(result, (v,i) => {
            rr.push({
                currency:v,
                decimal: dl[v] || 0,
                image: variables.node_url + "p2p/image/crypto/" + v + ".png"
            })
        })

        return res.send(reply.success("Fetched Currencies.", rr));
    } catch (error) {
        console.log(error);
        return res.send(reply.failed("unable to fetch at this time."));
    }
}

const order_history = async (req, res) => {

    let { order_type, currency, status, startDate, endDate  } = req.query;

    let validation = await CValidator(req.query, {
        page: "integer|gt:0",
        per_page: `integer|gt:0`,
    });

    if (!validation.status) {
        return res.send(reply.failed(validation.message));
    }

    var condition = {
        where: { user_id: req.user.id },

        include: [
            {
                model: User,
                attributes: ["id", "name", "email"],
            },
            {
                model: P2P_Trade,
                as: 'seller',
                attributes: ['id', 'seller_order_id', 'buyer_order_id'],


            },
            {
                model: P2P_Trade,
                as: 'buyer',
                attributes: ['id', 'seller_order_id', 'buyer_order_id'],
            }],
        order: [
            // ...we use the same syntax from the include
            // in the beginning of the order array
            [['id', 'DESC']],
            [{ model: P2P_Trade, as: 'seller' }, 'id', 'DESC'],
            [{ model: P2P_Trade, as: 'buyer' }, 'id', 'DESC']

        ]
    };

    if (order_type) {
        condition.where.order_type = { [Op.substring]: order_type };
    }
    if (currency) {
        condition.where.currency = { [Op.substring]: currency };
    }
    if (status) {
        condition.where.status = { [Op.substring]: status }
    }

    if (startDate && endDate) {
        startDate = new Date(startDate);
        endDate = new Date(endDate);

        condition.where.created_at = {
            [Op.between]: [startOfDay(startDate), endOfDay(endDate)]
        }
    }


    try {

        const page = req.query.page ? parseInt(req.query.page) : 1;
        const per_page = req.query.per_page ? parseInt(req.query.per_page) : 10;
        const offset = (page - 1) * per_page;
        let paginate = { offset: offset, limit: per_page, page: page }

        let finalquery = Object.assign(condition, paginate)

        let all_order = await P2P_Order.findAll(finalquery);

        let total_count = await P2P_Order.count(condition);

        all_order = reply.paginate(paginate.page, all_order, paginate.limit, total_count);

        return res.send(reply.success("p2p orders History successfully.", all_order))
    } catch (err) {
        console.log(err);
        return res.send(reply.failed("Unable to fetch orders at this time."))
    }

}

const trade_history = async (req, res) => {

    let { order_type, currency, status, start_date, end_date } = req.query;

    var condition = {
        where: {
            [Op.or]: [
                {
                    buyer_user_id: req.user.id
                },
                {
                    seller_user_id: req.user.id
                }
            ]
        }, raw: true,
        order: [
            // ...we use the same syntax from the include
            // in the beginning of the order array
            [['id', 'DESC']],


        ]
    };
    // if (order_type) {
    //     condition.where.order_type = { [Op.substring]: order_type };
    // }
    if (currency) {
        condition.where.currency = { [Op.substring]: currency };
    }
    if (status) {
        condition.where.status = { [Op.substring]: status }
    }

    if(start_date && end_date){
        condition.where.created_at = { [Op.between]: [start_date, end_date]}
    }

    try {

        const page = req.query.page ? parseInt(req.query.page) : 1;
        const per_page = req.query.per_page ? parseInt(req.query.per_page) : 10;
        const offset = (page - 1) * per_page;
        let paginate = { offset: offset, limit: per_page, page: page }

        let finalquery = Object.assign(condition, paginate)

        let all_order = await P2P_Trade.findAll(finalquery);
        let total_count = await P2P_Trade.count(condition);

        all_order.map((v) => {
            if (v.buyer_user_id == req.user.id) { 
                v.order_type = 'Buy';
                v.counter_party = v.seller_user_id;
            }
            else { 
                v.order_type = 'Sell';   
                v.counter_party = v.buyer_user_id;
            }
            v.c_image = variables.node_url + "p2p/image/crypto/" + v.currency + ".png";
            v.f_image = variables.node_url + "p2p/image/fiat/" + v.with_currency + ".png";
            return v
        });

        if(order_type){
            all_order = all_order.filter((v) =>v.order_type == order_type);
        }

        all_order = reply.paginate(paginate.page, all_order, paginate.limit, total_count);

        return res.send(reply.success("p2p orders History successfully.", all_order))
    } catch (err) {
        console.log(err);
        return res.send(reply.failed("Unable to fetch orders at this time."))
    }

}

// Admin Methods:
const get_order_boss = async (req, res) => {
    console.log("hello");
    let { order_type, country, max_qty, pending_qty, currency, with_currency, payment_type, status, amount, sortbyname, sortby, name } = req.query;

    let validation = await CValidator(req.query, {
        page: "integer|gt:0",
        per_page: `integer|gt:0`,
    });

    if (!validation.status) {
        return res.send(reply.failed(validation.message));
    }

    // Order status filter
    var user_where = {}

    if (name) {
        user_where.name = { [Op.substring]: name };
    }

    var condition = {
        where: {},
        attributes: { exclude: ['reason'] },
        include: [{ model: User, where: user_where, attributes: ["id", "name", "lname", "email"] }]
    };

    order_type = (order_type == "BUY") ? "SELL" : "BUY";

    if (order_type) {
        condition.where.order_type = { [Op.substring]: order_type };
    }

    if (country) {
        condition.where.country = { [Op.substring]: country };
    }


    if (currency) {
        condition.where.currency = { [Op.substring]: currency };
    }

    with_currency = (with_currency == null) ? DEFAULT_FIAT_CURRENCIES : with_currency; // INR

    if (with_currency) {
        condition.where.with_currency = { [Op.substring]: with_currency };
    }

    if (payment_type) {
        condition.where.payment_type = { [Op.substring]: payment_type }
    }
    if (status) {
        condition.where.status = { [Op.substring]: status }
    }
    if (pending_qty) {
        condition.where.pending_qty = { [Op.substring]: pending_qty }
    }
    if (max_qty) {
        condition.where.max_qty = { [Op.substring]: max_qty };

    }
    if (amount) {
        condition.where.search_max_qty = { [Op.gte]: parseFloat(amount) }
        condition.where.search_min_qty = { [Op.lte]: parseFloat(amount) }
    }
    try {
        const paginate = Helper.getPaginate(req, sortbyname, sortby);

        let finalquery = Object.assign(condition, paginate)

        let all_order = await P2P_Order.findAll(finalquery);

        let total_count = await P2P_Order.count(condition);

        all_order = reply.paginate(paginate.page, all_order, paginate.limit, total_count);

        return res.send(reply.success("All p2p orders fetched successfully.", all_order));
    } catch (err) {
        console.log(err);
        return res.send(reply.failed("Unable to fetch orders at this time."))
    }

}

const get_crypto = async (req, res) => {
    try {
        let c = myCache.get("p2p_form_payment") ?? null;

        let ld = myCache.get("getBinanceFiatPrices") ?? null; //live data fiat
      

        let fiat = [];

        if(c){
            let cc =  Object.keys(c);

            _.map(cc, (v,i) => {
                fiat.push({
                    currency:v,
                    image: variables.node_url + "p2p/image/fiat/" + v + ".png",
                    iso_code:SYMBOL_ISO_CODE[v],
                    decimal:FIAT_DECIMAL[v],
                    price:ld.find(e=>e.pair == v+'_USD')?.price ?? ''
                })
            });
        }

        
        let crypto_ld = myCache.get("getBinanceCryptoPrices") ?? null; //live data crypto
        let currencies = await P2p_Currency.findAll({ attributes: ["currency", "decimal_length", "iso_code"], raw: true });
        let dd = [];

        _.map(currencies, (v,i) => {
            dd.push({
                currency:v.currency,
                image: variables.node_url + "p2p/image/crypto/" + v.currency + ".png",
                iso_code:v.iso_code,
                decimal:v.decimal_length,
                price:crypto_ld.find(e=>e.symbol == v.currency+'USDT')?.price ?? ''
            })
        });

        return res.send(reply.success("currencies fetched", null, {currency:dd, fiat}))
    } catch (error) {
        console.log(error)
        return res.send(reply.failed("Unable to fetch currencies at this time."))
    }
}



// testing purpose 

// create fiat currencies payment methods
async function add_random_orders () {



let _currency = ["BNB", "ADA", "MATIC", "WRX", "USDT", "BTC", "BUSD","ETH", "SHIB" ,"TRX", "SOL", "DAI"];

let _with_currency = ['AFN',
    'AOA',
    'AZN',
    'BIF',
    'BND',
    'BSD',
    'BWP',
    'BYN',
    'BZD',
    'CDF',
    'CNY',
    'CRC',
    'CVE',
    'DJF',
    'DKK',
    'ERN',
    'ETB',
    'GMD',
    'GNF',
    'GTQ',
    'HNL',
    'HRK',
    'HTG',
    'HUF',
    'IQD',
    'ISK',
    'JMD',
    'JOD',
    'KGS',
    'KMF',
    'KWD',
    'LRD',
    'LYD',
    'MDL',
    'MGA',
    'MOP',
    'MRU',
    'MWK',
    'MZN',
    'NAD',
    'NIO',
    'NOK',
    'PGK',
    'RSD',
    'SCR',
    'SLL'];


let _userid = [1,2,3,5];
let _price = [70,90,100,88,88.50,91,71,77.55,50.90,80.99,50.40,60.90,69,68,90.45];
let _country = "India"

let _order_type = ['BUY', 'SELL'];

let _min_qty = [10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
let _max_qty = [50,51,52,53,54,58,80,100,120,500,500,1000,5000,9000];

let _terms_and_conditions = ['dfgdgfdgfdfg','47345634563463456','lsdshfgkhjdsfgkhdksfghkjh', 'Arabadsgfsdfgdsfgdfg', 'Armenidfgdsfgdsfga', 'American Samoadsfgasdfgdfg', '253542352354Austria', '325423542345Bangladesh', 'Belg546746ium'];

let _status = "placed";


let _regions = ['India','Ascension Island', 'Arab Emirates', 'Armenia', 'American Samoa', 'Austria', 'Bangladesh', 'Belgium'];


function tt(arr) {

    // get random index value
    const randomIndex = Math.floor(Math.random() * arr.length);

    // get random item
    const item = arr[randomIndex];

    return item;
}



Array.from(Array(1500)).forEach(async (Element, index) => {

    let minqty = tt(_min_qty);
    let maxqty = tt(_max_qty);
    let atprice = tt(_price);

    await P2P_Order.create({
        currency : tt(_currency),
        with_currency: tt(_with_currency),
        user_id: tt(_userid),
        at_price: atprice,
        country:_country,
        order_type:tt(_order_type),
        min_qty:minqty,
        search_min_qty:CL.mul(minqty, atprice),
        max_qty:maxqty,
        search_max_qty: CL.mul(maxqty, atprice),
        pending_qty:maxqty,
        terms_and_conditions: tt(_terms_and_conditions),
        status:_status,
        regions: [tt(_regions)]
    });
    console.log(index);
})





}
const my_get_order = async (req, res) => {
    let { order_type, country, page = 1, per_page = 10, currency, with_currency, payment_type, region, amount, sortbyname, sortby,start_date,
        end_date ,statuss} = req.query;

    let { status, message } = await CValidator(req.query, {
        page: "integer|gt:0",
        per_page: `integer|gt:0`,
    });

    if (!status) {
        return res.send(reply.failed(message));
    } 
    // Order status filter
    var condition = {
        where: {
            user_id: req.user.id,
            
        },
        attributes: ['id', 'user_id', 'country', 'status','at_price', 'currency', 'with_currency', 'order_type', 'min_qty', 'max_qty','search_min_qty','search_max_qty', ['pending_qty', 'available_qty'], 'terms_and_conditions', 'created_at', 'updated_at','payment_type', 'regions'],
        include: [{ model: User, attributes: ["id", "name", "lname", "email"] }]
    };

    
    if (order_type) {
        order_type = (order_type == "BUY") ? "SELL" : "BUY";
        condition.where.order_type = order_type;
    }
    if(statuss){
        condition.where.status = statuss;
    }

    if (country) {
        condition.where.country = country;
    }
    if(start_date && end_date){
        condition.where.created_at = { [Op.between]: [start_date, end_date]}
    }

    if (currency) {
        condition.where.currency = currency;
    }

    with_currency = (with_currency == null) ? DEFAULT_FIAT_CURRENCIES : with_currency; // INR

    if (with_currency) {
        condition.where.with_currency = with_currency;
    }

    if (payment_type) {
        condition.where.payment_type = { [Op.like]: `%${payment_type}%` }
    }

    if (region) {
        condition.where.regions = { [Op.like]: `%${region}%` }
    }

    if (amount) {
        condition.where.search_max_qty = { [Op.gte]: parseFloat(amount) }
        condition.where.search_min_qty = { [Op.lte]: parseFloat(amount) }
    }

    try {
        const paginate = Helper.getPaginate(req, sortbyname, sortby);

        let finalquery = Object.assign(condition, paginate)

        let all_order = await P2P_Order.findAll(finalquery);

        let total_count = await P2P_Order.count(condition);

        all_order = reply.paginate(paginate.page, all_order, paginate.limit, total_count);

        return res.send(reply.success("All p2p orders fetched successfully.", all_order))
    } catch (err) {
        console.log(err);
        return res.send(reply.failed("Unable to fetch orders at this time."))
    }

}

const upload_image = async (req, res) => {
    let request = req.body;
    request.image = req?.file?.filename || "";
    request.type = req?.file?.type;
 
    if (req.filedata.status_code == 0) {
        return res.send(req.filedata)
    }

    //custom validation
    let { status, message } = await CValidator(request, {
        image: "string",
        url: "url"
    });

    if (!status) { return res.send(reply.failed(message)); }


    return res.send(reply.success("message", `${variables.node_url}p2p/image/chatImages/${req?.file?.filename}`));
}

// await add_random_orders()


export default {
    place_order,
    cancel_order,
    get_order,
    order_crypto,
    get_order_boss,
    order_history,
    trade_history,
    get_crypto,
    getCountries,
    User_P2p_Profile,
    my_get_order,
    upload_image
}