import _ from "lodash";
import CValidator from "../../Validator/CustomValidation.js";
import { P2P_FiatCurrency } from "../Models/P2p_FiatCurrency.js";
import reply from "../../Common/reply.js";
import { P2P_PaymentDetail } from "../Models/P2P_PaymentDetail.js";
import { Op } from "sequelize";
import scheduler from "../NodeSheduler/scheduler.js";
import { startOfDay, endOfDay } from "date-fns";
import Helper from "../../Common/Helper.js";
import myCache from "../NodeCache/cache.js";
import variables from "../../Config/variables.js";

import { SYMBOL_ISO_CODE, FIAT_DECIMAL } from "../Common/StaticObject.js";

// User Routes
const getType = async (req, res) => {
    let c = myCache.get("p2p_form_payment") ?? null;

    let fiat = [];

    if(c){
        let cc =  Object.keys(c);

        _.map(cc, (v,i) => {
            fiat.push({
                currency:v,
                image: variables.node_url + "p2p/image/fiat/" + v + ".png",
                iso_code:SYMBOL_ISO_CODE[v],
                fiat_decimal:FIAT_DECIMAL[v],
            })
        });
    }

    return res.send(reply.success("Payment Types Fetched Successfully", c , {fiat}));
}

const createPaymentMethod = async (req, res) => {
    let request = req.body;

    let { status, message } = await CValidator(request, {
        currency: `required|exists:p2p_fiat_currencies,currency`,
        payment_slug: `required`
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let countt = await P2P_PaymentDetail.count({where:{
        user_id:req.user.id,
        payment_slug:request.payment_slug,
        currency:request.currency,
    }})

    if(countt >= 3){
        return res.send(reply.failed(`You have already added 3 same type of payment methods for ${request.currency}`))
    }

    let ptypes = myCache.get("p2p_payment_methods");
    let pforms = myCache.get("p2p_form_payment");

    if (!ptypes || !pforms) {
        return res.send(reply.failed("getting some issue our side. Please wait"));
    }

    let all_slug = ptypes[request.currency] ?? null;

    if (!all_slug) {
        return res.send(reply.failed("Invalid Data!"));
    }

    if (!all_slug.includes(request.payment_slug)) {
        return res.send(reply.failed("Invalid Data!"));
    }

    let form_data = _.find(pforms[request.currency], (v) => v.slug == request.payment_slug);

    let rules = form_data.form_validate ?? null;

    if (!rules) {
        return res.send(reply.failed('Invalid Data!'));
    }

    let pValidate = await CValidator(request, rules);

    if (pValidate.status == 0) {
        return res.send(reply.failed(pValidate.message));
    }

    let keys = _.keys(rules);

    request.payment_detail = _.pick(request, keys);

    request.user_id = req.user.id;

    request = _.pick(request, ["payment_slug", "payment_detail", "currency", "user_id"]);

    try {
        await P2P_PaymentDetail.create(request);
        return res.send(reply.success(`The payment Method is added.`))
    } catch (error) {
        console.log(error);
        return res.send(reply.success(`Unable to Create at this time.`))
    }
}

const updatePaymentStatus = async (req, res) => {
    let request = req.query;

    let { status, message } = await CValidator(request, {
        id: "required|exists:p2p_payment_details,id",
        status: "required|in:1,0"
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let payment_method = await P2P_PaymentDetail.findOne({
        where: {
            id: request.id,
            user_id: req.user.id,
            is_verify: 1 //verification needed
        },
        attributes: ["payment_slug", "status", "id"]
    });

    if (!payment_method) {
        return res.send(reply.failed("Payment Method still not verified by admin!"))
    }

    await P2P_PaymentDetail.update({ status: 0 }, {
        where: {
            payment_slug: payment_method.payment_slug,
            user_id: req.user.id
        },
    });

    payment_method.status = 1;
    await payment_method.save();

    return res.send(reply.success("status updated successfully."))

}

const getPaymentMethod = async (req, res) => {
    let { sortbyname, sortby, date, slug, with_currency } = req.query;

    let validation = await CValidator(req.query, {
        page: "integer|gt:0",
        per_page: `integer|gt:0`,
        with_currency: `required`
    });

    if (!validation.status) {
        return res.send(reply.failed(validation.message));
    }

    let today = new Date(date);

    const where = {};

    if (slug) {
        where.payment_slug = { [Op.substring]: [slug] };
    }

    if (date) {
        where.created_at = {
            [Op.between]: [startOfDay(today), endOfDay(today)]
        }
    }

    where.currency = with_currency || "";

    where.user_id = req.user.id;

    let condition = {
        where: where,
        attributes: ['id', 'user_id', 'payment_slug', 'payment_detail', 'currency', 'status', 'is_verify', 'remark', 'created_at'],
    }

    const paginate = Helper.getPaginate(req, sortbyname, sortby);

    let finalquery = Object.assign(condition, paginate);

    let p = await P2P_PaymentDetail.findAll(finalquery);

    let total_count = await P2P_PaymentDetail.count(condition);

    p = reply.paginate(paginate.page, p, paginate.limit, total_count);

    return res.send(reply.success("Fetched successfully.", p));
}

// Admin Routes
const getAllPaymentMethods = async (req, res) => {
    let { sortbyname, sortby, date, slug, currency } = req.query;

    let validation = await CValidator(req.query, {
        page: "integer|gt:0",
        per_page: `integer|gt:0`,
    });

    if (!validation.status) {
        return res.send(reply.failed(validation.message));
    }

    let today = new Date(date);

    const where = {};

    if (slug) {
        where.payment_slug = { [Op.substring]: [slug] };
    }

    if (date) {
        where.created_at = {
            [Op.between]: [startOfDay(today), endOfDay(today)]
        }
    }

    if (currency) {
        where.currency = {
            [Op.substring]: [currency]
        }
    }

    let condition = {
        where: where,
        attributes: ['id', 'user_id', 'payment_slug', 'payment_detail', 'currency', 'status', 'is_verify', 'remark', 'created_at'],
    }

    const paginate = Helper.getPaginate(req, sortbyname, sortby);

    let finalquery = Object.assign(condition, paginate);

    let p = await P2P_PaymentDetail.findAll(finalquery);

    let total_count = await P2P_PaymentDetail.count(condition);

    p = reply.paginate(paginate.page, p, paginate.limit, total_count);

    return res.send(reply.success("Fetched successfully.", p));
}

const verifyPaymentMethod = async (req, res) => {
    let request = req.query;

    let { status, message } = await CValidator(request, {
        id: "required|integer|exists:p2p_payment_details,id",
        is_verify: "required|in:1,2",
    });

    if (!status) { return res.send(reply.failed(message)); }

    let payment_method = await P2P_PaymentDetail.findOne({ where: { id: request.id, is_verify: 0 } });

    if (!payment_method) {
        return res.send(reply.failed(`Invalid Data!`));
    }

    if (payment_method.is_verify == "2") {
        return res.send(reply.failed(`Already Rejected this payment method!`));
    }

    try {
        payment_method.is_verify = request.is_verify;
        await payment_method.save();
        let result = (request.is_verify == 1) ? "Approved" : "Rejected";
        return res.send(reply.success(`${result} Successfully!!`));
    } catch (error) {
        console.log(error);
        return res.send(reply.success("Unable to process request at this time!!"));
    }
}

const createType = async (req, res) => {
    let request = req.body;

    request = _.pick(request, ['slug', 'currency', 'form_field', 'form_validate']);

    let { status, message } = await CValidator(request, {
        currency: `required`,
        slug: `required`,
        form_field: `required`,
        form_validate: `required`
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    if (typeof request.form_field != 'object' || typeof request.form_validate != 'object') {
        return res.send(reply.failed('Invalid Data!'));
    }

    let e = await P2P_FiatCurrency.findOne({
        where: {
            currency: request.currency,
            slug: request.slug
        },
        attributes: ["id"]
    });

    // if payment type is already exists
    if (e) {
        return res.send(reply.failed('Already Exists!'));
    }

    request = _.pick(request, ['currency', 'form_field', 'form_validate', 'slug']);

    try {
        await P2P_FiatCurrency.create(request);
        await scheduler.getFiatCurrencies();
        return res.send(reply.success(`The payment type is added.`))
    } catch (error) {
        console.log(error);
        return res.send(reply.success(`Unable to Create at this time.`))
    }

}


const getbyId = async (req, res) => {

    var all_currencies = await P2P_FiatCurrency.findOne({
        where:{id:req.query.id},
        attributes: ['type','id', 'slug', 'form_field', 'currency', 'form_validate']
    });


    return res.send(reply.success("Payment Types Fetched Successfully",all_currencies ));
}




export default {
    getType,
    getbyId,
    createType,
    createPaymentMethod,
    verifyPaymentMethod,
    updatePaymentStatus,
    getPaymentMethod,
    getAllPaymentMethods
}