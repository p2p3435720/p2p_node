import _ from "lodash";
import reply from "../../Common/reply.js";
import CValidator from "../../Validator/CustomValidation.js";
import { P2P_Feedback } from '../Models/P2P_Feedback.js'
import { User } from "../../Models/User.js";
import { Model, Sequelize } from '../../Database/sequelize.js';
import { ORDER_STATUS } from "../Common/GVariables.js";
import { P2P_Trade } from "../Models/P2P_Trade.js";
import { Op } from "sequelize";
const { QueryTypes } = Sequelize;

const get = async (req, res) => {

    let request = req.query;

    let { status, message } = await CValidator(request, {
        user_id: "required|integer|exists:users,id",
        type: "required|in:All,positive,negative",
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let condition = { where: { feedback_to: request.user_id }, include: [{ model: User, attributes: ["id", "name", "email"] }], attributes: ["id", "created_at", "trade_id", "feedback_type", "feedback_comment"] };

    if (request.type != 'All') {
        condition.where.feedback_type = request.type;
    }

    try {
        let query = `SELECT COUNT(id) as total_feedback,
        (SELECT Count(id) FROM p2p_feedbacks WHERE feedback_to = ${request.user_id}  AND feedback_type = 'positive')  as positive_feedback,
        (SELECT Count(id) FROM p2p_feedbacks WHERE feedback_to = ${request.user_id} AND feedback_type = 'negative') as negative_feedback
        FROM p2p_feedbacks
        WHERE feedback_to = ${request.user_id}`;

        const [rr] = await Model.query(query, { type: QueryTypes.SELECT });

        let { total_feedback = 0, positive_feedback = 0, negative_feedback = 0 } = rr;

        const feedback = await P2P_Feedback.findAll(condition);

        return res.send(reply.success("All p2p feedbacks fetched successfully.", feedback, { total_feedback, positive_feedback, negative_feedback }));
    } catch (err) {
        return res.send(reply.failed("Unable to fetch feedback at this time."))
    }
}

const Fdelete = async (req, res) => {
    let request = req.query;

    let { status, message } = await CValidator(request, {
        trade_id: "required|exists:p2p_trades,id",
    });

    if (!status) {
        return res.send(reply.failed(message));
    }
    
    try {
        let destroy = await P2P_Feedback.destroy({
            where: {
                trade_id: request.trade_id,
                user_id: req.user.id
            }
        })
        return (destroy == 1) ? res.send(reply.success("Feedback deleted successfully.")) : res.send(reply.success("Nothing to delete."))

    } catch (err) {
        console.log(err)
        return res.send(reply.failed("Unable to delete feedback at this time."))
    }
}

const TableTrade = async (update_columns = {}, whereCondition = {}) => {

    let r = await P2P_Trade.findOne({ where: whereCondition });

    if (r) {

        if (Object.keys(update_columns).length > 0) {
            for (const key in update_columns) {
                r[key] = update_columns[key];
            }

            await r.save();
        }


        return r;
    } else {
        return 'INVALID';
    }
}

const create = async (req,res) => {
    let request = req.body
    let { status, message } = await CValidator(request, {
        user_id: "required|integer|exists:users,id",
        receiver_id: "required|integer|exists:users,id",
        feedback_type: "required|in:positive,negative",
        trade_id:"required|integer|exists:p2p_trades,id",
        feedback_comment:"required"
    });

    if (!status) {
        return res.send(reply.failed(message));
    }
    const { trade_id, user_id, receiver_id, feedback_type, feedback_comment } = request;

    // Trade Update
    let trade = await TableTrade({}, {
        id: trade_id,
        status: {
            [Op.not]: [ORDER_STATUS.processing]
        },
        [Op.or]: [
            {
                buyer_user_id: user_id
            },
            {
                seller_user_id: user_id
            }
        ]
    });

    if (trade == 'INVALID') {
        return res.send(reply.failed("Invalid Data!"))
    }

    const feedback_data = {
        trade_id,
        user_id,
        feedback_to: receiver_id,
        feedback_type,
        feedback_comment
    }

    let exist_feed = await P2P_Feedback.findOne({
        where: {
            trade_id: feedback_data.trade_id,
            user_id: feedback_data.user_id,
            feedback_to: feedback_data.feedback_to
        }
    });

    try {
        if (exist_feed) {
            exist_feed.feedback_type = feedback_data.feedback_type;
            exist_feed.feedback_comment = feedback_data.feedback_comment;
            await exist_feed.save();
            return res.send(reply.success("Feedback updated successfully.", { data: exist_feed }))
        } else {
            let create = await P2P_Feedback.create(feedback_data);
            return res.send(reply.success("Feedback added successfully.", { data: create }))
        }
    } catch (err) {
        console.log(err);
        return res.send(reply.failed("Unable to add feedback at this time."))
    }
}



export default {
    get,
    create,
    Fdelete
}