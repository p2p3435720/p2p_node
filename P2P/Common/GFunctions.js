import p2pevent from "../Events/myp2pevent.js";
import { UserKyc } from "../../Models/UserKyc.js";
import reply from "../../Common/reply.js";
import { CL } from "cal-time-stamper";
import { P2P_WALLET_EVENT, MAIL_CONFIG } from "./GVariables.js";
import { P2P_WalletLog } from "../Models/P2P_WalletLog.js";
import { P2P_Wallet } from "../Models/P2P_Wallet.js";

import nodeMailer from 'nodemailer';

// Verification KYC 
const Kyc_status = async (user_id) => {
    let b = await UserKyc.findOne({
        where: {
            user_id,
            status: 'completed'
        },
        attributes: ['country'],
        raw: true
    });

    if (!b) {
        return reply.failed('Kyc not verified')
    }

    return reply.success('Kyc is verified', b?.country);
}

// Create log in p2p wallet log table
const p2p_log = async ({ wallet_id, user_id, amount, transaction_type, comment }) => {
    await P2P_WalletLog.create({ wallet_id, user_id, amount, transaction_type, comment });
}

// ========== EVENTS MEMBERS ============= //
// P2p wallet event handler like freze, unfreze,cancel,credit, debit
const P2PWalletEvent = async ({ data, type }) => {
    var userWallet = await P2P_Wallet.findOne({
        where: { user_id: data.user_id, currency: data.currency }  ///added currency filter for new currency adding  by priyanka
    });

    var transaction_type = "credit";

    if (!userWallet) {
        userWallet = await P2P_Wallet.create({
            'user_id': data.user_id,
            'currency': data.currency,
            'balance': (type == P2P_WALLET_EVENT.credit) ? data.amount : 0
        });
    } else {

        // Type --->
        if (type == P2P_WALLET_EVENT.credit) {   // credit 
            transaction_type = "credit";
            userWallet.balance = CL.add(userWallet.balance, data.amount);
            await userWallet.save();
        }

        if (type == P2P_WALLET_EVENT.debit) {   // debit 
            
            transaction_type = "debit";
            userWallet.balance = CL.sub(userWallet.balance, data.amount);
            await userWallet.save();
        }

        if (type == P2P_WALLET_EVENT.freeze) {  // freeze
            transaction_type = "debit";
            userWallet.balance = CL.sub(userWallet.balance, data.amount);
            userWallet.freeze_balance = CL.add(userWallet.freeze_balance, data.amount);
            await userWallet.save();
        }

        if (type == P2P_WALLET_EVENT.unfreeze) { // unfreeze
            transaction_type = "debit";
            userWallet.freeze_balance = CL.sub(userWallet.freeze_balance, data.amount);
            await userWallet.save();
        }

        if (type == P2P_WALLET_EVENT.cancel) { // cancel
            transaction_type = "credit";
            userWallet.balance = CL.add(userWallet.balance, data.amount);
            userWallet.freeze_balance = CL.sub(userWallet.freeze_balance, data.amount);
            await userWallet.save();
        }
    }

    await p2p_log({ wallet_id: userWallet.id, user_id: data.user_id, amount: data.amount, transaction_type, comment: data.comment });
}

// send mail to users
const send_mail = async (to, message) => {
    let transporter = nodeMailer.createTransport({
        host: MAIL_CONFIG.MAIL_HOST,
        port: MAIL_CONFIG.MAIL_PORT,
        secure: false,
        auth: {
            user: MAIL_CONFIG.MAIL_USERNAME,
            pass: MAIL_CONFIG.MAIL_PASSWORD
        }
    });

    let mailOptions = {
        from: MAIL_CONFIG.MAIL_FROM_ADDRESS, // sender address
        to: to, // list of receivers
        subject: 'Test', // Subject line
        text: message, // plain text body
        html: '<b>' + message + '</b>' // html body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });

}


p2pevent.on("P2PWalletEvent", P2PWalletEvent);

// p2pevent.emit('P2PWalletEvent', {data:{user_id:1,currency:'USDT',amount:50}, type: P2P_WALLET_EVENT.credit})
// p2pevent.emit('P2PWalletEvent', {data:{user_id:1,currency:'USDT',amount:10}, type: P2P_WALLET_EVENT.debit})
// p2pevent.emit('P2PWalletEvent', {data:{user_id:1,currency:'USDT',amount:20}, type: P2P_WALLET_EVENT.freeze})
// p2pevent.emit('P2PWalletEvent', {data:{user_id:1,currency:'USDT',amount:20}, type: P2P_WALLET_EVENT.unfreeze})
// p2pevent.emit('P2PWalletEvent', {data:{user_id:1,currency:'USDT',amount:40}, type: P2P_WALLET_EVENT.cancel})

export {
    Kyc_status,
    send_mail
}