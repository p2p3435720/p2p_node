const ORDER_STATUS = {
    completed: "completed",
    canceled: "cancelled",
    partially_completed: "partially_completed",
    processing: "processing",
    placed: "placed",
    expired: "expired",
    disputed: "disputed"
}

const APPEAL_STATUS = {
    canceled: "canceled",
    closed: "closed",
    processing: "processing",
    opened: "opened",
}

const P2P_WALLET_EVENT = {
    credit: "credit",
    debit: "debit",
    freeze: "freeze",
    unfreeze: "unfreeze",
    cancel: "cancel"
}

const MAIL_CONFIG = {
    MAIL_HOST: "smtp.gmail.com",
    MAIL_PORT: "587",
    MAIL_USERNAME: "support@myvee.io",
    MAIL_PASSWORD: "yqbvigeffahskcfa",
    MAIL_FROM_ADDRESS: "support@myvee.io",
}

const DEFAULT_FIAT_CURRENCIES = "INR";

export {
    ORDER_STATUS,
    APPEAL_STATUS,
    P2P_WALLET_EVENT,
    MAIL_CONFIG,
    DEFAULT_FIAT_CURRENCIES
}