
import myCache from "../NodeCache/cache.js";
import { P2P_FiatCurrency } from "../Models/P2P_FiatCurrency.js";
import _ from "lodash";
import reply from "../../Common/reply.js";
import {COUNTRIES  } from "../Common/StaticObject.js";
import fetch from "node-fetch";
import schedule from "node-schedule";
import { P2p_Currency } from "../Models/P2p_Currency.js";


let cc = _.map(COUNTRIES, (v,i) => v.name);

const getFiatCurrencies = async () => {
    var all_currencies = await P2P_FiatCurrency.findAll({
        attributes: ['id','type', 'slug', 'form_field', 'currency', 'form_validate']
    });


    let c = reply.groupBy("currency", all_currencies);

    myCache.set("p2p_form_payment", c); // set p2p payment method form validation and form fields

    let result_arr = {};

    _.filter(all_currencies, (v, i) => {
        if (v.currency in result_arr) {
            result_arr[v.currency].push(v.slug);
        } else {
            result_arr[v.currency] = [v.slug];
        }
    })

    myCache.set("p2p_payment_methods", result_arr);


    console.log({ cache: "p2p payment methods cached!!" })
}

const getcountries = async () => {
    myCache.set("p2p_countries", COUNTRIES);
    myCache.set("validate_p2p_countries", cc);  
    console.log({cache: 'countries set into cache!!'});
}

const getBinanceFiatPrices = async () => {
    let dd = await fetch("https://www.binance.com/bapi/asset/v1/public/asset-service/product/currency");
    const jsonData = await dd.json();

    let ee = [];

    _.map(jsonData.data, (v,i) => {
        ee.push({
            pair: v.pair,
            price: v.rate,
            
        })
    })

    myCache.set("getBinanceFiatPrices", ee);
    // console.log({cache: 'Binance Fiat Prices set into cache!!'});
}

const getBinanceCryptoPrices = async () => {
    let currencies = await P2p_Currency.findAll({ group: 'currency',raw:true,attributes:['currency'] })
    currencies = _.flatMap(currencies, (e) =>{
        return e.currency + 'USDT'
    }).toString();

    //console.log(currencies)

    let dd = await fetch(`https://datalake.network3.info/current-price/${currencies}`);
    let jsonData = await dd.json();
    
    
    let price_obj = [
        {
            symbol: 'BUSDUSDT',
            price: '1',
        },
        {
            symbol: 'USDTUSDT',
            price: '1',
        },   
    ]

    jsonData =jsonData.data.concat(price_obj)
    myCache.set("getBinanceCryptoPrices", jsonData);
}


try {
    await getFiatCurrencies();
    await getcountries();
    await getBinanceFiatPrices(); 
    await getBinanceCryptoPrices(); 
} catch (error) {
    console.log({error})
}



const job = schedule.scheduleJob("0 0 */1 * * *", getBinanceFiatPrices);
const job1 = schedule.scheduleJob("0 0 */1 * * *", getBinanceCryptoPrices);

export default {
    getFiatCurrencies,
    getcountries
}