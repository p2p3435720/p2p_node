import { DataTypes } from 'sequelize';
import { Model as DB } from '../../Database/sequelize.js';


export const P2P_WalletLog = DB.define('p2p_wallet_logs', {

    id: {
        type: DataTypes.BIGINT(20),
        autoIncrement: true,
        primaryKey: true
    },

    user_id: {
        type: DataTypes.STRING,
        allowNull: false
    },

    wallet_id: {
        type: DataTypes.STRING,
        allowNull: false
    },

    amount: {
        type: DataTypes.STRING,
        defaultValue: 0
    },

    transaction_type: {
        type: DataTypes.STRING,
        allowNull: false
    },
    comment: {
        type: DataTypes.TEXT,
        allowNull: false
    }


}, {
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});


await P2P_WalletLog.sync({ alter: true });