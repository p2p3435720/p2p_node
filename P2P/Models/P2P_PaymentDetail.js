import { DataTypes } from "sequelize";
import { Model as DB } from "../../Database/sequelize.js";

export const P2P_PaymentDetail = DB.define("p2p_payment_details", {
  id: {
    type: DataTypes.BIGINT(20),
    autoIncrement: true,
    primaryKey: true,
  },

  user_id: {
    type: DataTypes.BIGINT(20),
    allowNull: false,
  },

  payment_slug: {
    type: DataTypes.STRING,
    allowNull: false,
  },

  payment_type: {
    type: DataTypes.VIRTUAL,
    get() {
      const rawValue = this.getDataValue('payment_slug');
      return (rawValue != undefined) ? rawValue.toUpperCase().replace('_', ' ') : "";
    }
  },

  payment_detail: {
    type: DataTypes.TEXT,
    allowNull: false,
    set(value) {
      this.setDataValue("payment_detail", JSON.stringify(value));
    },
    get() {
      const rawValue = this.getDataValue('payment_detail');
      return rawValue ? JSON.parse(rawValue) : {};
    }
  },

  currency: {
    type: DataTypes.STRING,
    allowNull: false,
  },

  status: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
    comment: "0.inactive,1.active",
  },

  is_verify: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
    comment: "0.pending,1.approved,2.rejected",
  },

  remark: {
    type: DataTypes.STRING,
    defaultValue: "",
  },
},
  {
    underscored: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

await P2P_PaymentDetail.sync();
