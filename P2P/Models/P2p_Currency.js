import { DataTypes } from "sequelize";
import { Model as DB } from "../../Database/sequelize.js";

export const P2p_Currency = DB.define("p2p_currencies", {
  id: {
    type: DataTypes.BIGINT(20),
    autoIncrement: true,
    primaryKey: true,
  },
  currency: {
    type: DataTypes.STRING,
    allowNull: false,
    set(value) {
      this.setDataValue("currency", value.toUpperCase());
    },
  },
  decimal_length: {
    type: DataTypes.STRING
  },
  iso_code: {
    type: DataTypes.STRING
  },
},
  {
    underscored: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);


await P2p_Currency.sync();
