import { DataTypes } from 'sequelize';
import { Model as DB } from '../../Database/sequelize.js';


export const P2P_Wallet = DB.define('p2p_wallets', {

    id: {
        type: DataTypes.BIGINT(20),
        autoIncrement: true,
        primaryKey: true
    },

    user_id: {
        type: DataTypes.STRING,
        allowNull: false
    },

    currency: {
        type: DataTypes.STRING,
        allowNull: false,
        set(value) {
            this.setDataValue('currency', value.toUpperCase());
        }
    },
    balance: {
        type: DataTypes.STRING,
        defaultValue: 0
    },

    freeze_balance: {
        type: DataTypes.STRING,
        defaultValue: 0
    },

}, {
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});


await P2P_Wallet.sync();