import { DataTypes } from 'sequelize';
import { Model as DB } from '../../Database/sequelize.js';
import variables from '../../Config/variables.js';

const imagePath = variables.node_url;
export const P2P_Msg = DB.define('p2p_msgs', {

    id: {
        type: DataTypes.BIGINT(20),
        autoIncrement: true,
        primaryKey: true
    },
    sender_id: {
        type: DataTypes.STRING,
        allowNull: false
    },
    receiver_id: {
        type: DataTypes.STRING,
        allowNull: false
    },
    trade_id: {
        type: DataTypes.STRING,
        allowNull: false
    },
    msg: {
        type: DataTypes.STRING,
        defaultValue: 0
    },
    file: {
        type: DataTypes.STRING,
        allowNull: false,
        // get(){
        //     const rawValue = this.getDataValue('file');
        //     return rawValue ? imagePath + "p2p/image/chatImages/" + rawValue: '';
        // }
         
    }
}, {
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});


await P2P_Msg.sync({alter:true});