import variables from "../../Config/variables.js";
import { Model, Sequelize } from "../../Database/sequelize.js";

const imagePath = variables.node_url;


export const P2p_dispute_chat = Model.define('p2p_dispute_chat', {
  id: {
    type: Sequelize.BIGINT(20),
    autoIncrement: true,
    primaryKey: true,
  },
  appeal_id: {
    type: Sequelize.BIGINT(20),
    allowNull: false
  },
  comment: {
    type: Sequelize.TEXT('long'),
    allowNull: false

  },
  receiver_id: {
    type: Sequelize.BIGINT(20),
    allowNull: false
  },
  sender_id: {
    type: Sequelize.BIGINT(20),
    allowNull: false
  },
  trade_id: {
    type: Sequelize.BIGINT(20),
    allowNull: false
  },
  image: {
    type: Sequelize.STRING,
    allowNull: false,
    get() {
      return (this.getDataValue('image') ? imagePath + 'image/tickets/' + this.getDataValue('image') : "");
    }
  },


}, {
  underscored: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});

await P2p_dispute_chat.sync({ alter: true });
