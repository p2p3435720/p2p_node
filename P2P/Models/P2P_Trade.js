import { DataTypes } from 'sequelize';
import { Model as DB } from '../../Database/sequelize.js';
import { ORDER_STATUS } from '../Common/GVariables.js';
import { P2P_Order } from './P2P_Order.js';

export const P2P_Trade = DB.define('p2p_trades', {

    id: {
        type: DataTypes.BIGINT(20),
        autoIncrement: true,
        primaryKey: true
    },

    buyer_order_id: {
        type: DataTypes.BIGINT(20),
        allowNull: false
    },

    seller_order_id: {
        type: DataTypes.BIGINT(20),
        allowNull: false,
    },

    at_price: {
        type: DataTypes.STRING,
        allowNull: false,
    },

    quantity: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    currency: {
        type: DataTypes.STRING,
        allowNull: false,
        set(value) {
            this.setDataValue('currency', value.toUpperCase());
        }
    },
    with_currency: {
        type: DataTypes.STRING,
        allowNull: false,
        set(value) {
            this.setDataValue('with_currency', value.toUpperCase());
        }
    },
    status: {
        type: DataTypes.STRING,
        defaultValue: ORDER_STATUS.processing
    },
    buyer_confirmation: {
        type: DataTypes.TINYINT,
        defaultValue: 0,
        comment: "0 = Pending, 1 = Confirmed, 2 = Cancelled"
    },
    seller_confirmation: {
        type: DataTypes.TINYINT,
        defaultValue: 0,
        comment: "0 = Pending, 1 = Confirmed"
    },

    reason: {
        type: DataTypes.STRING,
        defaultValue: ""
    },

    cancelled_at: {
        type: DataTypes.DATE,
        allowNull: true,
    },

    expired_at: {
        type: DataTypes.STRING
    },
    appeal_at: {
        type: DataTypes.STRING
    },
    buyer_user_id: {
        type: DataTypes.BIGINT(20),
        allowNull: false
    },
    seller_user_id: {
        type: DataTypes.BIGINT(20),
        allowNull: false
    }
}, {
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});


await P2P_Trade.sync();


P2P_Trade.belongsTo(P2P_Order, { as: "buyer", foreignKey: "buyer_order_id" });
P2P_Trade.belongsTo(P2P_Order, { as: "seller", foreignKey: "seller_order_id" });
P2P_Order.hasMany(P2P_Trade, { as: "seller", sourceKey: 'id', foreignKey: 'seller_order_id' });
P2P_Order.hasMany(P2P_Trade, { as: "buyer", sourceKey: 'id', foreignKey: 'buyer_order_id' });