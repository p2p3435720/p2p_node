import { DataTypes } from 'sequelize';
import { Model as DB } from '../../Database/sequelize.js';
import { APPEAL_STATUS } from '../Common/GVariables.js';
import { User } from '../../Models/User.js';
import { P2P_Trade } from './P2P_Trade.js';

export const P2p_Appeal = DB.define('p2p_appeals', {

    id: {
        type: DataTypes.BIGINT(20),
        autoIncrement: true,
        primaryKey: true
    },

    trade_id: {
        type: DataTypes.BIGINT(20),
        allowNull: false
    },

    appeal_user_id: {
        type: DataTypes.BIGINT(20),
        allowNull: false,
    },

    appeal_by: {
        type: DataTypes.STRING,
        allowNull: false,
    },

    status: {
        type: DataTypes.STRING,
        defaultValue: APPEAL_STATUS.opened
    },

    reason: {
        type: DataTypes.STRING,
        defaultValue: ""
    },

    cancelled_at: {
        type: DataTypes.STRING,
        allowNull: true

    },

    appeal_win: {
        type: DataTypes.STRING,
        defaultValue: ""
    },
    expired_at: {
        type: DataTypes.STRING
    },



}, {
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});


await P2p_Appeal.sync({ alter: true });
P2p_Appeal.belongsTo(User, { foreignKey: "appeal_user_id" });
P2p_Appeal.belongsTo(P2P_Trade, { foreignKey: "trade_id" });

