import { DataTypes } from "sequelize";
import { Model as DB } from "../../Database/sequelize.js";

export const P2P_FiatCurrency = DB.define("p2p_fiat_currencies", {
  id: {
    type: DataTypes.BIGINT(20),
    autoIncrement: true,
    primaryKey: true,
  },
  slug: {
    type: DataTypes.STRING,
    allowNull: false
  },
  type: {
    type: DataTypes.VIRTUAL,
    get() {
      const rawValue = this.getDataValue('slug');
      return rawValue.toUpperCase().replace('_', ' ');
    },
  },
  form_field: {
    type: DataTypes.TEXT,
    allowNull: false,
    set(value) {
      this.setDataValue("form_field", JSON.stringify(value));
    },
    get() {
      const rawValue = this.getDataValue('form_field');
      return rawValue ? JSON.parse(rawValue) : {};
    }
  },
  form_validate: {
    type: DataTypes.TEXT,
    allowNull: false,
    set(value) {
      this.setDataValue("form_validate", JSON.stringify(value));
    },
    get() {
      const rawValue = this.getDataValue('form_validate');
      return rawValue ? JSON.parse(rawValue) : {};
    }
  },
  currency: {
    type: DataTypes.STRING,
    allowNull: false,
    set(value) {
      this.setDataValue("currency", value.toUpperCase());
    },
  },
},
  {
    underscored: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);


await P2P_FiatCurrency.sync();
