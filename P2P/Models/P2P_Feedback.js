import { DataTypes } from "sequelize";
import { Model as DB } from "../../Database/sequelize.js";
import { User } from "../../Models/User.js";
import { P2P_Trade } from "./P2P_Trade.js";

export const P2P_Feedback = DB.define("p2p_feedbacks", {
  id: {
    type: DataTypes.BIGINT(20),
    autoIncrement: true,
    primaryKey: true,
  },
  trade_id: {
    type: DataTypes.BIGINT(20),
    allowNull: false
  },
  user_id: {
    type: DataTypes.BIGINT(20),
    allowNull: false
  },
  feedback_to: {
    type: DataTypes.BIGINT(20),
    allowNull: false
  },
  feedback_type: {
    type: DataTypes.STRING,
    allowNull: false,
    Comment: "positive, negative"
  },
  feedback_comment: {
    type: DataTypes.TEXT
  }
},
  {
    underscored: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);


await P2P_Feedback.sync({ alter: true });
P2P_Feedback.belongsTo(P2P_Trade, { foreignKey: "trade_id" })
P2P_Feedback.belongsTo(User, { foreignKey: "user_id" })
P2P_Trade.hasMany(P2P_Feedback, { as: "feedback", sourceKey: 'id', foreignKey: 'trade_id' })