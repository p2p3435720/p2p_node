import { DataTypes } from 'sequelize';
import { Model as DB } from '../../Database/sequelize.js';
import { ORDER_STATUS } from '../Common/GVariables.js';
import _ from "lodash";
import { User } from '../../Models/User.js';

export const P2P_Order = DB.define('p2p_orders', {

    id: {
        type: DataTypes.BIGINT(20),
        autoIncrement: true,
        primaryKey: true
    },

    user_id: {
        type: DataTypes.STRING,
        allowNull: false
    },

    country: {
        type: DataTypes.STRING,
        allowNull: false,
    },

    at_price: {
        type: DataTypes.STRING,
        allowNull: false,
    },

    currency: {
        type: DataTypes.STRING,
        allowNull: false,
        set(value) {
            this.setDataValue('currency', value.toUpperCase());
        }
    },
    with_currency: {
        type: DataTypes.STRING,
        allowNull: false,
        set(value) {
            this.setDataValue('with_currency', value.toUpperCase());
        }
    },

    order_type: {
        type: DataTypes.STRING,
        allowNull: false,
        comment: "BUY, SELL"
    },

    min_qty: {
        type: DataTypes.DECIMAL(28, 18),
        allowNull: false
    },

    max_qty: {
        type: DataTypes.DECIMAL(28, 18),
        allowNull: false
    },

    search_min_qty: {
        type: DataTypes.DECIMAL(28, 18),
        allowNull: false
    },

    search_max_qty: {
        type: DataTypes.DECIMAL(28, 18),
        allowNull: false
    },

    pending_qty: {
        type: DataTypes.DECIMAL(28, 18),
        allowNull: false
    },


    payment_type: {
        type: DataTypes.STRING,
        defaultValue: '',
        set(value) {
            this.setDataValue('payment_type', _.join(value, ","));
        },
        get() {
            const rawValue = this.getDataValue('payment_type');
            return rawValue ? _.split(rawValue, ',') : [];
        },
    },

    regions: {
        type: DataTypes.STRING,
        defaultValue: '',
        set(value) {
            this.setDataValue('regions', _.join(value, ","));
        },
        get() {
            const rawValue = this.getDataValue('regions');
            return rawValue ? _.split(rawValue, ',') : [];
        }
    },

    terms_and_conditions: {
        type: DataTypes.TEXT
    },

    status: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: ORDER_STATUS.placed
    },

    reason: {
        type: DataTypes.STRING,
        defaultValue: ""
    },

    cancelled_at: {
        type: DataTypes.DATE,
        allowNull: true,
    }

}, {
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});


await P2P_Order.sync();
P2P_Order.belongsTo(User, { foreignKey: "user_id" })

