import express from "express";
import UserController from "../Controller/UserController.js"; 
import Authenticate from "../Middleware/Authenticate.js";
let router = express.Router();
router.post("/signup", UserController.register);  
router.post("/IsValid_mail", UserController.IsValid_mail);
router.post("/login", UserController.login);
router.post("/get_code" , UserController.verfications);
router.post("/set_gauth" ,Authenticate, UserController.set_googleauth);
router.post("/gauth_verify" ,Authenticate, UserController.google_auth_verfiy);
router.post("/g_auth" , UserController.gauthenticate);
router.post("/verify_otp" , UserController.verify_otp);
router.delete("/logout" , Authenticate,UserController.logout);
router.post("/forgot" ,UserController.forgotPassword);
router.post("/reset" ,UserController.resetPassword);


export const UserRoutes = router;
