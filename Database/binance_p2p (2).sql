-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2023 at 07:13 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `binance_p2p`
--

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE `authorities` (
  `id` bigint(20) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` varchar(4) DEFAULT 'off',
  `message` varchar(255) DEFAULT 'Currently Server Is Busy.',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `authorities`
--

INSERT INTO `authorities` (`id`, `type`, `status`, `message`, `created_at`, `updated_at`) VALUES
(1, 'maintenance', 'off', 'Currently Server Is Busy.', '2023-02-15 10:12:30', '2023-02-15 10:12:30'),
(2, 'P2P', 'off', 'Currently Server Is Busy.', '2023-02-15 10:12:30', '2023-02-15 10:12:30'),
(3, 'withdraw', 'off', 'Currently Server Is Busy.', '2023-02-15 10:12:30', '2023-02-15 10:12:30'),
(4, 'deposit', 'off', 'Currently Server Is Busy.', '2023-02-15 10:12:30', '2023-02-15 10:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `block_networks`
--

CREATE TABLE `block_networks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `network_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_symbol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rpc_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chain_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `explorer_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=Active , 0=Not Active',
  `extra` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `block_networks`
--

INSERT INTO `block_networks` (`id`, `network_name`, `currency_symbol`, `rpc_url`, `token_type`, `chain_id`, `explorer_url`, `active_status`, `extra`, `created_at`, `updated_at`) VALUES
(1, 'Ethereum Mainnet', 'ETH', 'https://mainnet.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161', '', '1', 'https://ropsten.etherscan.io', 1, '', '2022-01-16 16:18:51', '2022-01-16 16:18:57'),
(2, 'Tron Network', 'TRON', 'https://api.trongrid.io/', '', '', 'https://shasta.tronscan.org', 1, '', '2022-01-16 18:51:32', '2022-01-16 18:51:32'),
(3, 'Binance Network', 'BNB', 'https://bsc-dataseed.binance.org/', '', '97', 'https://testnet.bscscan.com', 1, '', '2022-01-16 20:39:02', '2022-01-16 20:39:02'),
(4, 'Bitcoin Network', 'BTC', 'BTCTEST', '', '0', 'https://live.blockcypher.com/btc-testnet/', 1, '', '2022-01-16 20:39:02', '2022-01-16 20:39:02'),
(5, 'Indian Ruppess', 'INR', 'INR', '', '0', '', 1, '', '2022-01-16 20:39:02', '2022-01-16 20:39:02');

-- --------------------------------------------------------

--
-- Table structure for table `block_network_types`
--

CREATE TABLE `block_network_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `block_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `block_network_types`
--

INSERT INTO `block_network_types` (`id`, `block_id`, `token_type`, `created_at`, `updated_at`) VALUES
(1, '1', 'ERC20', '2022-02-28 12:47:06', '2022-02-28 12:47:06'),
(2, '2', 'TRC20', '2022-02-28 12:47:06', '2022-02-28 12:47:06'),
(3, '3', 'BEP20', '2022-02-28 12:47:06', '2022-02-28 12:47:06');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_multiple` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=Active , 0=Not Active',
  `default_c_network_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Default Currency Network Id',
  `currency_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'crypto' COMMENT ' fiat, crypto',
  `deposit_enable` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=Active , 0=Not Active',
  `deposit_desc` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Wallet Maintenance, Deposit Suspended',
  `withdraw_enable` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=Active , 0=Not Active',
  `withdraw_desc` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Wallet Maintenance, Withdrawal Suspended',
  `extra` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `active_status_enable` tinyint(4) NOT NULL DEFAULT 0,
  `p2p_currency_decimal` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `image`, `name`, `symbol`, `is_multiple`, `default_c_network_id`, `currency_type`, `deposit_enable`, `deposit_desc`, `withdraw_enable`, `withdraw_desc`, `extra`, `active_status_enable`, `p2p_currency_decimal`, `created_at`, `updated_at`) VALUES
(1, 'currency_images/dent.png', 'DENT', 'DENT', 0, '1', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 8, '2022-01-16 06:26:59', '2023-01-02 13:03:08'),
(2, 'currency_images/bnb.png', 'BNB', 'BNB', 0, '2', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 8, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(3, 'currency_images/eos.png', 'EOS', 'EOS', 0, '3', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 8, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(4, 'currency_images/sand.png', 'SAND', 'SAND', 0, '4', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(5, 'currency_images/comp.png', 'COMP', 'COMP', 0, '5', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(6, 'currency_images/link.png', 'LINK', 'LINK', 0, '6', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(7, 'currency_images/omg.png', 'OMG Network', 'OMG', 0, '7', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(8, 'currency_images/waves.png', 'WAVES', 'WAVES', 0, '8', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 8, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(9, 'currency_images/ez.png', 'EZ', 'EZ', 0, '9', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(10, 'currency_images/mana.png', 'MANA', 'MANA', 0, '10', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(11, 'currency_images/enj.png', 'ENJ', 'ENJ', 0, '11', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(12, 'currency_images/mtl.png', 'MTL', 'MTL', 0, '12', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 8, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(13, 'currency_images/uni.png', 'UNI', 'UNI', 0, '13', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(14, 'currency_images/poly.png', 'POLY', 'POLY', 0, '14', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(15, 'currency_images/btt.png', 'BTTC', 'BTTC', 0, '15', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(16, 'currency_images/win.png', 'WIN', 'WIN', 1, '16', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 6, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(17, 'currency_images/trx.png', 'TRX', 'TRX', 0, '17', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 6, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(18, 'currency_images/eth.png', 'ETH', 'ETH', 0, '18', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(19, 'currency_images/xrp.png', 'XRP', 'XRP', 0, '19', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 8, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(20, 'currency_images/doge.png', 'DOGE', 'DOGE', 0, '20', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 8, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(21, 'currency_images/yfi.png', 'YFI', 'YFI', 0, '21', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(22, 'currency_images/cake.png', 'CAKE', 'CAKE', 0, '22', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(23, 'currency_images/matic.png', 'MATIC', 'MATIC', 0, '23', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(24, 'currency_images/usdc.png', 'USDC', 'USDC', 0, '24', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 6, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(25, 'currency_images/yfii.png', 'YFII', 'YFII', 0, '25', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 18, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(26, 'currency_images/aave.png', 'AAVE', 'AAVE', 0, '26', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(27, 'currency_images/dot.png', 'DOT', 'DOT', 0, '27', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(28, 'currency_images/axs.png', 'AXS', 'AXS', 0, '28', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(29, 'currency_images/avax.png', 'AVAX', 'AVAX', 0, '29', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(30, 'currency_images/ltc.png', 'LTC', 'LTC', 0, '30', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(31, 'currency_images/ada.png', 'ADA', 'ADA', 0, '31', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(32, 'currency_images/1inch.png', '1INCH', '1INCH', 1, '32', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(33, 'currency_images/wrx.png', 'WRX', 'WRX', 0, '33', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(34, 'currency_images/shib.png', 'SHIBA INU', 'SHIB', 0, '34', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(35, 'currency_images/etc.png', 'ETC', 'ETC', 0, '35', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(36, 'currency_images/jst.png', 'JST', 'JST', 0, '36', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(37, 'currency_images/sol.png', 'SOL', 'SOL', 0, '37', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(38, 'currency_images/iota.png', 'IOTA', 'IOTA', 0, '38', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(39, 'currency_images/chz.png', 'CHZ', 'CHZ', 0, '39', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(40, 'currency_images/luna.png', 'LUNA', 'LUNA', 0, '40', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(41, 'currency_images/tether.png', 'USDT', 'USDT', 1, '41', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(44, 'currency_images/btc.png', 'Bitcoin', 'BTC', 0, '44', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-01-16 06:26:59', '2023-01-02 01:01:43'),
(54, 'currency_images/cht.png', 'CHARLIE', 'CHT', 1, '57', 'crypto', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-11-11 13:13:35', '2023-01-02 01:01:43'),
(55, 'currency_images/inr.png', 'RUPEE', 'INR', 0, '0', 'fiat', 1, 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2022-03-01 05:35:24', '2023-01-02 01:01:43'),
(71, '1672820368095.png', 'MAKE', 'MAKE', 0, '72', 'crypto', 0, 'Wallet Maintenance, Deposit Suspended', 0, 'Wallet Maintenance, Withdrawal Suspended', '', 1, 0, '2023-01-04 02:49:28', '2023-01-04 02:49:28'),
(72, '1672904197973.png', 'Charlie', 'TEST', 0, '73', 'crypto', 0, 'Wallet Maintenance, Deposit Suspended', 0, 'Wallet Maintenance, Withdrawal Suspended', '', 0, 0, '2023-01-05 02:06:38', '2023-01-05 02:06:38');

-- --------------------------------------------------------

--
-- Table structure for table `currency_networks`
--

CREATE TABLE `currency_networks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `currency_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `network_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deposit_enable` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=Active , 0=Not Active',
  `deposit_min` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.01',
  `deposit_desc` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Wallet Maintenance, Deposit Suspended',
  `withdraw_enable` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=Active , 0=Not Active',
  `withdraw_desc` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Wallet Maintenance, Withdrawal Suspended',
  `withdraw_min` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `withdraw_max` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `withdraw_commission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `deposit_commission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `deposit_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'flat' COMMENT ' flat, percentage',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'percentage' COMMENT 'flat , percentage',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1=Active , 0=Not Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currency_networks`
--

INSERT INTO `currency_networks` (`id`, `currency_id`, `network_id`, `address`, `token_type`, `deposit_enable`, `deposit_min`, `deposit_desc`, `withdraw_enable`, `withdraw_desc`, `withdraw_min`, `withdraw_max`, `withdraw_commission`, `deposit_commission`, `deposit_type`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '0x3597bfd533a99c9aa083587b074434e61eb0a258', 'ERC20', 0, '0.01', 'Deposit Description', 0, 'Withdraw Description Withdraw Description', '10', '10', '0.1', '0', 'flat', 'flat', 1, '2022-01-16 11:56:59', '2023-01-02 13:03:26'),
(2, '2', '3', '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '1', '100', '10', '0', 'flat', 'percentage', 1, '2022-01-16 12:19:52', '2022-12-01 08:10:47'),
(3, '3', '1', '0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '10', '100', '20', '0', 'flat', 'percentage', 1, '2022-01-16 12:29:34', '2022-03-23 18:35:02'),
(4, '4', '1', '0x3845badAde8e6dFF049820680d1F14bD3903a5d0', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:30:36', '2022-01-16 12:30:36'),
(5, '5', '1', '0xc00e94cb662c3520282e6f5717214004a7f26888', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:31:35', '2022-01-16 12:31:35'),
(6, '6', '1', '0x514910771af9ca656af840dff83e8264ecf986ca', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:34:12', '2022-01-16 12:34:12'),
(7, '7', '1', '0xd26114cd6EE289AccF82350c8d8487fedB8A0C07', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:50:27', '2022-01-16 12:50:27'),
(8, '8', '1', '0x1cf4592ebffd730c7dc92c1bdffdfc3b9efcf29a', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:51:38', '2022-01-16 12:51:38'),
(9, '9', '1', '0x00aba6fe5557de1a1d565658cbddddf7c710a1eb', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:52:37', '2022-01-16 12:52:37'),
(10, '10', '1', '0x0f5d2fb29fb7d3cfee444a200298f468908cc942', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:53:54', '2022-01-16 12:53:54'),
(11, '11', '1', '0xf629cbd94d3791c9250152bd8dfbdf380e2a3b9c', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:55:31', '2022-01-16 12:55:31'),
(12, '12', '1', '0xF433089366899D83a9f26A773D59ec7eCF30355e', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2022-01-16 12:56:22', '2022-01-16 12:56:22'),
(13, '13', '1', '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(14, '14', '1', '0x9992ec3cf6a55b00978cddf2b27bc6882d88d1ec', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(15, '15', '2', 'TBxTg48Y9ZRajLQpe9WDY9jYBWTANWSpps', 'TRC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(16, '16', '2', 'TLa2f6VPqDgRE67v1736s7bJ8Ray5wYjU7', 'TRC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(17, '17', '2', 'SELF', 'SELF', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '5', '1000', '2', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2022-12-01 07:53:45'),
(18, '18', '1', 'SELF', 'SELF', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0.1', '10', '0.1', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(19, '19', '3', '0x1d2f0da169ceb9fc7b3144628db156f3f6c60dbe', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(20, '20', '3', '0xba2ae424d960c26247dd6c32edc70b295c744c43', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(21, '21', '3', '0x88f1a5ae2a3bf98aeaf342d26b30a79438c9142e', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(22, '22', '3', '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(23, '23', '3', '0xcc42724c6683b7e57334c4e856f4c9965ed682bd', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(24, '24', '3', '0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(25, '25', '3', '0x7f70642d88cf1c4a3a7abb072b53b929b653eda5', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(26, '26', '3', '0xfb6115445bff7b52feb98650c87f44907e58f802', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(27, '27', '3', '0x7083609fce4d1d8dc0c979aab8c869ea2c873402', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(28, '28', '3', '0x715d400f88c167884bbcc41c5fea407ed4d2f8a0', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(29, '29', '3', '0x1ce0c2827e2ef14d5c4f29a091d735a204794041', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(30, '30', '3', '0x4338665cbb7b2485a8855a139b75d5e34ab0db94', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(31, '31', '3', '0x3ee2200efb3400fabb9aacf31297cbdd1d435d47', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '1', '10', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(32, '32', '3', '0x111111111117dc0aa78b770fa6a738034120c302', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(33, '33', '3', '0x8e17ed70334c87ece574c9d537bc153d8609e2a3', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(34, '34', '3', '0x2859e4544c4bb03966803b044a93563bd2d0dd4d', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(35, '35', '3', '0x3d6545b08693dae087e957cb1180ee38b9e3c25e', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(36, '36', '2', 'TCFLL5dx5ZJdKnWuesXxi1VPwjLVmWZZy9', 'TRC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(37, '37', '3', '0xfea6ab80cd850c3e63374bc737479aeec0e8b9a1', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(38, '38', '3', '0xd944f1d1e9d5f9bb90b62f9d45e447d989580782', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(39, '39', '1', '0x3506424f91fd33084466f402d5d97f05f8e3b4af', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(40, '40', '1', '0xbd31ea8212119f94a611fa969881cba3ea06fa3d', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(41, '41', '1', '0xdac17f958d2ee523a2206206994597c13d831ec7', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0.1', '1000', '0.1', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2022-06-01 08:43:27'),
(44, '44', '4', 'SELF', 'SELF', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0.000001', '10', '0.1', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(51, '41', '2', 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t', 'TRC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0.1', '100', '0.1', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2022-07-11 05:05:05'),
(52, '41', '3', '0x55d398326f99059ff775485246999027b3197955', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0.1', '5', '0.1', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(53, '16', '3', '0xaeF0d72a118ce24feE3cD1d43d383897D05B4e99', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 1, '2021-09-06 02:37:30', '2021-12-27 14:31:28'),
(57, '54', '3', '0x275b686A5c7312E50299b1c64507c90eE8a381A0', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '10', '10000', '1', '0', 'flat', 'percentage', 0, '2022-11-11 13:13:35', '2022-11-11 13:24:21'),
(58, '1', '1', '0x3597bfd533a99c9aa083587b074434e61eb0a258', 'ERC20', 1, '0.01', 'er', 1, 're', '10', '10', '0.1', '0', 'flat', 'percentage', 1, '2022-01-16 11:56:59', '2023-01-02 13:03:36'),
(59, '2', '3', '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', 'BEP20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 0, 'Wallet Maintenance, Withdrawal Suspended', '1', '100', '10', '0', 'flat', 'percentage', 1, '2022-01-16 12:19:52', '2023-01-02 00:52:14'),
(72, '71', '1', 'john', 'ERC20', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '1', '10', '0.1', '0', 'flat', 'percentage', 0, '2023-01-04 02:49:28', '2023-01-04 03:06:01'),
(73, '72', '3', 'testsdfasdfdasffsdfdsfdsfdsfdsf', 'BEP20', 0, '0.01', 'Wallet Maintenance, Deposit Suspended', 0, 'Wallet Maintenance, Withdrawal Suspended', '0', '0', '0', '0', 'flat', 'percentage', 0, '2023-01-05 02:06:38', '2023-01-05 02:06:38'),
(74, '55', '5', 'SELF', 'INR', 1, '0.01', 'Wallet Maintenance, Deposit Suspended', 1, 'Wallet Maintenance, Withdrawal Suspended', '0', '1000', '0.1', '0.1', 'flat', 'percentage', 1, '2023-01-05 02:06:38', '2023-01-05 02:06:38');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `user_unique_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `mobile_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `scopes` text DEFAULT NULL,
  `revoked` tinyint(1) DEFAULT 0,
  `expires_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `expires_at`, `created_at`, `updated_at`) VALUES
('1d8e665aae35563f07ae7e2b7ca33a90d3818cd1e57d6145212f7ff5697f8416f90609a56c2a50ad', 4, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-27 07:08:50', '2023-04-27 07:08:50'),
('281c7f0e24435fbe8067975ce808a6db7b2e5456f9703553012aedc7ad214ffa6493fbe66bf38dc6', 5, 1, '', NULL, 0, NULL, '2023-04-26 13:25:38', '2023-04-26 13:25:38'),
('5467575690d5e15a6337a436cab355398ab456ca6d15f13861ffda2fa20517e01cd79a17dd8c76b9', 5, 1, '', NULL, 0, NULL, '2023-04-26 13:24:14', '2023-04-26 13:24:14'),
('5951721fdee6b4474203183dbe2955c1bd4d7e1c67095bc6234a07044641a32f518cc4f87d126c53', 1, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-28 13:23:26', '2023-04-28 13:23:26'),
('6ea67b3c9609985db43636c2a69abc10ffa741230b7a4ee3b769dc6caad002a078072173310af222', 4, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-25 10:22:10', '2023-04-25 10:22:10'),
('72fb4135e70d87236b32ef7cf4b871173988b52b0bccb942fb0543e8030c0f096ab8a2cb4c5d617c', 4, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-25 10:13:17', '2023-04-25 10:13:17'),
('81998700d01df5e59def636104f2b07306af7fe2d2ecb672335612bdd0d1492b18865bc6e00a77ac', 4, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-26 13:32:16', '2023-04-26 13:32:16'),
('84190eb567f86ab3357333ba447f51c37dc6d659d0eba4a05913b2bc6e63de16f8c03935a15c3f32', 4, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-27 12:30:28', '2023-04-27 12:30:28'),
('857f5a43fbd35194dbe5e1d42eedc07c2d3cc38a3b310499109a4cdd972c5b80aca830892c532c4d', 5, 1, '', NULL, 0, NULL, '2023-04-26 13:33:47', '2023-04-26 13:33:47'),
('980b8e15125453b7cea80abfa2f8f012803763cba399cd407b66604262a25f6e3a0ce7ddec92e8b8', 4, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-26 13:30:26', '2023-04-26 13:30:26'),
('9bccc84f15655e97e3e27c380f221f02a670a104b92f96cfbbce4bcc3aad2e993761f7f43288612a', 1, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-28 11:12:06', '2023-04-28 11:12:06'),
('9d30b5169fcdcae3ce18559b7c01e7b3fb59ac8071441c169a7622eab437bd02906b830075cc6548', 1, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-28 05:15:39', '2023-04-28 05:15:39'),
('ac3cefcd22e0a7c8c8359cefded4e0ceabee0c2c6a8c59964311f384b588df45ba6f4ee0a8b187d4', 1, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-28 13:01:36', '2023-04-28 13:01:36'),
('acfbd25f0112b8b43ea56b06b61e9a74a04d5ab17479dd30738d6e6b9178d5b68b1a14cededc09de', 1, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-28 13:37:18', '2023-04-28 13:37:18'),
('d4b8009e2b174b0310f8712db30a2c5373138ca9a5f66fa5810a8cf107842f2f201b8e1faab1b3d5', 4, 1, 'reetika@yopmail.com', NULL, 0, NULL, '2023-04-27 07:36:37', '2023-04-27 07:36:37');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'Xigm93vugTe2j7nwmDfDLbYJ8XRPbIX9rlOUUswc', NULL, 'http://localhost', 1, 0, 0, '2022-04-15 00:16:31', '2022-04-15 00:16:31'),
(2, NULL, 'Laravel Password Grant Client', '3XoGQegzcYzsuKfcaIzXd49mG9MVSqCu0odcGBL5', 'users', 'http://localhost', 0, 1, 0, '2022-04-15 00:16:31', '2022-04-15 00:16:31'),
(3, NULL, 'Laravel Password Grant Client', '0vXcSwgubuhdTXtBZ9OOJXy91zx69fehcYyr7cdi', 'coinlisters', 'http://localhost', 0, 1, 0, '2022-04-15 00:19:21', '2022-04-15 00:19:21');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2022-04-15 00:16:31', '2022-04-15 00:16:31');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `otps`
--

CREATE TABLE `otps` (
  `id` bigint(20) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `otp` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `p2p_appeals`
--

CREATE TABLE `p2p_appeals` (
  `id` bigint(20) NOT NULL,
  `trade_id` bigint(20) NOT NULL,
  `appeal_user_id` bigint(20) NOT NULL,
  `appeal_by` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT 'opened',
  `reason` varchar(255) DEFAULT '',
  `cancelled_at` varchar(255) DEFAULT NULL,
  `appeal_win` varchar(255) DEFAULT '',
  `expired_at` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_appeals`
--

INSERT INTO `p2p_appeals` (`id`, `trade_id`, `appeal_user_id`, `appeal_by`, `status`, `reason`, `cancelled_at`, `appeal_win`, `expired_at`, `created_at`, `updated_at`) VALUES
(97, 54, 1, '1', 'processing', 'sadasdas', NULL, '', '1681474116958', '2023-04-14 12:07:36', '2023-04-14 12:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_currencies`
--

CREATE TABLE `p2p_currencies` (
  `id` bigint(20) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `decimal_length` varchar(255) DEFAULT NULL,
  `iso_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_currencies`
--

INSERT INTO `p2p_currencies` (`id`, `currency`, `created_at`, `updated_at`, `decimal_length`, `iso_code`) VALUES
(1, 'BTC', '2023-03-22 12:01:42', '2023-03-22 12:01:42', '8', '₿'),
(2, 'USDT', '2023-03-22 12:01:58', '2023-03-22 12:01:58', '2', '₮'),
(3, 'BUSD', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', '$'),
(4, 'BNB', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', 'Ⓑ'),
(5, 'ETH', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', 'Ξ'),
(6, 'DAI', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', '◈'),
(7, 'SHIB', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '2', NULL),
(8, 'ADA', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '2', '₳'),
(9, 'TRX', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', NULL),
(10, 'MATIC', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', NULL),
(11, 'SOL', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', '◎'),
(12, 'WRX', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', NULL),
(13, 'XRP', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', '✕'),
(14, 'DOT', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', '●'),
(15, 'DOGE', '2023-03-22 12:02:22', '2023-03-22 12:02:22', '8', 'Ð');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_dispute_chats`
--

CREATE TABLE `p2p_dispute_chats` (
  `id` bigint(20) NOT NULL,
  `appeal_id` bigint(20) NOT NULL,
  `comment` longtext NOT NULL,
  `receiver_id` bigint(20) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `trade_id` bigint(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `p2p_feedbacks`
--

CREATE TABLE `p2p_feedbacks` (
  `id` bigint(20) NOT NULL,
  `trade_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `feedback_to` bigint(20) NOT NULL,
  `feedback_type` varchar(255) NOT NULL,
  `feedback_comment` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_feedbacks`
--

INSERT INTO `p2p_feedbacks` (`id`, `trade_id`, `user_id`, `feedback_to`, `feedback_type`, `feedback_comment`, `created_at`, `updated_at`) VALUES
(61, 54, 1, 2, 'positive', 'ertretret', '2023-04-14 10:49:13', '2023-04-14 10:49:13'),
(62, 54, 2, 1, 'negative', 'retretret', '2023-04-14 10:49:22', '2023-04-14 10:49:22');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_fiat_currencies`
--

CREATE TABLE `p2p_fiat_currencies` (
  `id` bigint(20) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `form_field` text NOT NULL,
  `form_validate` text NOT NULL,
  `currency` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_fiat_currencies`
--

INSERT INTO `p2p_fiat_currencies` (`id`, `slug`, `form_field`, `form_validate`, `currency`, `created_at`, `updated_at`) VALUES
(1, 'gpay', '[{\"label\":\"Upi Id\",\"name\":\"upi_id\",\"required\":true,\"type\":\"text\"}]', '{\"upi_id\":\"required|max:255|upi_regex\"}', 'INR', '2023-02-27 12:03:45', '2023-02-27 12:03:45'),
(2, 'phonepe', '[{\"label\":\"Upi Id\",\"name\":\"upi_id\",\"required\":true,\"type\":\"text\"}]', '{\"upi_id\":\"required|max:255|upi_regex\"}', 'INR', '2023-02-27 12:04:07', '2023-02-27 12:04:07'),
(3, 'bank_transfer', '[{\"label\":\"Account Number\",\"name\":\"account_number\",\"required\":true,\"type\":\"text\"},{\"label\":\"Holder Name\",\"name\":\"holder_name\",\"required\":true,\"type\":\"text\"},{\"label\":\"Ifsc Code\",\"name\":\"ifsc_code\",\"required\":true,\"type\":\"text\"},{\"label\":\"Bank Name\",\"name\":\"bank_name\",\"required\":true,\"type\":\"text\"},{\"label\":\"Account Type\",\"name\":\"account_type\",\"required\":true,\"type\":\"select\",\"options\":[{\"label\":\"SAVING\",\"value\":\"saving\"},{\"label\":\"CURRENT\",\"value\":\"current\"}]}]', '{\"account_number\":\"required|max:50\",\"holder_name\":\"required|max:50\",\"ifsc_code\":\"required|max:50\",\"bank_name\":\"required|max:100\",\"account_type\":\"required|in:saving,current\"}', 'INR', '2023-02-27 12:04:21', '2023-02-27 12:04:21'),
(4, 'bank_transfer', '[{\"label\":\"Account Number\",\"name\":\"account_number\",\"required\":true,\"type\":\"text\"},{\"label\":\"Holder Name\",\"name\":\"holder_name\",\"required\":true,\"type\":\"text\"},{\"label\":\"Ifsc Code\",\"name\":\"ifsc_code\",\"required\":true,\"type\":\"text\"},{\"label\":\"Bank Name\",\"name\":\"bank_name\",\"required\":true,\"type\":\"text\"},{\"label\":\"Account Type\",\"name\":\"account_type\",\"required\":true,\"type\":\"select\",\"options\":[{\"label\":\"SAVING\",\"value\":\"saving\"},{\"label\":\"CURRENT\",\"value\":\"current\"}]}]', '{\"account_number\":\"required|max:50\",\"holder_name\":\"required|max:50\",\"ifsc_code\":\"required|max:50\",\"bank_name\":\"required|max:100\",\"account_type\":\"required|in:saving,current\"}', 'EUR', '2023-02-27 12:04:32', '2023-02-27 12:04:32'),
(5, 'paytm', '[{\"label\":\"Upi Id\",\"name\":\"upi_id\",\"required\":true,\"type\":\"text\"}]', '{\"upi_id\":\"required|max:255|upi_regex\"}', 'INR', '2023-03-03 07:48:30', '2023-03-03 07:48:30');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_msgs`
--

CREATE TABLE `p2p_msgs` (
  `id` bigint(20) NOT NULL,
  `sender_id` varchar(255) NOT NULL,
  `receiver_id` varchar(255) NOT NULL,
  `trade_id` varchar(255) NOT NULL,
  `msg` varchar(255) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_msgs`
--

INSERT INTO `p2p_msgs` (`id`, `sender_id`, `receiver_id`, `trade_id`, `msg`, `created_at`, `updated_at`) VALUES
(12, '2', '1', '51', 'aaaaa', '2023-04-11 05:14:13', '2023-04-11 05:14:13'),
(13, '2', '1', '51', 'bbb', '2023-04-11 05:14:27', '2023-04-11 05:14:27'),
(14, '2', '1', '51', 'zzz', '2023-04-11 05:17:30', '2023-04-11 05:17:30'),
(15, '2', '1', '51', 'xxx', '2023-04-11 05:27:29', '2023-04-11 05:27:29'),
(16, '1', '2', '51', 'qqq', '2023-04-11 05:31:55', '2023-04-11 05:31:55'),
(17, '2', '1', '51', 'www', '2023-04-11 05:32:01', '2023-04-11 05:32:01'),
(18, '1', '2', '51', 'zz', '2023-04-11 05:32:06', '2023-04-11 05:32:06'),
(19, '1', '2', '51', 'qqq', '2023-04-11 05:54:16', '2023-04-11 05:54:16'),
(20, '2', '1', '51', '111', '2023-04-11 05:54:21', '2023-04-11 05:54:21'),
(21, '1', '2', '51', '22', '2023-04-11 05:54:27', '2023-04-11 05:54:27'),
(22, '2', '1', '51', 'helllo merko buy krna h', '2023-04-11 06:00:58', '2023-04-11 06:00:58'),
(23, '1', '2', '51', 'haaan payment kr lo', '2023-04-11 06:01:13', '2023-04-11 06:01:13'),
(24, '2', '1', '51', 'payment done h\n', '2023-04-11 06:01:24', '2023-04-11 06:01:24'),
(25, '1', '2', '51', 'ha received', '2023-04-11 06:01:44', '2023-04-11 06:01:44'),
(26, '1', '2', '52', 'dadasdasd', '2023-04-13 09:36:27', '2023-04-13 09:36:27'),
(27, '2', '1', '52', 'asdada', '2023-04-13 09:36:31', '2023-04-13 09:36:31'),
(28, '2', '1', '52', 'kaisa h', '2023-04-14 07:41:13', '2023-04-14 07:41:13'),
(29, '1', '2', '52', 'mein thik hu', '2023-04-14 07:41:21', '2023-04-14 07:41:21'),
(30, '2', '1', '52', 'dfgsgf', '2023-04-14 07:45:36', '2023-04-14 07:45:36'),
(31, '1', '2', '52', '67547', '2023-04-14 07:45:41', '2023-04-14 07:45:41'),
(32, '2', '1', '52', 'dghjj', '2023-04-14 07:45:43', '2023-04-14 07:45:43'),
(33, '1', '2', '52', '4574574', '2023-04-14 07:45:46', '2023-04-14 07:45:46'),
(34, '2', '1', '52', 'dfhjfhj', '2023-04-14 07:45:51', '2023-04-14 07:45:51'),
(35, '1', '2', '52', 'fghjfhj', '2023-04-14 07:45:53', '2023-04-14 07:45:53'),
(36, '2', '1', '52', 'fghjgfhj', '2023-04-14 07:45:55', '2023-04-14 07:45:55'),
(37, '1', '2', '52', 'fghj;lkjgfhj\n=dklfg\nxkdfngkd\ndfgn\nhttp://192.168.11.112:5173/sell_detail?trade_id=52http://192.168.11.112:5173/sell_detail?trade_id=52http://192.168.11.112:5173/sell_detail?trade_id=52http://192.168.11.112:5173/sell_detail?trade_id=52http://192.168.11.112', '2023-04-14 07:46:19', '2023-04-14 07:46:19'),
(38, '2', '1', '52', '3495679348678934@#$^&*(^&%$^&*(^&*(*&^%$^&*(*&^*\"\"\"\"\'\'\'\'\'\'\'\'\'\'\'\'\'\"aidshgds8fy7g7gfdsfggf\'\'\'\'', '2023-04-14 07:46:51', '2023-04-14 07:46:51'),
(39, '1', '2', '53', 'cghjghj', '2023-04-14 08:02:50', '2023-04-14 08:02:50'),
(40, '2', '1', '53', 'ghjhgj', '2023-04-14 08:02:54', '2023-04-14 08:02:54'),
(41, '2', '1', '54', 'heloo admin kese ho\n', '2023-04-14 12:02:23', '2023-04-14 12:02:23'),
(42, '1', '2', '54', 'bdiyaa g', '2023-04-14 12:02:37', '2023-04-14 12:02:37');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_orders`
--

CREATE TABLE `p2p_orders` (
  `id` bigint(20) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `at_price` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `with_currency` varchar(255) NOT NULL,
  `order_type` varchar(255) NOT NULL COMMENT 'BUY, SELL',
  `min_qty` decimal(28,18) NOT NULL,
  `max_qty` decimal(28,18) NOT NULL,
  `search_min_qty` decimal(28,18) NOT NULL,
  `search_max_qty` decimal(28,18) NOT NULL,
  `pending_qty` decimal(28,18) NOT NULL,
  `payment_type` varchar(255) DEFAULT '',
  `terms_and_conditions` text DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'placed',
  `reason` varchar(255) DEFAULT '',
  `cancelled_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `regions` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_orders`
--

INSERT INTO `p2p_orders` (`id`, `user_id`, `country`, `at_price`, `currency`, `with_currency`, `order_type`, `min_qty`, `max_qty`, `search_min_qty`, `search_max_qty`, `pending_qty`, `payment_type`, `terms_and_conditions`, `status`, `reason`, `cancelled_at`, `created_at`, `updated_at`, `regions`) VALUES
(24, '1', 'Bahrain', '1', 'USDT', 'INR', 'BUY', '1.000000000000000000', '100.000000000000000000', '1.000000000000000000', '100.000000000000000000', '100.000000000000000000', '', NULL, 'cancelled', '', '2023-04-28 12:47:57', '2023-04-28 10:52:24', '2023-04-28 12:47:57', 'India'),
(25, '1', 'Bahrain', '1', 'BTC', 'INR', 'BUY', '2.000000000000000000', '2.000000000000000000', '2.000000000000000000', '2.000000000000000000', '2.000000000000000000', '', NULL, 'placed', '', NULL, '2023-04-28 12:55:29', '2023-04-28 12:55:29', 'India');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_payment_details`
--

CREATE TABLE `p2p_payment_details` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `payment_slug` varchar(255) NOT NULL,
  `payment_detail` text NOT NULL,
  `currency` varchar(255) NOT NULL,
  `status` int(11) DEFAULT 0 COMMENT '0.inactive,1.active',
  `is_verify` int(11) DEFAULT 0 COMMENT '0.pending,1.approved,2.rejected',
  `remark` varchar(255) DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_payment_details`
--

INSERT INTO `p2p_payment_details` (`id`, `user_id`, `payment_slug`, `payment_detail`, `currency`, `status`, `is_verify`, `remark`, `created_at`, `updated_at`) VALUES
(11, 4, 'gpay', '{\"upi_id\":\"Suryanreetu@yml\"}', 'INR', 0, 1, '', '2023-04-27 09:48:41', '2023-04-27 10:58:28'),
(12, 4, 'bank_transfer', '{\"account_number\":\"236526578364578\",\"holder_name\":\"reetika\",\"ifsc_code\":\"123213421\",\"bank_name\":\"asasasa\",\"account_type\":\"saving\"}', 'INR', 0, 0, '', '2023-04-27 09:54:10', '2023-04-27 09:54:10'),
(13, 4, 'gpay', '{\"upi_id\":\"Suryanreetu@fgm\"}', 'INR', 0, 0, '', '2023-04-27 10:19:46', '2023-04-27 10:58:28'),
(14, 4, 'bank_transfer', '{\"account_number\":\"236526578364578\",\"holder_name\":\"reetika\",\"ifsc_code\":\"eewe\",\"bank_name\":\"wsasas\",\"account_type\":\"saving\"}', 'EUR', 0, 0, '', '2023-04-27 11:34:16', '2023-04-27 11:34:16'),
(15, 1, 'gpay', '{\"upi_id\":\"rqwrqw@dfsdsf\"}', 'INR', 0, 0, '', '2023-04-28 06:39:21', '2023-04-28 06:39:21'),
(16, 1, 'gpay', '{\"upi_id\":\"Suryanreetu@fgm\"}', 'INR', 0, 0, '', '2023-04-28 13:09:38', '2023-04-28 13:09:38'),
(17, 1, 'gpay', '{\"upi_id\":\"Suryanreetu@fgm\"}', 'INR', 0, 0, '', '2023-04-28 13:10:01', '2023-04-28 13:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_trades`
--

CREATE TABLE `p2p_trades` (
  `id` bigint(20) NOT NULL,
  `buyer_order_id` bigint(20) NOT NULL,
  `seller_order_id` bigint(20) NOT NULL,
  `at_price` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `with_currency` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT 'processing',
  `buyer_confirmation` tinyint(4) DEFAULT 0 COMMENT '0 = Pending, 1 = Confirmed, 2 = Cancelled',
  `seller_confirmation` tinyint(4) DEFAULT 0 COMMENT '0 = Pending, 1 = Confirmed',
  `reason` varchar(255) DEFAULT '',
  `cancelled_at` datetime DEFAULT NULL,
  `expired_at` varchar(255) DEFAULT NULL,
  `appeal_at` varchar(255) DEFAULT NULL,
  `buyer_user_id` bigint(20) NOT NULL,
  `seller_user_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_trades`
--

INSERT INTO `p2p_trades` (`id`, `buyer_order_id`, `seller_order_id`, `at_price`, `quantity`, `currency`, `with_currency`, `status`, `buyer_confirmation`, `seller_confirmation`, `reason`, `cancelled_at`, `expired_at`, `appeal_at`, `buyer_user_id`, `seller_user_id`, `created_at`, `updated_at`) VALUES
(54, 21, 18, '95', '20', 'USDT', 'INR', 'disputed', 1, 0, '', NULL, NULL, '1681473776323', 2, 1, '2023-04-14 10:18:01', '2023-04-14 12:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_wallets`
--

CREATE TABLE `p2p_wallets` (
  `id` bigint(20) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `balance` varchar(255) DEFAULT '0',
  `freeze_balance` varchar(255) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_wallets`
--

INSERT INTO `p2p_wallets` (`id`, `user_id`, `currency`, `balance`, `freeze_balance`, `created_at`, `updated_at`) VALUES
(1, '4', 'USDT', '2890', '0', '2023-03-24 06:26:45', '2023-04-05 06:43:05'),
(2, '1', 'USDT', '10000', '-44', '2023-03-24 06:26:45', '2023-04-14 12:03:52'),
(3, '2', 'USDT', '954', '0', '2023-04-05 05:15:23', '2023-04-14 12:03:52');

-- --------------------------------------------------------

--
-- Table structure for table `p2p_wallet_logs`
--

CREATE TABLE `p2p_wallet_logs` (
  `id` bigint(20) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `wallet_id` varchar(255) NOT NULL,
  `amount` varchar(255) DEFAULT '0',
  `transaction_type` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p2p_wallet_logs`
--

INSERT INTO `p2p_wallet_logs` (`id`, `user_id`, `wallet_id`, `amount`, `transaction_type`, `comment`, `created_at`, `updated_at`) VALUES
(1, '8', '1', '100', 'debit', 'Freeze 100 USDT by placed sell order in p2p', '2023-03-24 06:15:27', '2023-03-24 06:15:27'),
(2, '8', '1', '1000', 'debit', 'Freeze 1000 USDT by placed sell order in p2p', '2023-03-24 06:17:51', '2023-03-24 06:17:51'),
(3, '1', '2', '20', 'debit', 'Freeze 20 USDT by placed sell order in p2p', '2023-04-04 07:08:40', '2023-04-04 07:08:40'),
(4, '1', '2', '20', 'debit', 'Freeze 20 USDT by placed sell order in p2p', '2023-04-04 07:15:56', '2023-04-04 07:15:56'),
(5, '1', '2', '20', 'debit', 'Freeze 20 USDT by placed sell order in p2p', '2023-04-04 07:25:07', '2023-04-04 07:25:07'),
(6, '1', '2', '20', 'debit', 'Freeze 20 USDT by placed sell order in p2p', '2023-04-04 07:27:30', '2023-04-04 07:27:30'),
(7, '1', '2', '20', 'debit', 'Freeze 20 USDT by placed sell order in p2p', '2023-04-04 07:28:56', '2023-04-04 07:28:56'),
(8, '1', '2', '20', 'debit', 'Freeze 20 USDT by placed sell order in p2p', '2023-04-04 07:30:10', '2023-04-04 07:30:10'),
(9, '1', '2', '20', 'debit', 'Freeze 20 USDT by placed sell order in p2p', '2023-04-04 07:35:27', '2023-04-04 07:35:27'),
(10, '1', '2', '20', 'debit', 'Freeze 20 USDT by placed sell order in p2p', '2023-04-04 07:38:50', '2023-04-04 07:38:50'),
(11, '1', '2', '10', 'debit', 'Freeze 10 USDT by placed sell order in p2p', '2023-04-05 04:58:55', '2023-04-05 04:58:55'),
(12, '2', '3', '10', 'credit', 'Credit 10 USDT to your p2p wallet', '2023-04-05 05:15:23', '2023-04-05 05:15:23'),
(13, '1', '2', '10', 'debit', 'UnFreeze 10 USDT from your p2p wallet', '2023-04-05 05:15:23', '2023-04-05 05:15:23'),
(14, '1', '2', '50.00', 'debit', 'Freeze 50.00 USDT by placed sell order in p2p', '2023-04-05 05:30:17', '2023-04-05 05:30:17'),
(15, '8', '1', '10', 'debit', 'Freeze 10 USDT by placed sell order in p2p', '2023-04-05 06:43:05', '2023-04-05 06:43:05'),
(16, '1', '2', '100', 'debit', 'Freeze 100 USDT by placed sell order in p2p', '2023-04-06 09:36:59', '2023-04-06 09:36:59'),
(17, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 10:48:39', '2023-04-06 10:48:39'),
(18, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 10:48:39', '2023-04-06 10:48:39'),
(19, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:06:04', '2023-04-06 11:06:04'),
(20, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:06:04', '2023-04-06 11:06:04'),
(21, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:13:23', '2023-04-06 11:13:23'),
(22, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:13:23', '2023-04-06 11:13:23'),
(23, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:15:28', '2023-04-06 11:15:28'),
(24, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:15:28', '2023-04-06 11:15:28'),
(25, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:19:50', '2023-04-06 11:19:50'),
(26, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:19:50', '2023-04-06 11:19:50'),
(27, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:25:53', '2023-04-06 11:25:53'),
(28, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:25:53', '2023-04-06 11:25:53'),
(29, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:30:14', '2023-04-06 11:30:14'),
(30, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:30:14', '2023-04-06 11:30:14'),
(31, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:31:40', '2023-04-06 11:31:40'),
(32, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:31:40', '2023-04-06 11:31:40'),
(33, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:34:45', '2023-04-06 11:34:45'),
(34, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:34:45', '2023-04-06 11:34:45'),
(35, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:37:23', '2023-04-06 11:37:23'),
(36, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:37:23', '2023-04-06 11:37:23'),
(37, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 11:47:36', '2023-04-06 11:47:36'),
(38, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 11:47:36', '2023-04-06 11:47:36'),
(39, '1', '2', '-450.000000000000000000', 'credit', 'Refund -450.000000000000000000 USDT by p2p sell order Expired!', '2023-04-06 11:51:50', '2023-04-06 11:51:50'),
(40, '1', '2', '-450.000000000000000000', 'credit', 'Refund -450.000000000000000000 USDT by p2p sell order Expired!', '2023-04-06 11:55:50', '2023-04-06 11:55:50'),
(41, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 12:22:03', '2023-04-06 12:22:03'),
(42, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 12:22:03', '2023-04-06 12:22:03'),
(43, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-06 12:56:57', '2023-04-06 12:56:57'),
(44, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-06 12:56:57', '2023-04-06 12:56:57'),
(45, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 09:43:59', '2023-04-07 09:43:59'),
(46, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 10:05:59', '2023-04-07 10:05:59'),
(47, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 10:25:59', '2023-04-07 10:25:59'),
(48, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 10:29:59', '2023-04-07 10:29:59'),
(49, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:01:59', '2023-04-07 11:01:59'),
(50, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:09:59', '2023-04-07 11:09:59'),
(51, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:26:56', '2023-04-07 11:26:56'),
(52, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:42:56', '2023-04-07 11:42:56'),
(53, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:44:56', '2023-04-07 11:44:56'),
(54, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:46:56', '2023-04-07 11:46:56'),
(55, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:48:56', '2023-04-07 11:48:56'),
(56, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:52:56', '2023-04-07 11:52:56'),
(57, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 11:58:56', '2023-04-07 11:58:56'),
(58, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 12:04:56', '2023-04-07 12:04:56'),
(59, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 12:06:56', '2023-04-07 12:06:56'),
(60, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 12:14:30', '2023-04-07 12:14:30'),
(61, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 13:04:06', '2023-04-07 13:04:06'),
(62, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 13:08:06', '2023-04-07 13:08:06'),
(63, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 13:16:06', '2023-04-07 13:16:06'),
(64, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 13:20:06', '2023-04-07 13:20:06'),
(65, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-07 13:28:06', '2023-04-07 13:28:06'),
(66, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-10 04:29:27', '2023-04-10 04:29:27'),
(67, '1', '2', '-550.000000000000000000', 'credit', 'Refund -550.000000000000000000 USDT by p2p sell order Expired!', '2023-04-10 04:57:27', '2023-04-10 04:57:27'),
(68, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-10 11:48:01', '2023-04-10 11:48:01'),
(69, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-10 11:48:01', '2023-04-10 11:48:01'),
(70, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-11 04:25:46', '2023-04-11 04:25:46'),
(71, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-11 04:25:46', '2023-04-11 04:25:46'),
(72, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-11 06:01:57', '2023-04-11 06:01:57'),
(73, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-11 06:01:57', '2023-04-11 06:01:57'),
(74, '1', '2', '-700.000000000000000000', 'credit', 'Refund -700.000000000000000000 USDT by p2p sell order Expired!', '2023-04-11 06:07:18', '2023-04-11 06:07:18'),
(75, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-11 06:49:08', '2023-04-11 06:49:08'),
(76, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-11 06:49:08', '2023-04-11 06:49:08'),
(77, '2', '3', '50.00', 'credit', 'Credit 50.00 USDT to your p2p wallet', '2023-04-11 06:49:54', '2023-04-11 06:49:54'),
(78, '1', '2', '50.00', 'debit', 'UnFreeze 50.00 USDT from your p2p wallet', '2023-04-11 06:49:54', '2023-04-11 06:49:54'),
(79, '1', '2', '-800.000000000000000000', 'credit', 'Refund -800.000000000000000000 USDT by p2p sell order Expired!', '2023-04-11 08:15:19', '2023-04-11 08:15:19'),
(80, '1', '2', '-800.000000000000000000', 'credit', 'Refund -800.000000000000000000 USDT by p2p sell order Expired!', '2023-04-11 10:31:21', '2023-04-11 10:31:21'),
(81, '1', '2', '-800.000000000000000000', 'credit', 'Refund -800.000000000000000000 USDT by p2p sell order Expired!', '2023-04-12 07:37:10', '2023-04-12 07:37:10'),
(82, '2', '3', '2', 'credit', 'Credit 2 USDT to your p2p wallet', '2023-04-14 07:50:56', '2023-04-14 07:50:56'),
(83, '1', '2', '2', 'debit', 'UnFreeze 2 USDT from your p2p wallet', '2023-04-14 07:50:56', '2023-04-14 07:50:56'),
(84, '2', '3', '2', 'credit', 'Credit 2 USDT to your p2p wallet', '2023-04-14 08:30:10', '2023-04-14 08:30:10'),
(85, '1', '2', '2', 'debit', 'UnFreeze 2 USDT from your p2p wallet', '2023-04-14 08:30:10', '2023-04-14 08:30:10'),
(86, '2', '3', '20', 'credit', 'Credit 20 USDT to your p2p wallet', '2023-04-14 10:20:59', '2023-04-14 10:20:59'),
(87, '1', '2', '20', 'debit', 'UnFreeze 20 USDT from your p2p wallet', '2023-04-14 10:20:59', '2023-04-14 10:20:59'),
(88, '2', '3', '20', 'credit', 'Credit 20 USDT to your p2p wallet', '2023-04-14 12:03:52', '2023-04-14 12:03:52'),
(89, '1', '2', '20', 'debit', 'UnFreeze 20 USDT from your p2p wallet', '2023-04-14 12:03:52', '2023-04-14 12:03:52');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `id` bigint(20) NOT NULL,
  `admin_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `temp_type` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0' COMMENT '1=Active,0=Inactive',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_unique_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `mobile_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_unique_id`, `name`, `email`, `email_verified_at`, `mobile`, `mobile_verified_at`, `password`, `created_at`, `updated_at`, `fname`, `lname`) VALUES
(1, '48922920', 'reetika', 'reetika@yopmail.com', NULL, '', NULL, '$2b$10$vjX/2fCYVf62VmMWjCOHaOtzgImHu2HIRCarihyqO0fi9wYws9Pk6', '2023-04-28 05:14:49', '2023-04-28 05:14:49', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_bank_accounts`
--

CREATE TABLE `user_bank_accounts` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `ifsc_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `account_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'current, saving',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0, 1',
  `is_verify` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `verify_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `branch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `payment_type_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_bank_accounts`
--

INSERT INTO `user_bank_accounts` (`id`, `user_id`, `alias`, `account_number`, `ifsc_code`, `country`, `state`, `account_type`, `status`, `is_verify`, `verify_status`, `remark`, `branch`, `payment_type_id`, `created_at`, `updated_at`) VALUES
(1, '2', 'lalit', '9874563210', 'SBIN0000319', 'Bangladesh', '', 'saving', '1', '1', 'completed', '', '', '1', '2023-01-03 13:56:09', '2023-01-04 15:51:45'),
(2, '1', 'Rohit', '111111111111', 'SBIN0002455', 'India', '', 'saving', '0', '1', 'completed', '', '', '1', '2023-01-03 14:00:12', '2023-01-04 15:54:14'),
(4, '2', 'lalit', '1236547890', 'SBIN0000319', 'Antarctica', '', 'saving', '0', '1', 'completed', '', '', '1', '2023-01-04 15:50:14', '2023-01-04 15:51:45'),
(5, '2', 'Lalit', '12365478902', 'SBIN0000319', 'Algeria', '', 'saving', '0', '1', 'completed', '', '', '1', '2023-01-04 15:50:44', '2023-01-04 15:51:45'),
(6, '1', 'Raja', '111111111119', 'SBIN0002456', 'Kyrgyzstan', '', 'saving', '1', '1', 'completed', '', '', '1', '2023-01-04 15:52:49', '2023-01-04 15:54:14'),
(7, '1', 'King', '111111111115', 'HDFC0000125', 'Angola', '', 'saving', '0', '1', 'completed', '', '', '1', '2023-01-04 15:53:07', '2023-01-04 15:54:14'),
(8, '3', 'Raja', '111111111119', 'SBIN0002456', 'Kyrgyzstan', '', 'saving', '1', '1', 'completed', '', '', '1', '2023-01-04 15:52:49', '2023-01-04 15:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `user_cryptos`
--

CREATE TABLE `user_cryptos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `freezed_balance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_cryptos`
--

INSERT INTO `user_cryptos` (`id`, `user_id`, `currency`, `balance`, `freezed_balance`, `created_at`, `updated_at`) VALUES
(1, '1', 'TRX', '4000', '0', '2023-03-02 12:10:59', '2023-03-02 12:11:41'),
(2, '1', 'USDT', '150', '0', '2023-03-02 12:11:11', '2023-03-02 12:12:23'),
(3, '1', 'SOL', '50', '0', '2023-03-02 12:11:17', '2023-03-02 12:11:17'),
(4, '8', 'TRX', '4000', '0', '2023-03-02 12:10:59', '2023-03-02 12:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `user_kycs`
--

CREATE TABLE `user_kycs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `date_birth` date NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `identity_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `identity_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `identity_front_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `identity_back_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_identity_verify` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `pan_card_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `pan_card_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_pan_verify` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `identity_remark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `pan_card_remark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `selfie_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_selfie_verify` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `selfie_remark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_kycs`
--

INSERT INTO `user_kycs` (`id`, `user_id`, `first_name`, `middle_name`, `last_name`, `country`, `state`, `date_birth`, `address`, `identity_type`, `identity_number`, `identity_front_path`, `identity_back_path`, `is_identity_verify`, `pan_card_number`, `pan_card_path`, `is_pan_verify`, `status`, `identity_remark`, `pan_card_remark`, `selfie_path`, `is_selfie_verify`, `selfie_remark`, `created_at`, `updated_at`) VALUES
(1, '2', 'Lalit', '', '', 'Bahrain', 'Al Mintaqah al Gharbiyah', '2008-01-01', 'dfgfdgfdgdfgdfgf', 'passport', 'gfhgfhgfh', '/kyc/identity/hda4kG_user_2_1672734427.png', '/kyc/identity/FvlsSp_user_2_1672734427.png', '1', 'HJKPK8418J', '', '0', 'completed', '', 'lalit', '/kyc/selfie/sdGDOO_user_2_1672734427.png', '1', '', '2023-01-03 08:27:07', '2023-01-03 08:29:29'),
(2, '1', 'admin', '', '', 'Bahrain', 'Al Mintaqah al Gharbiyah', '2008-01-01', 'dfgfdgfdgdfgdfgf', 'passport', 'gfhgfhgfh', '/kyc/identity/hda4kG_user_2_1672734427.png', '/kyc/identity/FvlsSp_user_2_1672734427.png', '1', 'admin', '', '0', 'completed', '', 'admin', '/kyc/selfie/sdGDOO_user_2_1672734427.png', '1', '', '2023-01-03 08:27:07', '2023-01-03 09:42:19'),
(3, '3', 'ranjit', '', '', 'Bahrain', 'Al Mintaqah al Gharbiyah', '2008-01-01', 'dfgfdgfdgdfgdfgf', 'passport', 'gfhgfhgfh', '/kyc/identity/hda4kG_user_2_1672734427.png', '/kyc/identity/FvlsSp_user_2_1672734427.png', '1', 'PKKOP9089Y', '', '0', 'completed', '', 'ranjit', '/kyc/selfie/sdGDOO_user_2_1672734427.png', '1', '', '2023-01-03 08:27:07', '2023-01-03 08:29:29'),
(4, '4', 'reetika', '', '', 'India', 'Al Mintaqah al Gharbiyah', '2008-01-01', 'dfgfdgfdgdfgdfgf', 'passport', 'gfhgfhgfh', '/kyc/identity/hda4kG_user_2_1672734427.png', '/kyc/identity/FvlsSp_user_2_1672734427.png', '1', 'PKKOP9089Y', '', '0', 'completed', '', 'ranjit', '/kyc/selfie/sdGDOO_user_2_1672734427.png', '1', '', '2023-01-03 08:27:07', '2023-01-03 08:29:29'),
(5, '5', 'rahul', '', '', 'India', 'Al Mintaqah al Gharbiyah', '2008-01-01', 'dfgfdgfdgdfgdfgf', 'passport', 'gfhgfhgfh', '/kyc/identity/hda4kG_user_2_1672734427.png', '/kyc/identity/FvlsSp_user_2_1672734427.png', '1', 'PKKOP9089Y', '', '1', 'completed', '', 'ranjit', '/kyc/selfie/sdGDOO_user_2_1672734427.png', '1', '', '2023-01-03 08:27:07', '2023-01-03 08:29:29');

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE `user_meta` (
  `id` bigint(20) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `is_email_authentication` tinyint(4) DEFAULT 1 COMMENT '0 = OFF, 1 = ON',
  `is_mobile_authentication` tinyint(4) DEFAULT 1 COMMENT '0 = OFF, 1 = ON',
  `is_google_authentication` tinyint(4) DEFAULT 0 COMMENT '0 = OFF, 1 = ON',
  `gauth_secret` varchar(255) DEFAULT NULL,
  `r_key` varchar(255) DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_meta`
--

INSERT INTO `user_meta` (`id`, `user_id`, `is_email_authentication`, `is_mobile_authentication`, `is_google_authentication`, `gauth_secret`, `r_key`, `expired_at`, `created_at`, `updated_at`) VALUES
(1, '1', 1, 1, 0, NULL, NULL, '2023-04-28 14:06:25', '2023-04-28 05:14:49', '2023-04-28 13:37:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_upis`
--

CREATE TABLE `user_upis` (
  `id` bigint(20) NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upi_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qr_code` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`qr_code`)),
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_verify` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `verify_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `payment_type_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_upis`
--

INSERT INTO `user_upis` (`id`, `user_id`, `alias`, `upi_id`, `qr_code`, `status`, `is_verify`, `verify_status`, `payment_type_id`, `remark`, `created_at`, `updated_at`) VALUES
(1, '2', 'Lalit', '9354663343@YBL', NULL, '0', '1', 'completed', '1', '', '2023-01-03 13:56:26', '2023-01-04 15:58:22'),
(2, '1', 'Rohit', '7018384067@upi', NULL, '1', '1', 'completed', '1', '', '2023-01-03 14:01:30', '2023-01-04 15:58:34'),
(3, '1', 'Raju', '7018384066@upi', NULL, '0', '0', 'completed', '1', '', '2023-01-04 15:56:44', '2023-01-04 15:58:34'),
(4, '2', 'sdfdsfdsfdsfdsfdsf', 'cxgfdxc@ghfvyj', NULL, '0', '1', 'completed', '1', '', '2023-01-04 15:56:50', '2023-01-04 15:58:22'),
(5, '1', 'zfbdfb', '7018384065@upi', NULL, '0', '1', 'completed', '1', '', '2023-01-04 15:57:00', '2023-01-04 15:58:34'),
(6, '2', 'sdfdsfds', 'dfsfdsfsdfdsfs@hgfh', NULL, '1', '1', 'completed', '1', '', '2023-01-04 15:57:04', '2023-01-04 15:58:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authorities`
--
ALTER TABLE `authorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_networks`
--
ALTER TABLE `block_networks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_network_types`
--
ALTER TABLE `block_network_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_networks`
--
ALTER TABLE `currency_networks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_id` (`user_unique_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `otps`
--
ALTER TABLE `otps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_appeals`
--
ALTER TABLE `p2p_appeals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_currencies`
--
ALTER TABLE `p2p_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_dispute_chats`
--
ALTER TABLE `p2p_dispute_chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_feedbacks`
--
ALTER TABLE `p2p_feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_fiat_currencies`
--
ALTER TABLE `p2p_fiat_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_msgs`
--
ALTER TABLE `p2p_msgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_orders`
--
ALTER TABLE `p2p_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_payment_details`
--
ALTER TABLE `p2p_payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_trades`
--
ALTER TABLE `p2p_trades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_wallets`
--
ALTER TABLE `p2p_wallets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_wallet_logs`
--
ALTER TABLE `p2p_wallet_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_id` (`user_unique_id`),
  ADD UNIQUE KEY `user_unique_id_2` (`user_unique_id`);

--
-- Indexes for table `user_bank_accounts`
--
ALTER TABLE `user_bank_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_cryptos`
--
ALTER TABLE `user_cryptos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_kycs`
--
ALTER TABLE `user_kycs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_upis`
--
ALTER TABLE `user_upis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authorities`
--
ALTER TABLE `authorities`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `block_networks`
--
ALTER TABLE `block_networks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `block_network_types`
--
ALTER TABLE `block_network_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `currency_networks`
--
ALTER TABLE `currency_networks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `otps`
--
ALTER TABLE `otps`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `p2p_appeals`
--
ALTER TABLE `p2p_appeals`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `p2p_currencies`
--
ALTER TABLE `p2p_currencies`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `p2p_dispute_chats`
--
ALTER TABLE `p2p_dispute_chats`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `p2p_feedbacks`
--
ALTER TABLE `p2p_feedbacks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `p2p_fiat_currencies`
--
ALTER TABLE `p2p_fiat_currencies`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `p2p_msgs`
--
ALTER TABLE `p2p_msgs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `p2p_orders`
--
ALTER TABLE `p2p_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `p2p_payment_details`
--
ALTER TABLE `p2p_payment_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `p2p_trades`
--
ALTER TABLE `p2p_trades`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `p2p_wallets`
--
ALTER TABLE `p2p_wallets`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `p2p_wallet_logs`
--
ALTER TABLE `p2p_wallet_logs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_bank_accounts`
--
ALTER TABLE `user_bank_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_cryptos`
--
ALTER TABLE `user_cryptos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_kycs`
--
ALTER TABLE `user_kycs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_upis`
--
ALTER TABLE `user_upis`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
