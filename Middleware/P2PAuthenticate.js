import reply from "../Common/reply.js";
import { Authority } from "../Models/Authority.js";


export default async (req, res, next) => {
    if(req?.user?.role == "admin"){
       return next();
    }

    var auth = await Authority.findOne({where:{type:'P2P',status:"on"}});
    if (auth) {
        return res.json(reply.failed("P2P is under maintaince"));
    }

    return next();
}
