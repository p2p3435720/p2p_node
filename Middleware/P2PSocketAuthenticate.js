
import JWT from 'jsonwebtoken';
import fs from 'fs';

import reply from "../Common/reply.js";

import { Token } from './../Models/Token.js';
import { User } from './../Models/User.js';


const PUBLIC_KEY = fs.readFileSync('./keys/public.key', 'utf-8');

export default async (req) => {
    
    // Is Token
    var token = req.headers['sec-websocket-protocol'] ?? null;

    if (token == null) {
        return reply.unauth();
    }
    
    // Verify Token
    const result = JWT.verify(token, PUBLIC_KEY, { algorithms: ['RS256'] }, function (err, user) {
        
        if (err) { return reply.unauth(); }

        return user;
    });

    if (result == 0) {
        return reply.unauth();
    }


    // Check Token In Database After Verify
    var is_token = await Token.findByPk(result.jti);
    if (is_token == null) {
        return reply.unauth();

    }

    var user = await User.findByPk(is_token.user_id);
    if (user == null) {
        return reply.unauth();
    }

    return reply.success('ok',user);
}

// Use This When Going Live
// https://obfuscator.io/