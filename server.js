import express from "express";
import http from "http";
const app = express();

import { fileURLToPath } from 'url';
import path from 'path';

const __filename = fileURLToPath(import.meta.url);
// sss
const __dirname = path.dirname(__filename);


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const PORT = 5001;

// import fileUpload from 'express-fileupload';
 

// app.use(fileUpload());

import cors from "./Config/cors.js";
app.use(cors);

import { socketServer } from "./P2P/websocket/socketServer.js";
import { UserRoutes } from "./Routes/UserRoutes.js";
app.use('/user', UserRoutes);

import { P2PBaseRoutes } from "./P2P/Routes/P2PBaseRoutes.js"; 
app.use('/p2p', P2PBaseRoutes);

app.get("/p2p/image/:type/:path", (req, res) => {
    res.sendFile(__dirname + '/P2P/Images/' + req.params.type + '/' + req.params.path);
});

var server = http.createServer({}, app);


socketServer(server);

server.listen(PORT, () => {
    console.log(`App is listening at http://localhost:${PORT}`);
});
