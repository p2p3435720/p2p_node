import { Model, Sequelize } from "../Database/sequelize.js";
import { P2P_PaymentDetail } from "../P2P/Models/P2P_PaymentDetail.js";
import { UserMeta } from "./UserMeta.js";

export const User = Model.define('users',{
    id:{
       type: Sequelize.INTEGER,
       autoIncrement: true,
       primaryKey: true
    },
    user_unique_id: {
       type: Sequelize.STRING,
       allowNull: false,
       unique: true
    },
    name:{
       type:Sequelize.STRING
    },
    fname : Sequelize.STRING,
    lname:Sequelize.STRING,
    email:{
       type:Sequelize.STRING
    },
    email_verified_at: {
       type: Sequelize.DATE,
       allowNull: true
    },
    mobile:{ 
       type:Sequelize.STRING
    },
    mobile_verified_at: {
       type: Sequelize.DATE,
       allowNull: true
    },
    password:{
       type:Sequelize.STRING
    },
 role:{
    type:Sequelize.STRING,
    defaultValue:'user'
 },
 user_verify:{
    type:Sequelize.INTEGER,
    defaultValue:1
 },
 status:{
    type:Sequelize.STRING,
    defaultValue:"1"
 }
 },{
      underscored:true,
      createdAt:'created_at',
      updatedAt:'updated_at'   
 })
 
 

const UserKyc = Model.define('user_kycs', {
    id: {
        type: Sequelize.BIGINT(20),
        autoIncrement: true,
        primaryKey: true
    },
    user_id: Sequelize.STRING,
    first_name: Sequelize.STRING,
    middle_name: Sequelize.STRING,
    last_name: Sequelize.STRING,
    date_birth: Sequelize.DATE,
    address: Sequelize.TEXT,
    identity_type: Sequelize.STRING,
    identity_number: Sequelize.STRING,
    identity_front_path: {
        type: Sequelize.STRING,
        get() {
            return getImgUrl(this.getDataValue('identity_front_path'));
        }
    },
    identity_back_path: {
        type: Sequelize.STRING,
        get() {
            return getImgUrl(this.getDataValue('identity_back_path'));
        }
    },
    is_identity_verify: {
        type: Sequelize.STRING,
        defaultValue: '0'
    },
    pan_card_number: Sequelize.STRING,
    pan_card_path: {
        type: Sequelize.STRING,
        get() {
            return getImgUrl(this.getDataValue('pan_card_path'));
        }
    },
    is_pan_verify: {
        type: Sequelize.STRING,
        defaultValue: 'pending'
    },
    status: {
        type: Sequelize.STRING,
        defaultValue: '0'
    },
    identity_remark: Sequelize.STRING,
    pan_card_remark: Sequelize.STRING,
    selfie_path: {
        type: Sequelize.STRING,
        defaultValue: '',
        get() {
            return getImgUrl(this.getDataValue('selfie_path'));
        }
    },
    is_selfie_verify: {
        type: Sequelize.STRING,
        defaultValue: '0'
    },
    selfie_remark: {
        type: Sequelize.STRING,
        defaultValue: ''
    },
    country: {
        type: Sequelize.STRING,
        defaultValue: ''
    },
    state: {
        type: Sequelize.STRING,
        defaultValue: ''
    }, 
}, {
    underscored: true,
});

await User.sync();

User.hasOne(UserKyc, { foreignKey: "user_id" });
User.hasOne(UserMeta, { as: "meta", foreignKey: "user_id" });
User.hasMany(P2P_PaymentDetail, { foreignKey: "user_id" });