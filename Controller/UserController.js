import { User } from "../Models/User.js"
import reply from "../Common/reply.js";
import Sequelize from "sequelize";
const { Op } = Sequelize;
import _ from "lodash";
import CValidator from "../Validator/CustomValidation.js";
import Validator from "validatorjs";
import bcrypt from 'bcrypt';
import { UserMeta } from "../Models/UserMeta.js";
import { Token } from "../Models/Token.js";
import crypto from "crypto"
import jwt from 'jsonwebtoken';
import { fileURLToPath } from 'url';
import path from 'path';
import fs from 'fs'
import Mail from "../Helper/mail.js";
import variables from "../Config/variables.js";
import Helper from "../Common/Helper.js";
import speakeasy from "speakeasy"

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
var privateKey = fs.readFileSync(path.join(__dirname, '..', 'keys/private.key'));
let r_key_timestmp = 600000

function firstError(validation) {
    let first_key = Object.keys(validation.errors.errors)[0];
    return validation.errors.first(first_key);
}

// for generate uer unique id
const generateUserUniqueId = async () => {
    let u_key = Math.floor(10000000 + Math.random() * 90000000)
    let exist = await User.findOne({ where: { user_unique_id: u_key }, attributes: ["id"] })
    if (exist) {
        return generateUserUniqueId()
    }
    return u_key;
}

// for register new user 
async function register(req, res) {

    let data = req.body;

    // return res.json({data})

    let { status, message } = await CValidator(data, {
        type: 'required|in:email,mobile',
        email: 'required_if:type,email|email',
        mobile: 'required_if:type,mobile|digits_between:7,15',
        password: 'required|password_regex',
        // confirm_password: 'required|same:password'
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    data = _.pick(data, ["type", "email", "mobile", "password"]);
    data.user_unique_id = await generateUserUniqueId()
    data.otp_status = 1
    var condition = {
        where:{}
    }
    if(data.email){
        condition.where.email = data.email
    }
    if(data.mobile){
        condition.where.mobile = data.mobile
    }
    //check if exists
    let exists = await User.findOne(condition)
    if (exists) {
        return res.send(reply.failed(`${data.type} Already Exist`))
    }

    //hashing password
    let result = bcrypt.hash(data.password, 10, async function (err, hash) {
        if (err) return 0;
        data.password = hash;
        let user = await User.create(data);
        await UserMeta.create({ user_id: user.id })
        if (!user) return 0;
        return res.send(reply.success("User Created"));
    });

    if (result == 0) {
        return res.send(reply.failed("Unable to create user at this time."));
    }
}
// IsValid_mail for verify mail and phone 
async function IsValid_mail(req, res) {
    let request = req.body
    const { username } = request;
    let { status, message } = await CValidator(request, {
        'username': 'required'
    })
    if (!status) {
        return res.send(reply.failed(message));
    }
    try {
        const user = await User.findOne({
            where: {
                [Op.or]: [{ email: username }, { mobile: username }]
            },attributes:["email"]
        });
        if (!user) {
            return res.send(reply.failed('Verification', false));
        } 
        return res.json(reply.success('Verification', {email:user.email }))
    } 
    catch (error) {
        console.log({ error });
        return res.json(reply.failed('Failed to Find Mail!!'));
    }


}

// simple login with mail and password
async function login(req, res) {
    let request = req.body;

    let { status, message } = await CValidator(request, {
        'email': 'required|email|exists:users,email',
        'password': 'required'   
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    try {
        let user = await User.findOne({
            where: {
                email: request.email,
            },
            attributes: ["id", "email", "password", "name"],
            include: [{ model: UserMeta, as: "meta", attributes: ["is_email_authentication", "is_mobile_authentication", "is_google_authentication", "expired_at", "r_key"] }],
        });

        if (!user) {
            return res.json(reply.failed('Invalid Data!!'));
        }
        let { id, email, password } = user;

        let hash = password.replace(/^\$2y(.+)$/i, '$2a$1');

        let compare_pwd = await bcrypt.compare(request.password, hash);

        if (!compare_pwd) {
            return res.json(reply.failed("Invalid Credentials!!"));
        }

        if (user.meta.r_key && user.meta.expired_at > Date.now()) {
            let result = parseInt((user.meta.expired_at - Date.now()) / 1000)
            let mins = parseInt(result / 60)
            let seconds = result % 60
            if (seconds < 10) {
                seconds = "0" + seconds
            };
            return res.send(reply.failed({ msg: `Please try after ${mins}:${seconds} ` }))
        }

      
        if (!user.meta) {
            return res.json(reply.failed("Server Issue Try it later!!"));
        }

        let { is_email_authentication, is_mobile_authentication, is_google_authentication, r_key, expired_at } = user.meta;

        let v = true; // stands for verification

        if (is_email_authentication == 0 && is_mobile_authentication == 0 && is_google_authentication == 0) {
            let t_id = crypto.randomBytes(40).toString('hex');

            await Token.create({
                id: t_id,
                user_id: id,
                client_id: '1',
                name: email
            });

            v = false;

            let token = jwt.sign({ jti: t_id }, privateKey, { algorithm: 'RS256', expiresIn: '1d' });

            return res.json(reply.success('Login Success', { token }));
        }


        r_key = crypto.randomBytes(64).toString('hex');
        expired_at = Date.now() + r_key_timestmp

        // r_key = btoa(r_key);
        await UserMeta.update({ r_key, expired_at }, {
            where: {
                user_id: id
            }
        });

        let vm = []; // stands for verification mode
        (is_email_authentication != 0) ? vm.push(_OTP.email) : '';
        (is_mobile_authentication != 0) ? vm.push(_OTP.mobile) : '';
        (is_google_authentication != 0) ? vm.push(_OTP.two_factor) : '';
        return res.send(reply.success('Verification Pending', { v, vm, r_key: btoa(r_key) }));

    } catch (error) {
        console.log({ error });
        return res.json(reply.failed('Unable to login at this moment!!'));
    }
}

// otp create
const _OTP = {   /// update keys 1,2,3
    "email": 0,
    "mobile": 1,
    "two_factor": 2
}

// get code for email and mobile
async function verfications(req, res) {
    let request = req.body;
    // request.user = req.user
    // request.type = req.query?.type;   

    let { status, message } = await CValidator(request, {
        'type': 'required|in:email,mobile',
        'value': 'required'
    })

    if (!status) {
        return res.send(reply.failed(message));
    }

    const user = await User.findOne({
        where: { [request.type]: request.value }, attributes: ["email", "mobile", "id"]
    });

    if (!user) {
        return res.send(reply.failed('Invalid Data!'));
    }

    var otp = Math.floor(100000 + Math.random() * 900000)
    if (request.type == "email") {
    
        // let otp = Math.floor(100000 + Math.random() * 900000)

        let details = await Helper.create_otp("login", otp, user.id, request.type, user.email)
        if (!details.resStatus) {
            return res.send(reply.failed(details.msg));
        }
        let mailContent = await Helper.getTemplateContent('email', 'login');
      
        let mailContentSend = mailContent.replace(variables.mail_dynamic_content, otp);
        
        let a = await Mail.send(user.email, mailContentSend);
        if (!a) {
            return res.send(reply.success('Unable to forgot at this time!!'));
        }
        return res.send(reply.success('Mail has successfully sent', { expired_at: details.expired_at }));
    }
    if (request.type == "mobile") {
        // let otp = Math.floor(100000 + Math.random() * 900000)

        let details = await Helper.create_otp("login", otp, user.id, request.type, user.mobile)
        if (!details.resStatus) {
            return res.send(reply.failed(details.msg));

        }
        let mailContent = await Helper.getTemplateContent('mobile', 'login');
        let mailContentSend = mailContent.replace(variables.mail_dynamic_content, otp);
        let a = await Mail.send(user.mobile, mailContentSend);
        if (!a) {
            return res.send(reply.success('Unable to forgot at this time!!'));
        }
        return res.send(reply.success('Mail has successfully sent', { expired_at: details.expired_at }));
    }

}

// create google auth 
async function set_googleauth(req, res) {
    let user = req?.user;

    let exist_user = await UserMeta.findOne({ where: { user_id: user?.id } });

    if (exist_user && exist_user?.is_google_authentication == 0) {

        let secret_key = speakeasy.generateSecret({ length: 10 });
        exist_user['gauth_secret'] = secret_key?.base32;
        exist_user.save();
        return res.json(reply.success("Google Auth Secret Created Successfully", secret_key?.base32));
    }
    return res.json(reply.failed("Google Authentication Key Already generated"));
}

//verify google auth with secrete key
async function google_auth_verfiy(req, res) {
    let request = req?.body;
    let user = req?.user;

    let { status, message } = await CValidator(request, {
        'totp': 'required|min:6|max:6'
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let exist_user = await UserMeta.findOne({ where: { user_id: user?.id } });
  

    if (!exist_user) {
        return res.json(reply.failed("Not found."));
    }

    // Verify a given token
    const verified = speakeasy.totp.verify({
        secret: exist_user?.gauth_secret,
        encoding: 'base32',
        token: request?.totp,
    }); // Returns true if the token matches

 


    if (verified) {
        exist_user["is_google_authentication"] = 1;
        exist_user.save();
        return res.json(reply.success("Google Authenticator Setup Verified Successfully"))
    } else {
        return res.json(reply.failed("Incorrect TOTP Provided"))
    }
}

// verify with otp google auth
async function gauthenticate(req, res) {
    let request = req?.body;

    let { status, message } = await CValidator(request, {
        'email': 'required|email|max:30',
        'totp': 'required|digits:6'
    });

    if (!status) {
        return res.send(reply.failed(message));
    }
    let exist_user = await User.findOne({
        where: {
            email: request.email,
        },
        attributes: ["name", 'email', 'mobile'],
        include: [{ model: UserMeta, as: "meta", attributes: ["user_id", 'is_google_authentication', 'gauth_secret'] }]
    });

    let { gauth_secret } = exist_user.meta;

    // if (is_google_authentication != 0) {
    //     return res.json(reply.failed("Please try again later."));
    // }

    // Verify a given token
    const verified = speakeasy.totp.verify({
        secret: gauth_secret,
        encoding: 'base32',
        token: request?.totp,
    }); // Returns true if the token matches

    const PRIVATE_KEY = fs.readFileSync('./keys/private.key', 'utf-8');
    const tokenid = crypto.randomBytes(80).toString('hex');
    let token = jwt.sign({ tokenid }, PRIVATE_KEY, { algorithm: 'RS256', expiresIn: '1d' });

    let data = {};
    if (verified) {
        data['token'] = token;
        data['user'] = exist_user;
        return res.json(reply.success("TOTP verified successfully.", data))
    }
    return res.json(reply.failed("Incorrect TOTP Provided."));
}

// verify otp email and mobile
// const verify_otp = async (req, res) => {
//     // {
//     // "otp"{
//     //     "email":"123456",
//     //    "mobile":"45454",
//     //   "two_factor":"456445"
//     // },
//     // "r_key":"cfffffff"
//     // }

//     // speakeasy.verification(otp.two_factor)

//     let request = req.body;

//     let { status, message } = await CValidator(request, {
//         r_key: "required",
//         otps: "required",
//     });
//     if (!status) {
//         return res.send(reply.failed(message));
//     }

//     if (typeof request.otps !== "object") {
//         return res.send(reply.failed("Invalid Data"));
//     }

//     let key = atob(request.r_key);

//     let userMetaData = await UserMeta.findOne({
//         where: { r_key: key },
//     });
   
// console.log(userMetaData,"userMetaDatauserMetaData");
//     if (!userMetaData) {
//         return res.send(reply.failed("Invalid Data"));
//     }

//     let user = await User.findOne({ where: { id: userMetaData.user_id } });

//     if (!user) {
//         return res.send(reply.failed("Invalid Data"));
//     }
  
//     // Mobile Otp verification
//     if (userMetaData.is_mobile_authentication) {
//         if (!request.otps["mobile"]) {
//             return res.send(reply.failed("Invalid Mobile Data"));
//         }
//         let details = await Helper.verify(
//             "mobile",
//             request.otps["mobile"],
//             user.id
//         );

//         if (!details.status) {
//             return res.send(reply.failed(details.message));
//         }
//     }

//     // Email Otp verification
//     if (userMetaData.is_email_authentication) {
//         if (!request.otps["email"]) {
//             return res.send(reply.failed("Invalid email Data"));
//         }
//         let details = await Helper.verify("email", request.otps["email"], user.id);
//         if (!details.status) {
//             return res.send(reply.failed(details.message));
//         }
//     }

//     //   User meta table rkey could not be expiry
//     // isemail , ismobile , istwofactor

//     // verify
//     if (user.email_verified_at == null) {
//         user.email_verified_at = Date.now();
//         await user.save();
//     }
//     let t_id = crypto.randomBytes(40).toString("hex");

//     await Token.create({
//         id: t_id,
//         user_id: user.id,
//         client_id: "1",
//         name: user.email,
//     });

//     var token = jwt.sign(
//         { jti: t_id },
//         privateKey,
//         { algorithm: "RS256" },
//         { expiresIn: "1d" }
//     );

//     userMetaData.r_key = null;
//     await userMetaData.save();
//     return res.json(
//         reply.success("Login Success", {
//             id: user.id,
//             name: user.name,
//             email: user.email,
//             token,
//         })
//     );
// };

// modifie verify_otp
const verify_otp = async (req, res) => {
    // {
    // "otp"{
    //     "email":"123456",
    //    "mobile":"45454",
    //   "two_factor":"456445"
    // },
    // "r_key":"cfffffff"
    // }

    // speakeasy.verification(otp.two_factor)

    let request = req.body;

    let { status, message } = await CValidator(request, {
        r_key: "required",
        otps: "required",
    });
    if (!status) {
        return res.send(reply.failed(message));
    }

    if (typeof request.otps !== "object") {
        return res.send(reply.failed("Invalid Data"));
    }

    let key = atob(request.r_key);

    let userMetaData = await UserMeta.findOne({
        where: { r_key: key },
        attribute:["gauth_secret"]
    });
   

    if (!userMetaData) {
        return res.send(reply.failed("Invalid Data"));
    }

    let user = await User.findOne({ where: { id: userMetaData.user_id } });

    if (!user) {
        return res.send(reply.failed("Invalid Data"));
    }  
   
 
    if (userMetaData.is_google_authentication) {
        if (!request.otps["totp"]) {
            return res.send(reply.failed("Invalid GoogleAuth Data"));
        }
        // let details = await Helper.verify(
        //     "mobile",
        //     request.otps["mobile"],
        //     user.id
        // ); 

        const verified = speakeasy.totp.verify({
            secret: userMetaData?.gauth_secret,
            encoding: 'base32', 
            token: request.otps["totp"],
        });   
        
        const PRIVATE_KEY = fs.readFileSync('./keys/private.key', 'utf-8');
        const tokenid = crypto.randomBytes(80).toString('hex');
        let tokens = jwt.sign({ tokenid }, PRIVATE_KEY, { algorithm: 'RS256', expiresIn: '1d' });
     
        let data = {};
         
        // if (verified) {
        //     data['token'] = tokens;
        //     data['user'] = exist_user;
        //     return res.json(reply.success("TOTP verified successfully.", data))
        // }

        // if (!details.status) {
        //     return res.send(reply.failed(details.message));
        // }
    }
    
    // Mobile Otp verification
    if (userMetaData.is_mobile_authentication) {
        if (!request.otps["mobile"]) {
            return res.send(reply.failed("Invalid Mobile Data"));
        }
        let details = await Helper.verify(
            "mobile",
            request.otps["mobile"],
            user.id
        );

        if (!details.status) {
            return res.send(reply.failed(details.message));
        }
    }

    // Email Otp verification
    if (userMetaData.is_email_authentication) {
        if (!request.otps["email"]) {
            return res.send(reply.failed("Invalid email Data"));
        }
        let details = await Helper.verify("email", request.otps["email"], user.id);
        if (!details.status) {
            return res.send(reply.failed(details.message));
        }
    }

    //   User meta table rkey could not be expiry
    // isemail , ismobile , istwofactor

    // verify
    if (user.email_verified_at == null) {
        user.email_verified_at = Date.now();
        await user.save();
    }
    let t_id = crypto.randomBytes(40).toString("hex");

    await Token.create({
        id: t_id,
        user_id: user.id,
        client_id: "1",
        name: user.email,
    });

    var token = jwt.sign(
        { jti: t_id },
        privateKey,
        { algorithm: "RS256" },
        { expiresIn: "1d" }
    );

    userMetaData.r_key = null;
    await userMetaData.save();
    return res.json(
        reply.success("Login Success", {
            id: user.id,
            name: user.name,
            email: user.email,
            token,
        })
    );
}

// forgot password
async function forgotPassword(req, res) {
    let request = req.body;

    let { status, message } = await CValidator(request, {
        'email': 'required|email|exists:users,email',
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let user = await User.findOne({ where: { email: request.email } });
    let otp = Math.floor(100000 + Math.random() * 900000)

    let details = await Helper.create_otp("forgot", otp, user.id,"email",request.email)

    if (!details.resStatus) {
        return res.send(reply.failed(details.msg));

    }
    let mailContent = await Helper.getTemplateContent('email', 'forgot');


    let mailContentSend = mailContent.replace(variables.mail_dynamic_content, otp);


    let a = await Mail.send(user.email, mailContentSend);

    if (!a) {
        return res.send(reply.success('Unable to forgot at this time!!'));
    }

    return res.send(reply.success('Mail has successfully sent', { expired_at: details.expired_at }));

}

// after forgot reset password
async function resetPassword(req, res) {
    let request = req.body;

    let { status, message } = await CValidator(request, {
        'otp': 'required|min:6|max:6',
        'email': 'required|email|exists:users,email',
        'new_password': 'required|min:8|max:18|password_regex',
        'confirm_password': 'required|same:new_password',
    });

    if (!status) {
        return res.send(reply.failed(message));
    }

    let user = await User.findOne({ where: { email: request.email } });
    let hash = user.password.replace(/^\$2y(.+)$/i, '$2a$1');


    //####### If old and new password is same
    let compare_new_pwd = await bcrypt.compare(request.new_password, hash);
    if (compare_new_pwd) {
        return res.json(reply.failed("old and new password should not be same!!"));
    }

    //#### Updated New password
    let hash_pwd = await bcrypt.hash(request.new_password, saltRounds);


    let details = await Helper.verify("forgot", request.otp, user.id)
    if (!details.status) {
        return res.send(reply.failed(details.message))
    };


    let update = await User.update({ password: hash_pwd }, { where: { id: user.id } })

    // # If password doesn't change with any server issue
    return (update ? res.send(reply.success('Reset Password Sucessfully')) : res.send(reply.failed('unable to change password at this time, Please try it later!')))
    // return res.send(reply.success('Password Changed', user))
}

// logout api //
async function logout(req, res) { 
    let token_id = req.user.id
    try {
        //// Delete Refresh Token
        await Token.destroy({ where: { user_id: token_id } }) 
        return res.send(reply.success('Logout Successfully'))
    } catch (error) {
        console.log(error)
        return res.send(reply.failed('Unable to Logout'))
    }
}


export default {
    login,
    register,
    IsValid_mail,
    verfications,
    set_googleauth,
    google_auth_verfiy,
    gauthenticate,
    verify_otp,
    forgotPassword,
    resetPassword,
    logout
}